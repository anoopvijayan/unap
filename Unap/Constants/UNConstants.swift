//
//  UNConstants.swift
//  Unapp
//
//  Created by Levin  on 03/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation

/// User type that will be used through out Unap
enum userType:String,Codable{
    case  landlord
    case  student
}

extension userType{
    var value:String{
      return self.rawValue
    }
}

/// Segue Identifiers

extension WelcomeVC:SegueHandlerType{
    
    enum SegueIdentifier: String {
        case createAccount
        case login
    }
}
