//
//  UNUser+CoreDataProperties.swift
//  Unap
//
//  Created by Levin  on 05/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//
//

import Foundation
import CoreData


extension UNUser {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UNUser> {
        return NSFetchRequest<UNUser>(entityName: "UNUser")
    }

    @NSManaged public var userId: String?
    @NSManaged public var userData: NSObject?
    @NSManaged public var userToken: String?

}
