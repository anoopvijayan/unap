//
//  UNMessage+CoreDataProperties.swift
//  Unap
//
//  Created by Levin  on 27/12/18.
//  Copyright © 2018 Levin . All rights reserved.
//
//

import Foundation
import CoreData


extension UNMessage {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UNMessage> {
        return NSFetchRequest<UNMessage>(entityName: "UNMessage")
    }

    @NSManaged public var dateSent: NSDate?
    @NSManaged public var isDelivered: Bool
    @NSManaged public var isSeen: Bool
    @NSManaged public var message: String?
    @NSManaged public var messageID: String?
    @NSManaged public var qbMessage: NSObject?
    @NSManaged public var recipientID: Int64
   // @NSManaged public var section: String?
    @NSManaged public var senderID: Int64
    @NSManaged public var dialogID: String?
    @NSManaged public var id: String?
    @NSManaged public var conversation: UNConversation?
    
     @objc var section: String? {
        
        return self.dayDifference(messageDate: dateSent! as Date)
        
    }
    
    func dayDifference(messageDate:Date) -> String
    {
        let calendar = NSCalendar.current
        let date = messageDate
        if calendar.isDateInYesterday(date) { return "Yesterday" }
        else if calendar.isDateInToday(date) { return "Today" }
        else if calendar.isDateInTomorrow(date) { return "Tomorrow" }
        else {
            let startOfNow = calendar.startOfDay(for: Date())
            let startOfTimeStamp = calendar.startOfDay(for: date)
            let components = calendar.dateComponents([.day], from: startOfNow, to: startOfTimeStamp)
            let day = components.day!
            if day < 1 { return "\(abs(day)) days ago" }
            else { return "In \(day) days" }
        }
    }
    
}
