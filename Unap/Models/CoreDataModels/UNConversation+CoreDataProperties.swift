//
//  UNConversation+CoreDataProperties.swift
//  Unap
//
//  Created by Levin  on 27/12/18.
//  Copyright © 2018 Levin . All rights reserved.
//
//

import Foundation
import CoreData


extension UNConversation {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UNConversation> {
        return NSFetchRequest<UNConversation>(entityName: "UNConversation")
    }

    @NSManaged public var acceptedList: NSObject?
    @NSManaged public var lastMessage: String?
    @NSManaged public var lastMessageDate: NSDate?
    @NSManaged public var qbDialog: NSObject?
    @NSManaged public var qblastMessage: NSObject?
    @NSManaged public var requestID: String?
    @NSManaged public var unreadMessageCount: Int64
    @NSManaged public var messages: NSSet?
    
    var acceptListData:AcceptedList? {
        let decoder = JSONDecoder()
        let values = try? decoder.decode(AcceptedList.self, from: self.acceptedList as! Data)
        return values
    }
    
    
}

// MARK: Generated accessors for messages
extension UNConversation {

    @objc(addMessagesObject:)
    @NSManaged public func addToMessages(_ value: UNMessage)

    @objc(removeMessagesObject:)
    @NSManaged public func removeFromMessages(_ value: UNMessage)

    @objc(addMessages:)
    @NSManaged public func addToMessages(_ values: NSSet)

    @objc(removeMessages:)
    @NSManaged public func removeFromMessages(_ values: NSSet)

}
