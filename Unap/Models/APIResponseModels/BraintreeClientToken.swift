//
//  BraintreeClientToken.swift
//  Unap
//
//  Created by Aneesh on 12/28/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation
struct ResponseToken : Codable {
    let client_token : String?
    
    enum CodingKeys: String, CodingKey {
        
        case client_token = "client_token"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        client_token = try values.decodeIfPresent(String.self, forKey: .client_token)
    }
    
}
