/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import SwiftyJSON

struct UniversityResponse : Codable{
    let universities : [Universities]?
}


struct Universities : Codable {
	let id : Int?
	let university_name : String?
	let university_status : Int?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case university_name = "university_name"
		case university_status = "university_status"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		university_name = try values.decodeIfPresent(String.self, forKey: .university_name)
		university_status = try values.decodeIfPresent(Int.self, forKey: .university_status)
	}

}

protocol unversitylocalFetchable{
    static func loadFromjsonFile() -> UniversityResponse?
}

extension unversitylocalFetchable{
    
    static func loadFromjsonFile() -> UniversityResponse?{
        guard let fileUrl = Bundle.main.url(forResource: "university", withExtension: "json") else {
            print("File could not be located at the given url")
            return nil
        }
        
        do {
            // Get data from file
            let data = try Data(contentsOf: fileUrl)
            
            // Decode data to a Dictionary<String, Any> object
            let decoder = JSONDecoder.init()
            let result = try decoder.decode(UniversityResponse.self, from: try self.checkForResponseStatus(data: data))
            return result
        } catch(let error) {
            // Print error if something went wrong
            print("Error: \(error)")
            return nil
        }
    }
    
     static func checkForResponseStatus(data:Data) throws -> Data{
        
        if JSON(data)["status"].int == 1 {
            return try JSON(data)["response"].rawData()
        }else if JSON(data)["status"].int == 0 {
            throw NetworkError(apiError: JSON(data)["message"].string ?? "Something went wrong.")
        }
        throw NetworkError(validationError: "Validation error")
    }
}

extension Universities:unversitylocalFetchable{
    
}
