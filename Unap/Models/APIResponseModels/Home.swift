/*
 Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 
 For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar
 
 */

import Foundation
struct Home : Codable {
    let id : Int?
    let description : String?
    let room_count : String?
    let bed_count : String?
    let bathroom_count : String?
    let address : String?
    let home_price : String?
    let accomodation_type : String?
    let lattitude : String?
    let longitude : String?
    let home_title : String?
    let amenities : [Amenities]?
    let owner_name : String?
    let profile_pic : String?
    let bills : [Bills]?
    let currency_code: String?
    let home_img : [Home_images]?
    let home_request_status :  Int?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case description = "description"
        case room_count = "room_count"
        case bed_count = "bed_count"
        case address  = "address"
        case bathroom_count = "bathroom_count"
        case home_price = "home_price"
        case accomodation_type = "accomodation_type"
        case lattitude = "lattitude"
        case longitude = "longitude"
        case home_title = "home_title"
        case amenities = "amenities"
        case bills = "bills"
        case currency_code = "currency_code"
        case home_img = "home_img"
        case profile_pic = "profile_pic"
        case owner_name  = "owner_name"
        case home_request_status = "home_request_status"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        room_count = try values.decodeIfPresent(String.self, forKey: .room_count)
        bed_count = try values.decodeIfPresent(String.self, forKey: .bed_count)
        bathroom_count = try values.decodeIfPresent(String.self, forKey: .bathroom_count)
        home_price = try values.decodeIfPresent(String.self, forKey: .home_price)
        accomodation_type = try values.decodeIfPresent(String.self, forKey: .accomodation_type)
        lattitude = try values.decodeIfPresent(String.self, forKey: .lattitude)
        longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
        home_title = try values.decodeIfPresent(String.self, forKey: .home_title)
        amenities = try values.decodeIfPresent([Amenities].self, forKey: .amenities)
        bills = try values.decodeIfPresent([Bills].self, forKey: .bills)
        currency_code = try values.decodeIfPresent(String.self, forKey: .currency_code)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        home_img = try values.decodeIfPresent([Home_images].self, forKey: .home_img)
        owner_name = try values.decodeIfPresent(String.self, forKey: .owner_name)
        profile_pic = try values.decodeIfPresent(String.self, forKey: .profile_pic)
        home_request_status =  try values.decodeIfPresent(Int.self, forKey: .home_request_status)
    }
    
}

extension Home{
    var bedDisplayable:String{
        guard let bedCount = self.bed_count else { return " 0 Bed" }
        return " " + bedCount + " " + "Bed"
    }
    
    var roomDisplayable:String{
        guard let roomCount = self.room_count else { return " 0 Room" }
        return " " + roomCount + " " + "Room"
    }
    
    var bathroomDisplayable:String{
        guard let bathroomCount = self.bathroom_count else { return " 0 Bath" }
        return " " + bathroomCount + " " + "Bath"
    }
    
    var rentDisplayble:String{
        guard let  currency = self.currency_code,
            let  rent = self.home_price,
            let  currencySymbol = self.getCurrencySymbol(from: currency)
            else { return "" }
        
        return " " + rent + " "+currencySymbol + " " + "/month"
    }
    
    var rentDisplaybleWithOutMonth:String{
        guard let  currency = self.currency_code,
            let  rent = self.home_price,
            let  currencySymbol = self.getCurrencySymbol(from: currency)
            else { return "" }
        
        return " " + rent + " "+currencySymbol
    }
    
    var hostDisplayable:String{
        guard let hosted = self.owner_name else { return "" }
        return "Hosted by " + hosted
    }
    
    func getCurrencySymbol(from currencyCode: String) -> String? {
        
        let locale = NSLocale(localeIdentifier: currencyCode)
        if locale.displayName(forKey: .currencySymbol, value: currencyCode) == currencyCode {
            let newlocale = NSLocale(localeIdentifier: currencyCode.dropLast() + "_en")
            return newlocale.displayName(forKey: .currencySymbol, value: currencyCode)
        }
        return locale.displayName(forKey: .currencySymbol, value: currencyCode)
    }
    
    
}



struct Bills : Codable {
    
    let id : Int?
    let bill_included : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case bill_included = "bill_included"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        bill_included = try values.decodeIfPresent(String.self, forKey: .bill_included)
    }
    
}

struct Amenities : Codable {
    let id : Int?
    let amenity : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case amenity = "amenity"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        amenity = try values.decodeIfPresent(String.self, forKey: .amenity)
    }
    
}

struct Home_images : Codable {
    let id : Int?
    let home_image : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case home_image = "home_image"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        home_image = try values.decodeIfPresent(String.self, forKey: .home_image)
    }
    
}


struct HomeAPIResponse : Codable {
    let home : Home?
    let home_img : [Home_images]?
    
    enum CodingKeys: String, CodingKey {
        
        case home = "home"
        case home_img = "home_img"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        home = try values.decodeIfPresent(Home.self, forKey: .home)
        home_img = try values.decodeIfPresent([Home_images].self, forKey: .home_img)
    }
    
}
struct HomeListResponse:Codable {
    let homeList:HomeList?
    
    enum CodingKeys: String, CodingKey {
        
        case homeList = "home"
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        homeList = try values.decodeIfPresent(HomeList.self, forKey: .homeList)
        
    }
    
}
struct HomeList : Codable {
    let current_page : Int?
    let home : [Home]?
    let first_page_url : String?
    let from : Int?
    let last_page : Int?
    let last_page_url : String?
    let next_page_url : String?
    let path : String?
    let per_page : Int?
    let prev_page_url : String?
    let to : Int?
    let total : Int?
    
    enum CodingKeys: String, CodingKey {
        
        case current_page = "current_page"
        case home = "data"
        case first_page_url = "first_page_url"
        case from = "from"
        case last_page = "last_page"
        case last_page_url = "last_page_url"
        case next_page_url = "next_page_url"
        case path = "path"
        case per_page = "per_page"
        case prev_page_url = "prev_page_url"
        case to = "to"
        case total = "total"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        current_page = try values.decodeIfPresent(Int.self, forKey: .current_page)
        home = try values.decodeIfPresent([Home].self, forKey: .home)
        first_page_url = try values.decodeIfPresent(String.self, forKey: .first_page_url)
        from = try values.decodeIfPresent(Int.self, forKey: .from)
        last_page = try values.decodeIfPresent(Int.self, forKey: .last_page)
        last_page_url = try values.decodeIfPresent(String.self, forKey: .last_page_url)
        next_page_url = try values.decodeIfPresent(String.self, forKey: .next_page_url)
        path = try values.decodeIfPresent(String.self, forKey: .path)
        per_page = try values.decodeIfPresent(Int.self, forKey: .per_page)
        prev_page_url = try values.decodeIfPresent(String.self, forKey: .prev_page_url)
        to = try values.decodeIfPresent(Int.self, forKey: .to)
        total = try values.decodeIfPresent(Int.self, forKey: .total)
    }
    
}

