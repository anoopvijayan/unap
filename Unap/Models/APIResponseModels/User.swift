

import Foundation

struct userUpdateResponse:Codable{
    let user:User?
    let bank:BankDetail?
}

struct User : Codable {
    let id : Int?
    let first_name : String?
    let age_or_dob : String?
    let email : String?
    let university_id : Int?
    let profile_pic : String?
    let user_type : userType?
    let device_token : String?
    let device_type : String?
    let qblocks_id : String?
    let stripe_id : String?
    let doc_is_verified : Int?
    let is_verified : Int?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case first_name = "first_name"
        case age_or_dob = "age_or_dob"
        case email = "email"
        case university_id = "university_id"
        case profile_pic = "profile_pic"
        case user_type = "user_type"
        case device_token = "device_token"
        case device_type = "device_type"
        case qblocks_id = "qblocks_id"
        case stripe_id = "stripe_id"
        case doc_is_verified = "doc_is_verified"
        case is_verified = "is_verified"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        first_name = try values.decodeIfPresent(String.self, forKey: .first_name)
        age_or_dob = try values.decodeIfPresent(String.self, forKey: .age_or_dob)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        university_id = try values.decodeIfPresent(Int.self, forKey: .university_id)
        profile_pic = try values.decodeIfPresent(String.self, forKey: .profile_pic)
        user_type = try values.decodeIfPresent(userType.self, forKey: .user_type)
        device_token = try values.decodeIfPresent(String.self, forKey: .device_token)
        device_type = try values.decodeIfPresent(String.self, forKey: .device_type)
        qblocks_id = try values.decodeIfPresent(String.self, forKey: .qblocks_id)
        stripe_id = try values.decodeIfPresent(String.self, forKey: .stripe_id)
        doc_is_verified = try values.decodeIfPresent(Int.self, forKey: .doc_is_verified)
        is_verified = try values.decodeIfPresent(Int.self, forKey: .is_verified)
    }
    
}
