

import Foundation
struct RegisterResponse : Codable {
	let token : String?
	let user : User?
    let bank : BankDetail?
    let university: Universities?

	enum CodingKeys: String, CodingKey {

		case token = "token"
		case user = "user"
        case bank = "bank"
        case university = "university"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		token = try values.decodeIfPresent(String.self, forKey: .token)
		user = try values.decodeIfPresent(User.self, forKey: .user)
        bank = try values.decodeIfPresent(BankDetail.self, forKey: .bank)
        university = try values.decodeIfPresent(Universities.self, forKey: .university)
	}

}

struct GetDetailsResponse : Codable {
    let token : String?
    let user : User?
}
