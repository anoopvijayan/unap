//
//  HomeRequest.swift
//  Unap
//
//  Created by Levin  on 12/12/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation

struct HomeRequestResponse: Codable{
    let requests:[HomeRequest]?
    
    enum CodingKeys: String, CodingKey {
        case requests = "data"
    }
    init(from decoder: Decoder) throws {
     let values = try decoder.container(keyedBy: CodingKeys.self)
     requests = try values.decodeIfPresent([HomeRequest].self, forKey: .requests)
    }
}


struct HomeRequest : Codable {
    let home_request_id : Int?
    let home_requested_created_timestamp : String?
    let home_request_created : String?
    let home : Home?
    let requested_by : User?
    let payment_message :String?
    let home_request_status: Int?
    let payment_status: Int?
    
    enum CodingKeys: String, CodingKey {
        
        case home_request_id = "home_request_id"
        case home_requested_created_timestamp = "home_requested_created_timestamp"
        case home_request_created = "home_request_created"
        case home = "home"
        case requested_by = "user_details"
        case payment_message = "payment_message"
        case home_request_status = "home_request_status"
        case payment_status = "payment_status"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        home_request_id = try values.decodeIfPresent(Int.self, forKey: .home_request_id)
        home_requested_created_timestamp = try values.decodeIfPresent(String.self, forKey: .home_requested_created_timestamp)
        home_request_created = try values.decodeIfPresent(String.self, forKey: .home_request_created)
        home = try values.decodeIfPresent(Home.self, forKey: .home)
        requested_by = try values.decodeIfPresent(User.self, forKey: .requested_by)
        payment_message = try values.decodeIfPresent(String.self, forKey: .payment_message)
        home_request_status = try values.decodeIfPresent(Int.self, forKey: .home_request_status)
        payment_status = try values.decodeIfPresent(Int.self, forKey: .payment_status)
        
    }
    
}


struct AcceptedListResponse: Codable{
    let requests:[AcceptedList]?
    
    enum CodingKeys: String, CodingKey {
        case requests = "data"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        requests = try values.decodeIfPresent([AcceptedList].self, forKey: .requests)
    }
}


struct AcceptedList : Codable {
    let payment_status : Int?
    let home_request_id : Int?
    let home_requested_created_timestamp : String?
    let home_request_created : String?
    let agreement_status : Int?
    let agreement_start_date : String?
    let agreement_start_date_timestamp : String?
    let agreement_end_date : String?
    let agreement_end_date_timestamp : String?
    let user_details : User?
    let home : Home?
    
    enum CodingKeys: String, CodingKey {
        
        case payment_status = "payment_status"
        case home_request_id = "home_request_id"
        case home_requested_created_timestamp = "home_requested_created_timestamp"
        case home_request_created = "home_request_created"
        case agreement_status = "agreement_status"
        case agreement_start_date = "agreement_start_date"
        case agreement_start_date_timestamp = "agreement_start_date_timestamp"
        case agreement_end_date = "agreement_end_date"
        case agreement_end_date_timestamp = "agreement_end_date_timestamp"
        case user_details = "user_details"
        case home = "home"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        payment_status = try values.decodeIfPresent(Int.self, forKey: .payment_status)
        home_request_id = try values.decodeIfPresent(Int.self, forKey: .home_request_id)
        home_requested_created_timestamp = try values.decodeIfPresent(String.self, forKey: .home_requested_created_timestamp)
        home_request_created = try values.decodeIfPresent(String.self, forKey: .home_request_created)
        agreement_status = try values.decodeIfPresent(Int.self, forKey: .agreement_status)
        agreement_start_date = try values.decodeIfPresent(String.self, forKey: .agreement_start_date)
        agreement_start_date_timestamp = try values.decodeIfPresent(String.self, forKey: .agreement_start_date_timestamp)
        agreement_end_date = try values.decodeIfPresent(String.self, forKey: .agreement_end_date)
        agreement_end_date_timestamp = try values.decodeIfPresent(String.self, forKey: .agreement_end_date_timestamp)
        user_details = try values.decodeIfPresent(User.self, forKey: .user_details)
        home = try values.decodeIfPresent(Home.self, forKey: .home)
    }
    
}
//Home TenantList

struct TenantListRequest : Codable {
    let payment_status : Int?
    let home_request_id : Int?
    let home_requested_created_timestamp : String?
    let home_request_created : String?
    let agreement_status : Int?
    let agreement_start_date : String?
    let agreement_start_date_timestamp : String?
    let agreement_end_date : String?
    let agreement_end_date_timestamp : String?
    let user_details : User?
    let home : Home?
    
    enum CodingKeys: String, CodingKey {
        
        case payment_status = "payment_status"
        case home_request_id = "home_request_id"
        case home_requested_created_timestamp = "home_requested_created_timestamp"
        case home_request_created = "home_request_created"
        case agreement_status = "agreement_status"
        case agreement_start_date = "agreement_start_date"
        case agreement_start_date_timestamp = "agreement_start_date_timestamp"
        case agreement_end_date = "agreement_end_date"
        case agreement_end_date_timestamp = "agreement_end_date_timestamp"
        case user_details = "user_details"
        case home = "home"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        payment_status = try values.decodeIfPresent(Int.self, forKey: .payment_status)
        home_request_id = try values.decodeIfPresent(Int.self, forKey: .home_request_id)
        home_requested_created_timestamp = try values.decodeIfPresent(String.self, forKey: .home_requested_created_timestamp)
        home_request_created = try values.decodeIfPresent(String.self, forKey: .home_request_created)
        agreement_status = try values.decodeIfPresent(Int.self, forKey: .agreement_status)
        agreement_start_date = try values.decodeIfPresent(String.self, forKey: .agreement_start_date)
        agreement_start_date_timestamp = try values.decodeIfPresent(String.self, forKey: .agreement_start_date_timestamp)
        agreement_end_date = try values.decodeIfPresent(String.self, forKey: .agreement_end_date)
        agreement_end_date_timestamp = try values.decodeIfPresent(String.self, forKey: .agreement_end_date_timestamp)
        user_details = try values.decodeIfPresent(User.self, forKey: .user_details)
        home = try values.decodeIfPresent(Home.self, forKey: .home)
    }
    
}

struct TenantListRequestResponse: Codable{
    let requests:[TenantListRequest]?
    
    enum CodingKeys: String, CodingKey {
        case requests = "data"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        requests = try values.decodeIfPresent([TenantListRequest].self, forKey: .requests)
    }
}

