//
//  BankDetails.swift
//  Unap
//
//  Created by Levin  on 24/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation



struct BankResponse : Codable {
    let status : Int
    let message : String?
    let response : BankDetail?
    
    enum CodingKeys: String, CodingKey {
        
        case status = "status"
        case message = "message"
        case response = "response"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)!
        message = try values.decodeIfPresent(String.self, forKey: .message)
        response = try values.decodeIfPresent(BankDetail.self, forKey: .response)
    }
    
}


struct BankDetail : Codable {
    let id : Int?
    let register_id : Int?
    let name : String?
    let account_number : Int?
    let sort_code : String?
    let address : String?
    let created_at : String?
    let updated_at : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case register_id = "register_id"
        case name = "name"
        case account_number = "account_number"
        case sort_code = "sort_code"
        case address = "address"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        register_id = try values.decodeIfPresent(Int.self, forKey: .register_id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        account_number = try values.decodeIfPresent(Int.self, forKey: .account_number)
        sort_code = try values.decodeIfPresent(String.self, forKey: .sort_code)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }
    
}
