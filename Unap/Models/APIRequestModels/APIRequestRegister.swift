//
//  userRegisterRequestInfo.swift
//  Unapp
//
//  Created by Levin  on 04/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation

struct APIRequestRegister:Codable,dictify {
    
    var name:String
    var age:String
    var password:String
    var user_type:String
    var device_token:String
    var device_type:String
    var email:String
    var university_id:String?
    
}

struct APISaveBank:Codable,dictify {
    
    var sort_code:String
    var account_number:String
    var address:String
    var name:String
}

struct APIRequestUpdateProfile:Codable,dictify{
    var first_name:String
    var dob:String
    var university_id:String?
}


struct APIRegisterHome:Codable,dictify{
    var accomodation_type:String
    var room_count:String
    var bed_count:String
    var bathroom_count:String
    var amenities:[String]
    var bill_included:[String]
    var home_price:String
    var lattitude:String
    var longitude:String
    var description:String
    var home_title:String
    var currency_code:String
}
