//
//  PaymentAPI.swift
//  Unap
//
//  Created by Aneesh on 12/28/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation
import Alamofire

enum PaymentAPI {
    case requestBraintreeClientToken()
    case requestBraintreePayment(nonce:String,amount:String)
    
}


extension PaymentAPI:Endpoint{
    var path: String {
        switch self{
        case .requestBraintreeClientToken(_):
            return "/api/generate/client-token"
        case .requestBraintreePayment(_,_):
            return "/api/client/transaction"
        }
        
        
    }
    
    var method: HTTPMethod {
        switch self{
            
        case .requestBraintreeClientToken(_):
            return .get
        case .requestBraintreePayment(_):
            return .post
        }
        
    }
    
    var headers: HTTPHeaders{
        
        if let auth = UserDefaultsManager.authToken{
            let token = "Bearer" + " " + auth
            return ["Accept":"application/json","Authorization":token]
        }
        
        return [:]
    }
    
    var body: Parameters {
        var body: Parameters = Parameters()
        switch self {
        case .requestBraintreePayment(let nonce, let amount):
            body["nonce"] = nonce
            body["amount"] = amount
            
        case .requestBraintreeClientToken(_):
            break
        
        }
        return body
    }
    
    var name: String{
        return "Payment API"
    }
}
