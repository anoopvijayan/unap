//
//  BooksApiGroup.swift
//  Shulph
//
//  Created by Levin  on 30/11/17.
//  Copyright © 2017 Ileaf Solutions. All rights reserved.
//

import Foundation
import Alamofire

enum OnboardingEndpoints {

  
    case createAccount(data:APIRequestRegister)
    case login(email:String,password:String)
    case reset(email:String)
    case uploadDoc(name:String,type:String,data:Data)
    case getUniversityList()
}

extension OnboardingEndpoints: Endpoint {
    
    
    var path: String {
        switch self{
            
        case .createAccount(_):
            return "/api/register"
        case .login(_,_):
            return "/api/user/login"
        case .reset(_):
            return "/api/forgotpassword"
        case .uploadDoc(_,_,_):
            return "/api/documents/upload"
        case .getUniversityList():
            return "/api/student/all-universities"
        }
        
        
    }
    
    var method: HTTPMethod {
        switch self{
            
        case .createAccount(_):
            return .post
        case .login(_,_):
            return .post
        case .reset(_):
            return .post
        case .uploadDoc(_,_,_):
            return .post
        case .getUniversityList():
            return .get
        }
    }
    
    var headers: HTTPHeaders{
        
        if let auth = UserDefaultsManager.authToken{
            let token = "Bearer" + " " + auth
            return ["Accept":"application/json","Authorization":token]
        }
        
        return [:]
    }
    
    var body: Parameters {
        var body: Parameters = Parameters()
        switch self {

        case.createAccount(let data):
            body = data.todict() ?? Parameters()
            
        
        case .login(let email, let password):
            body["email"] = email
            body["password"] = password
            body["device_type"] = Config.deviceOS
            if let device_token = UserDefaultsManager.deviceToken{
                body["device_token"] = device_token
            }
            
        case .reset(let email):
            body["email"] = email
            
        case .uploadDoc(let name, let type, _):
            body["document_name"] = name
            body["document_type"] = type
            
        case .getUniversityList():break

    }
         return body
    }
    
   var image:[String:Data] {
    var body:[String:Data] = [:]
    switch self {
   
    case .uploadDoc(_,_, let data):
        body["document"] = data
    default: return body
        
    }
    return body
    }
    
    var name: String{
        return "API:Create-Account"
    }
}
