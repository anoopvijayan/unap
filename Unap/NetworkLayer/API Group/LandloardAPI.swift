//
//  LandloardAPI.swift
//  Unap
//
//  Created by Levin  on 23/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation
import Alamofire

enum LandloardAPI {
    
    case getUserDetails
    case uploadDoc(name:String,type:String,data:Data)
    case saveBankDetails(model:APISaveBank)
    case updateProfile(model:APIRequestUpdateProfile,data:Data?)
    case createHome(model:APIRegisterHome)
    case uploadHomeImage(homeId:String,data:Data)
    case listAllHomes(page:String)
    case logout
    case listAllRequest
    case request(action:notificationActionType,requestID:Int)
    case acceptedList
    case tenantList
    
}

extension LandloardAPI: Endpoint {
    
    
    var path: String {
        switch self{
            
        case .getUserDetails:
            return "/api/user/details"
        case .uploadDoc(_,_,_):
            return "/api/documents/upload"
        case .saveBankDetails(_):
            return "/api/save/bank"
        case .updateProfile(_,_):
            return "/api/update/profile"
        case .createHome(_):
            return "/api/landlord/register/home"
        case .uploadHomeImage(let homeID, _):
            return "/api/upload/home/image/\(homeID)"
            
        case .listAllHomes(let page):
            return "/api/landlord/all/home?page=\(page)"
            
        case .logout:
            return "/api/logout_user"
        case .listAllRequest:
            return "/api/landlord/request/list-all-request"
            
        case .request(_,let requestID):
            return "/api/landlord/process/student-request/\(requestID)"
            
        case .acceptedList:
            return "/api/landlord/request/accepted-list"
        case .tenantList:
            return "/api/landlord/tenants-list"
            
        }
        
        
    }
    
    var method: HTTPMethod {
        switch self{
            
        case .getUserDetails:return .get
        case .uploadDoc(_,_,_):return .post
        case .saveBankDetails(_):return .post
        case .updateProfile(_,_):return .post
        case .createHome(_):return .post
        case .uploadHomeImage(_,_):return .post
        case .listAllHomes(_):return .get
        case .logout:return .get
        case .listAllRequest:return .get
        case .request(_,_): return .post
        case .acceptedList: return .get
        case .tenantList: return .get
        }
    }
    
    var headers: HTTPHeaders{
        
        if let auth = UserDefaultsManager.authToken{
            let token = "Bearer" + " " + auth
            return ["Content-Type":"application/json","Authorization":token,"cache-control":"no-cache"]
        }
        
        return [:]
    }
    
    var body: Parameters {
        var body: Parameters = Parameters()
        switch self {
            
        case.getUserDetails:break
            
        case .uploadDoc(let name, let type, _):
            body["document_name"] = name
            body["document_type"] = type
            break
            
        case .saveBankDetails(let model):
            body = model.todict() ?? Parameters()
            break
            
        case .updateProfile(let model,_):
            body  = model.todict() ?? Parameters()
            break
            
        case .createHome(let model):
            body = model.todict() ?? Parameters()
            
        case .uploadHomeImage(_,_):break
            
        case .listAllHomes(_):break
            
        case .logout:break
            
        case .listAllRequest:break
            
        case .acceptedList:break
            
        case .tenantList:break
            
        case .request(let action,_):
            body["action"] = "\(action.rawValue)"
            break
            
        }
        return body
    }
    
    var image:[String:Data] {
        var body:[String:Data] = [:]
        switch self {
            
        case .uploadDoc(_,_, let data):
            body["document"] = data
            break
        case .updateProfile(_, let data):
            
            guard let imageData = data else {return body}
            body["profile_pic"] = imageData
            break
            
        case .uploadHomeImage(_, let data):
            body["home_image"] = data
            break
            
        default: return body
            
        }
        return body
    }
    
    var name: String{
        return "API:LandLoard API"
    }
}

protocol userDetailsGetableAPI {
    func getUseDetails(compeletion:(_ user:User)->())
}

extension userDetailsGetableAPI{
    
    
    
}
