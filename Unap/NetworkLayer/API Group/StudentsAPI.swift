//
//  StudentsAPI.swift
//  Unap
//
//  Created by Levin  on 13/11/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation
import Alamofire

enum StudentsAPI {
    
    case searchHome(lat:String,long:String,page:String)
    case requestHome(homeID:String)
    case acceptedHomeList()
    
}


extension StudentsAPI:Endpoint{
    var path: String {
        switch self{
            
        case .searchHome(_,_,let page):
            return "/api/search/home?page=\(page)"
            
        case .requestHome(let homeID):
            return "/api/student/request-home/\(homeID)"
        case .acceptedHomeList():
            return "/api/student/home/request-accepted"
        }
        
        
    }
    
    var method: HTTPMethod {
        switch self{
            
        case .searchHome(_, _,_):
            return .post
        case .requestHome(_):
            return .get
        case .acceptedHomeList():
            return .get
        }
        
    }
    
    var headers: HTTPHeaders{
        
        if let auth = UserDefaultsManager.authToken{
            let token = "Bearer" + " " + auth
            return ["Accept":"application/json","Authorization":token]
        }
        
        return [:]
    }
    
    var body: Parameters {
        var body: Parameters = Parameters()
        switch self {
        case .searchHome(let lat, let long,_):
            body["lattitude"] = lat
            body["longitude"] = long
            
        case .requestHome(_):
            break
        case .acceptedHomeList():
            break
        }
        return body
    }
    
    var name: String{
        return "Students API"
    }
}
