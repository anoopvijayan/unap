//
//  NetworkManager.swift
//  BBCream
//
//  Created by Levin  on 20/06/17.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


public struct NetworkManager{
    
    public init() {
    }
    
    public var manager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
        let sessionManager = Alamofire.SessionManager(configuration: configuration)
        return sessionManager
    }()

    public static var shared = NetworkManager()
     
   
    }



public enum NetworkResult<modelClass:Codable>{
    case success(data:modelClass)
    case error(error:Error)
}


extension NetworkManager{
    
    
    public func request<modelClass:Codable>(endpoint:Endpoint,completion:@escaping (_ response:NetworkResult<modelClass>)->()){
        URLCache.shared.removeAllCachedResponses()
        let body = endpoint.method == .get ? nil : endpoint.body
        
      Alamofire.request(endpoint.fullURL, method: endpoint.method, parameters: body, encoding: endpoint.encoding, headers: endpoint.headers).responseData { (responseData) in
            
            endpoint.log()
            
            switch responseData.result{
                
            case .success(let data):
               
//                guard let data = data.data else {
//                    completion(.error(error: NetworkError.invalidAPIResponse()))
//                    return
//                }
                do{
                    let model = try self.decode(to: modelClass.self, from: data)
                    completion(.success(data: model))
                }catch (let error){
                    completion(.error(error: error))
                }
                
            case .failure(let error):
                completion(.error(error: error))
                break
            }
            }.responseString { (response) in
                  print("response:%@",response.value)
        }
   
    }
    
    public func requestForm<modelClass:Codable>(endpoint:Endpoint,completion:@escaping (_ response:NetworkResult<modelClass>)->()){
        
        endpoint.log()
        
        Alamofire.upload(multipartFormData: { (MultipartFormData) in
            for (key, value) in endpoint.body {
                
                if let newValue = value as? [String]{
                    if let jsonArr = try? JSONEncoder().encode(newValue){
                    
                    MultipartFormData.append(jsonArr, withName: key)
                    }
                }else{
                
                MultipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
                }
            }
             for (key, value) in endpoint.image {
                MultipartFormData.append(value, withName: "\(key)", fileName: "\(Date().timeIntervalSince1970).jpg", mimeType: "image/jpg")
            }
            
            
            
        }, usingThreshold: UInt64.init(), to: endpoint.fullURL, method: endpoint.method, headers: endpoint.headers) { (result) in
            
            switch result{
            case .success(let upload, _, _):
                upload.responseData { response in
                    print("Succesfully uploaded")
                    if let err = response.error{
                     completion(.error(error: err))
                        return
                    }
                    guard let data = response.data else {
                        completion(.error(error: NetworkError.invalidAPIResponse()))
                        return
                    }
                    do{
                        let model = try self.decode(to: modelClass.self, from: data)
                        completion(.success(data: model))
                    }catch (let error){
                        completion(.error(error: error))
                    }
                   
                    }.responseString(completionHandler: { (response) in
                        print("response:%@",response.value)
                
                    }).responseJSON(completionHandler: { (reponse) in
                        print("JSON:%@",reponse.value)
                    })
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                completion(.error(error: error))
            }
            
        }
        
      
        
    }


}

//MARK:- Data Parsing
extension NetworkManager{
    
    func decode<T:Codable>(to:T.Type,from:Data) throws -> T{
        do{
        let decoder = JSONDecoder()
            if to == APICommonResponse.self{
                let model = try decoder.decode(T.self, from: from)
                return model
            }
        let model = try decoder.decode(T.self, from: try checkForResponseStatus(data: from))
            return model
        }catch (let error){
            throw error
        }
    }
    
    func checkForResponseStatus(data:Data) throws -> Data{
        
        if JSON(data)["status"].int == 1 {
            return try JSON(data)["response"].rawData()
        }else if JSON(data)["status"].int == 0 {
            throw NetworkError(apiError: JSON(data)["message"].string ?? "Something went wrong.")
        }
        throw NetworkError(validationError: "Validation error")
    }
}
