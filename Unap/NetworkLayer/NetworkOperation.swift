//
//  NetworkOperation.swift
//  Unapp
//
//  Created by Levin  on 12/09/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation

class networkAsyncOperation:Operation{
    
    var  done:Bool
    var  running:Bool
    
    override init() {
        self.done = false
        self.running = false
    }
    
    override func start() {
        if self.isCancelled{
            if !self.isFinished { self.done = true }
            return
        }
        self.willChangeValue(forKey: "isExecuting")
        self.running = true
        self.didChangeValue(forKey: "isExecuting")
        main()
    }
    
    func completeOperation(){
        if self.isExecuting {
            self.willChangeValue(forKey: "isExecuting")
            self.running = false
            self.didChangeValue(forKey: "isExecuting")
        }
        if !self.isFinished {
            self.willChangeValue(forKey: "isFinished")
            self.done = true
            self.didChangeValue(forKey: "isFinished")
        }
    }
   
    //MARK: - NSOperation methods
    override var isAsynchronous: Bool{
        return true
    }
    
    override var isExecuting: Bool{
        return self.running
    }
    
    override var isFinished: Bool{
        return self.done
    }
    
    override var isConcurrent: Bool {
        get {
            return true
        }
    }
    
    
}

class NetworkOperaton:networkAsyncOperation{
    typealias NetworkOperationCompletion = (_ item:APICommonResponse?)->()
    let enpoint:Endpoint
    let compeletion:NetworkOperationCompletion
    
   init(endpoint:Endpoint,compeletion:@escaping NetworkOperationCompletion) {
        self.enpoint = endpoint
        self.compeletion = compeletion
       
    }
    
    override func main() {
        NetworkManager.shared.requestForm(endpoint: self.enpoint) { (result:NetworkResult<APICommonResponse>) in
           
            switch result{
                
            case .success(let data):
                self.compeletion(data)
                break
            case .error(let error):
                self.logError(error)
               self.compeletion(nil)
                break
            }
             self.completeOperation()
        }
    }
}

///Eg:
//let queue = OperationQueue()
//        queue.maxConcurrentOperationCount = 4
//
//       let op2 = NetworkOperaton(endpoint: BooksEndpoints.bookstore(action: "get-euser-books", userID: "91889781")) { (item) in
//             self.logDone("====ITEM 2 DONE======")
//
//        }
//        let op3 = NetworkOperaton(endpoint: BooksEndpoints.bookstore(action: "get-user-books", userID: "91889781")) { (item) in
//             self.logDone("====ITEM 3 DONE====")
//        }
//
//        let op1 = NetworkOperaton(endpoint: BooksEndpoints.bookstore(action: "get-user-books", userID: "91889781")) { (item) in
//
//            self.logDone("=====ITEM 1 DONE=====")
//        }
//
//        op2.addDependency(op1)
//        op3.addDependency(op2)
//
//        queue.addOperations([op1,op2,op3], waitUntilFinished: false)
