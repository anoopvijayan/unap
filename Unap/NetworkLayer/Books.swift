//
//  Booksl.swift
//  Shulph
//
//  Created by Levin  on 28/11/17.
//  Copyright © 2017 Ileaf Solutions. All rights reserved.
//

import Foundation
import UIKit


struct APICommonResponse: Codable{
    let status : Int
    let message : String
    let response : response?
}

struct response: Codable{
   let user : User?
   let home : Home?
   let home_img : [Home_images]?
}


struct SuperBooks :Codable{
    let status : Int
    let message : String
    let books :[Books]
}

struct Books : Codable {
	let id : String?
	let title : String?
	let subTitle : String?
	let description : String?
	let paperbackPages : String?
	let hardbackPages : String?
	let epubIsbnNumber : String?
	let paperbackIsbnNumber : String?
	let hardbackIsbnNumber : String?
	let creatorId : String?
	let imprintId : String?
	let epubFile : String?
	let coverImage : String?
	let epubFileUrl : String?
	let coverImageUrl : String?
	let ebookPrice : String?
	let paperbackPrice : String?
	let hardbackPrice : String?
	let bundlePrice : String?
	let publisherId : String?
	let scheduledReleaseDate : String?
	let uploadDate : String?
	let lastModified : String?
	let slug : String?
	let status : String?
	let authors : String?
	let genres : String?
	let publisher : String?
    var userHasBook:Bool?
    let shipping_fee:String?
    let file_size:String?
    let imprint:String?
    let delivery_duration:String?
    let displayTitle:String?
    
	enum CodingKeys: String, CodingKey {

		case id = "id"
		case title = "title"
		case subTitle = "subTitle"
		case description = "description"
		case paperbackPages = "paperbackPages"
		case hardbackPages = "hardbackPages"
		case epubIsbnNumber = "epubIsbnNumber"
		case paperbackIsbnNumber = "paperbackIsbnNumber"
		case hardbackIsbnNumber = "hardbackIsbnNumber"
		case creatorId = "creatorId"
		case imprintId = "imprintId"
		case epubFile = "epubFile"
		case coverImage = "coverImage"
		case epubFileUrl = "epubFileUrl"
		case coverImageUrl = "coverImageUrl"
		case ebookPrice = "ebookPrice"
		case paperbackPrice = "paperbackPrice"
		case hardbackPrice = "hardbackPrice"
		case bundlePrice = "bundlePrice"
		case publisherId = "publisherId"
		case scheduledReleaseDate = "scheduledReleaseDate"
		case uploadDate = "uploadDate"
		case lastModified = "lastModified"
		case slug = "slug"
		case status = "status"
		case authors = "authors"
		case genres = "genres"
		case publisher = "publisher"
        case userHasBook = "userHasBook"
        case shipping_fee = "shipping_fee"
        case file_size    = "file_size"
        case imprint      = "imprint"
        case delivery_duration = "delivery_duration"
        case displayTitle = "displayTitle"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		title = try values.decodeIfPresent(String.self, forKey: .title)
		subTitle = try values.decodeIfPresent(String.self, forKey: .subTitle)
		description = try values.decodeIfPresent(String.self, forKey: .description)
		paperbackPages = try values.decodeIfPresent(String.self, forKey: .paperbackPages)
		hardbackPages = try values.decodeIfPresent(String.self, forKey: .hardbackPages)
		epubIsbnNumber = try values.decodeIfPresent(String.self, forKey: .epubIsbnNumber)
		paperbackIsbnNumber = try values.decodeIfPresent(String.self, forKey: .paperbackIsbnNumber)
		hardbackIsbnNumber = try values.decodeIfPresent(String.self, forKey: .hardbackIsbnNumber)
		creatorId = try values.decodeIfPresent(String.self, forKey: .creatorId)
		imprintId = try values.decodeIfPresent(String.self, forKey: .imprintId)
		epubFile = try values.decodeIfPresent(String.self, forKey: .epubFile)
		coverImage = try values.decodeIfPresent(String.self, forKey: .coverImage)
		epubFileUrl = try values.decodeIfPresent(String.self, forKey: .epubFileUrl)
		coverImageUrl = try values.decodeIfPresent(String.self, forKey: .coverImageUrl)
		ebookPrice = try values.decodeIfPresent(String.self, forKey: .ebookPrice)
		paperbackPrice = try values.decodeIfPresent(String.self, forKey: .paperbackPrice)
		hardbackPrice = try values.decodeIfPresent(String.self, forKey: .hardbackPrice)
		bundlePrice = try values.decodeIfPresent(String.self, forKey: .bundlePrice)
		publisherId = try values.decodeIfPresent(String.self, forKey: .publisherId)
		scheduledReleaseDate = try values.decodeIfPresent(String.self, forKey: .scheduledReleaseDate)
		uploadDate = try values.decodeIfPresent(String.self, forKey: .uploadDate)
		lastModified = try values.decodeIfPresent(String.self, forKey: .lastModified)
		slug = try values.decodeIfPresent(String.self, forKey: .slug)
		status = try values.decodeIfPresent(String.self, forKey: .status)
		authors = try values.decodeIfPresent(String.self, forKey: .authors)
		genres = try values.decodeIfPresent(String.self, forKey: .genres)
		publisher = try values.decodeIfPresent(String.self, forKey: .publisher)
        userHasBook = try values.decodeIfPresent(Bool.self, forKey: .userHasBook)
        shipping_fee = try values.decodeIfPresent(String.self, forKey: .shipping_fee)
        file_size = try values.decodeIfPresent(String.self, forKey: .file_size)
        imprint = try values.decodeIfPresent(String.self, forKey: .imprint)
        delivery_duration = try values.decodeIfPresent(String.self, forKey: .delivery_duration)
        displayTitle =  try values.decodeIfPresent(String.self, forKey: .displayTitle)
	}
    
    

}
