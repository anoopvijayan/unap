//
//  Logger.swift
//  Unapp
//
//  Created by Levin  on 12/09/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation

/// A very light weight logging tool to help with debugging (debug build only).
/// Supports logging enabled/disabled functionality.
/// Production mode logging is disabled.
class Logger {
    var isEnabled:Bool
    private init() {
        isEnabled = true
    }
   static let shared = Logger()
}

public extension Endpoint{
    
    func log(){
        guard Logger.shared.isEnabled else { return }
        #if DEBUG
        print("🔷------API:\(name)-------")
        print("Header:\(headers)")
        print("URL:\(fullURL))")
        print("Method:\(method)")
        print("Params:\(body))")
        print("------------END API-------------------🔷")
        #endif
    }
    
    
}
extension NSObject{
    func logDone(_ val:Any){
        guard Logger.shared.isEnabled else { return }
        #if DEBUG
        print("✅\(val)✅")
        #endif
    }
    
    func logError(_ val:Error){
        guard Logger.shared.isEnabled else { return }
        #if DEBUG
        print("❌ Error: \(val.localizedDescription) ❌")
        #endif
    }
    
    func logInfo(_ val:Any){
        guard Logger.shared.isEnabled else { return }
        #if DEBUG
        print("🔶\(val)🔶")
        #endif
    }
 
}

extension CoreDataStack{
    func logDone(_ val:Any){
        guard Logger.shared.isEnabled else { return }
        #if DEBUG
        print("✅\(val)✅")
        #endif
    }
    
    func logError(_ val:Error){
        guard Logger.shared.isEnabled else { return }
        #if DEBUG
        print("❌ Error: \(val.localizedDescription) ❌")
        #endif
    }
    
    func logInfo(_ val:Any){
        guard Logger.shared.isEnabled else { return }
        #if DEBUG
        print("🔶\(val)🔶")
        #endif
    }
    
}


extension NetworkManager{
    func logData(_ val:Data){
        guard Logger.shared.isEnabled else { return }
        #if DEBUG
        do {
            let response = try JSONSerialization.jsonObject(with: val, options: []) as? [String:Any]
            guard let jsonString = response?.prettyPrintedJSON else {
                print("Can't create string with data.")
                return
            }
            print("🔶-------------Response:Start--------")
            print(jsonString)
            print("-------------Response:End--------🔶")
            
        } catch let parseError {
            print("json serialization error: \(parseError)")
            
        }
        #endif
    }
}

extension Dictionary {
    var prettyPrintedJSON: String? {
        do {
            let data: Data = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(data: data, encoding: .utf8)
        } catch _ {
            return nil
        }
    }
}
