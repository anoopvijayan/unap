//
//  UIViewController+Alert.swift
//  Shulph
//
//  Created by Levin  on 21/11/17.
//  Copyright © 2017 Ileaf Solutions. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    ///Shows a alert ViewController.
    /// - parameter message: A String that represent an the message to be displayed.
    func showAlertMessage(message:String) {
        let alert = UIAlertController(title: Config.appName, message: message, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: false, completion: nil)
    }
    
    func showAlertMessage(title:String,message:String,btnName:String,btnCallback:@escaping (_ actions:UIAlertAction)->()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let action1 = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(action1)
        let action2 = UIAlertAction(title: btnName, style: .default) { (action) in
            btnCallback(action)
        }
        alert.addAction(action2)
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension UIViewController {
    //MARK:- Indication Screen, Success, error, Activity Indicator
    var successScreen:SuccessScreen? {
        get{return SuccessScreen.loadFromNib()}
        set{                                                    }}
    //Show
    func showIndicator(indicatorType:NotifyType)  {
        guard let indicator = successScreen else { return}
        indicator.animatePin(loopEnable: true, holderViewBgColor: .clear, titleHidden: true, notificationText: "", type: indicatorType,size: CGSize(width: 100, height: 100))
        indicator.tag = 1000
        let window = UIApplication.shared.keyWindow!
        window.addSubview(indicator)
        indicator.frame = window.bounds
        indicator.center = window.center
    }
    
    func showSendRequestSuccess(messages:String) {
        guard let indicator = successScreen else { return}
        indicator.animatePin(loopEnable: false, holderViewBgColor: .white, titleHidden: false, notificationText: messages, type: .success, size: CGSize(width: 150, height: 150))
        let window = UIApplication.shared.keyWindow!
        window.addSubview(indicator)
        indicator.frame = window.bounds
        indicator.center = window.center
    }
    
    //Hide
    func hidePopUp() {
        guard let indicator = successScreen else { return}
        indicator.animationView.pause()
        UIApplication.shared.keyWindow!.viewWithTag(1000)?.removeFromSuperview()
    }
}
