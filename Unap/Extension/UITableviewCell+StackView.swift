//
//  UITableviewCell+StackView.swift
//  Unap
//
//  Created by Aneesh on 11/19/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation
import UIKit


public extension UITableViewCell {
    func addArrangedViews(dataArray:[Any], stackView:UIStackView,isAmenties:Bool)  {
        stackView.arrangedSubviews.forEach { (items) in
            items.removeFromSuperview()
        }
        
        for (index,_) in dataArray.enumerated(){
            print(index)
            
            let horizoanl = UIStackView()
            let imageIconName:String!
            if isAmenties{
                let object:Amenities = dataArray[index] as! Amenities
                imageIconName = object.amenity?.clearSpaces
                
            }else{
                let object:Bills = dataArray[index] as! Bills
                imageIconName = object.bill_included?.clearSpaces
            }
            let ImgVw:UIImageView = UIImageView(image: UIImage(named: imageIconName.lowercased()))
            ImgVw.heightAnchor.constraint(equalToConstant: 32.0).isActive = true
            ImgVw.widthAnchor.constraint(equalToConstant: 32.0).isActive = true
            ImgVw.contentMode = .scaleAspectFit
            
            //Text Label
            let textLabel               = UILabel()
            if let font = UIFont(name: "Avenir Next Condensed", size: 17) {
                textLabel.font = font
            }
            textLabel.heightAnchor.constraint(equalToConstant: 25.0).isActive = true
            textLabel.text  = imageIconName.uppercased()
            textLabel.textAlignment = .center
            
            horizoanl.addArrangedSubview(ImgVw)
            horizoanl.addArrangedSubview(textLabel)
            horizoanl.spacing = 10
            stackView.addArrangedSubview(horizoanl)
        }
    }
}
