//
//  UIColor+Unap.swift
//  Unapp
//
//  Created by Levin  on 03/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation
import UIKit

extension(UIColor){
    
    class var blueUN: UIColor {
        return UIColor(red: 24/255.0, green: 90/255.0, blue: 157/255.0, alpha: 1.0)
    }
    
    class var greenUN: UIColor {
        return UIColor(red: 67/255.0, green: 206/255.0, blue: 162/255.0, alpha: 1.0)
    }
    
    
}
