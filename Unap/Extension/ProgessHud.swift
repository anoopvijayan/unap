//
//  SVProgessHudHandler.swift
//  Shulph
//
//  Created by Levin on 15/05/17.
//  Copyright © 2017 Jaison Joseph. All rights reserved.
//

import UIKit
import SVProgressHUD

extension UIView{
    func showHud(message:String){
        SVProgressHUD.setDefaultStyle(.custom)
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.setFont(UIFont.systemFont(ofSize: 18.0, weight: UIFont.Weight.medium))
        SVProgressHUD.setBackgroundColor(UIColor.white)
        SVProgressHUD.setForegroundColor(.blueUN)
        SVProgressHUD.show(withStatus: message)
        //SVProgressHUD.show()
    
    }
    
    func showHud(message:String,progress:Float){
        SVProgressHUD.setDefaultStyle(.custom)
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.setFont(UIFont.systemFont(ofSize: 18.0, weight: UIFont.Weight.medium))
        SVProgressHUD.setBackgroundColor(UIColor.white)
        SVProgressHUD.setForegroundColor(.blueUN)
        SVProgressHUD.show(withStatus: message)
        //SVProgressHUD.show()
        SVProgressHUD.showProgress(progress, status: message)
        
    }
    
    func showSuccess(message:String){
        SVProgressHUD.setDefaultStyle(.custom)
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.setFont(UIFont.systemFont(ofSize: 18.0, weight: UIFont.Weight.medium))
        SVProgressHUD.setBackgroundColor(UIColor.white)
        SVProgressHUD.setForegroundColor(.blueUN)
        SVProgressHUD.setSuccessImage(#imageLiteral(resourceName: "check"))
        SVProgressHUD.setMinimumDismissTimeInterval(0.5)
        SVProgressHUD.setMinimumSize(CGSize(width: 100, height: 100))
        SVProgressHUD.setImageViewSize(CGSize(width: 79.5/2, height: 58.5/2))
        
        SVProgressHUD.showSuccess(withStatus: message)
        
    }
    
   func hideHud() {
    SVProgressHUD.dismiss()
   
    }
}
