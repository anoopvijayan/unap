//
//  AppDelegate.swift
//  Unapp
//
//  Created by Levin  on 11/09/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import UIKit
import CoreData
import Alamofire
import UserNotifications
import GooglePlaces
import GoogleMaps
import Quickblox



@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


     func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        GMSPlacesClient.provideAPIKey("AIzaSyDU1U59046QEcj-x8jgHQFz2yVf8OHlcbw")
        GMSServices.provideAPIKey("AIzaSyDU1U59046QEcj-x8jgHQFz2yVf8OHlcbw")
        Logger.shared.isEnabled = true
       // registerForPushNotification()
        setInitialViewController()
        
        QBSettings.applicationID = 75213
        QBSettings.authKey = "qESOhbA8ux5scKX"
        QBSettings.authSecret = "XS6bCgYhwMMf4SQ"
        QBSettings.accountKey = "x2Sji_TiVmmeYor8CNbY"
        QBSettings.networkIndicatorManagerEnabled = true
        QBSettings.streamManagementSendMessageTimeout = 10
        QBSettings.logLevel = .debug
        
       
        
        
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {

    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Logging out from chat.
        // ServicesManager.instance().chatService.disconnect(completionBlock: nil)
        if let _ = UserDefaultsManager.user{
            QBChat.instance.disconnect { (error) in
                
            }
        }
         CoreDataStack.shared.saveContext()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        if let userProfile = UserDefaultsManager.user{
            if let current = QBSession.current.currentUser{
                
                current.password = Config.QuickBloxUserPassword
                ChatManager.shared.connectionDelegate?.chatWillConnect()

                QBChat.instance.connect(withUserID: UInt(userProfile.qblocks_id!)!, password: Config.QuickBloxUserPassword) { (error) in
                    if (error != nil){
                        print(error?.localizedDescription ?? "connect error")
                    }
                }
                
            }
        }

    }

    func applicationDidBecomeActive(_ application: UIApplication) {

    }

    func applicationWillTerminate(_ application: UIApplication) {
        if let _ = UserDefaultsManager.user{
            
            QBChat.instance.disconnect { (error) in
                
            }
        }
    }
    
    
    
    
   
}

//MARK:- Push Notification Setup
extension AppDelegate:UNUserNotificationCenterDelegate{
    
    func registerForPushNotification(){
        
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: { granted, error in
            DispatchQueue.main.async {
                if granted {
                    UIApplication.shared.registerForRemoteNotifications()
                }
                else {
                    //Do stuff if unsuccessful...
                }
            }
        })
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        let token = tokenParts.joined()
        logInfo("DeviceToken:\(token)")
        UserDefaultsManager.deviceToken = token
        UserDefaultsManager.deviceTokenData = deviceToken
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        self.logError(error)
    }
    
    // called when user interacts with notification (app not running in foreground)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse, withCompletionHandler
        completionHandler: @escaping () -> Void) {
        
        // do something with the notification
        print(response.notification.request.content.userInfo)
        
        // the docs say you should execute this asap
        return completionHandler()
    }
    
    
    // called if app is running in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent
        notification: UNNotification, withCompletionHandler completionHandler:
        @escaping (UNNotificationPresentationOptions) -> Void) {
        
        // show alert while app is running in foreground
        return completionHandler(UNNotificationPresentationOptions.alert)
    }
}

//MARK: - Routing Logic
extension AppDelegate{
    
    func setInitialViewController(){
        
        guard let userDetails =  UserDefaultsManager.user,
              let type = userDetails.user_type
              else { return }
        
        switch type{
            
        case .landlord:self.setupLandLoardhomeAsRoot()
            
        case .student:self.setupStudentHomeAsRoot()
            
        }
        
    }
    
    func setupLandLoardhomeAsRoot(){
        let vc = UIStoryboard(name: "HomeLandLoard", bundle: nil).instantiateViewController(withIdentifier: "HomeTabbarController")
        self.window?.rootViewController = vc
        
    }
    
    func setupStudentHomeAsRoot(){
         let vc = UIStoryboard(name: "StudentHome", bundle: nil).instantiateViewController(withIdentifier: "studentTabBarController")
        self.window?.rootViewController = vc
    }
}
