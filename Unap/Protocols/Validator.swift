//
//  Validator.swift
//  FairyHub
//
//  Created by Levin  on 27/03/18.
//  Copyright © 2018 Ileaf Solutions. All rights reserved.
//

import Foundation


protocol Validator {
    
    func validate(values: (type: ValidationType, inputValue: String)...) -> Valid
    
}

extension Validator {
    
    func validate(values: (type: ValidationType, inputValue: String)...) -> Valid{
        
        for valueToBeChecked in values {
            
            switch valueToBeChecked.type {
            case .email:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .email, .emptyEmail, .inValidEmail)) {
                    return tempValue
                }
            case .stringWithFirstLetterCaps:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .alphabeticStringFirstLetterCaps, .emptyFirstLetterCaps, .invalidFirstLetterCaps)) {
                    return tempValue
                }
            case .phoneNo:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .phoneNo, .emptyPhone, .inValidPhone)) {
                    return tempValue
                }
            case .alphabeticString:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .alphabeticStringWithSpace, .emptyAlphabeticString, .invalidAlphabeticString)) {
                    return tempValue
                }
            case .countryCode:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .countryCode, .invalideCode, .invalideCode)) {
                    return tempValue
                }
                
            case .password:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .password, .emptyPSW, .inValidPSW)) {
                    return tempValue
                }
                
            case .empty:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .countryCode, .emptyString, .emptyString)) {
                    return tempValue
                }
                
            case .firstName:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .countryCode, .emptyFirstName, .emptyFirstName)) {
                    return tempValue
                }
            case .lastName:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .countryCode, .emptyLastName, .emptyLastName)) {
                    return tempValue
                }
                
            case .age:
                if let temp = isValidAge(text: valueToBeChecked.inputValue, emptyAlert: .emptyAge, invalidAlert: .invalideCode){
                    return temp
                }
                
            case .passwordUnap:
            
                if let temp = isPassWordValid(text: valueToBeChecked.inputValue){
                    return temp
                }
                
            case .confirmPassword:
                let filteredResult = values.filter{$0.type == ValidationType.password}
                if let password = filteredResult.first?.inputValue{
                    if password == valueToBeChecked.inputValue {
                        return Valid.success
                    } else {
                        return Valid.failure(.error,.invalidConfirmPassword)
                    }
                    
                }
                if let tempValue = isValidString((valueToBeChecked.inputValue, .countryCode, .emptyConfirmPassword, .emptyConfirmPassword)) {
                    return tempValue
                }
                
                
            }
            
            
        }
        return .success
    }
    
    func isValidString(_ input: (text: String, regex: RegEx, emptyAlert: AlertMessages, invalidAlert: AlertMessages)) -> Valid? {
        if input.text.isEmpty {
            return .failure(.error, input.emptyAlert)
        } else if isValidRegEx(input.text, input.regex) != true {
            return .failure(.error, input.invalidAlert)
        }
        return nil
    }
    
    func isValidAge(text:String,emptyAlert:AlertMessages,invalidAlert:AlertMessages)-> Valid?{
        
        if text.isEmpty{
             return .failure(.error, .emptyAge)
        }else if Int(text)! <= 18  {
            return .failure(.error, .invalidAge)
        }else if Int(text)! >= 100 {
           return .failure(.error, .invalidAge)
        }else{
            return nil
        }
    }
    
    func isPassWordValid(text:String)->Valid?{
    if text.isEmpty{
        return .failure(.error, .emptyPSW)
    }else if text.count < 7 {
        return .failure(.error, .inValidPSW)
    }else{
        return nil
        }
    }
    
    
    func isValidRegEx(_ testStr: String, _ regex: RegEx) -> Bool {
        if regex.rawValue == "" {return true}
        let stringTest = NSPredicate(format:"SELF MATCHES %@", regex.rawValue)
        let result = stringTest.evaluate(with: testStr)
        return result
    }
}


//for failure and success results
enum Alert {
    case success
    case failure
    case error
}
//for success or failure of validation with alert message
enum Valid {
    case success
    case failure(Alert, AlertMessages)
}
enum ValidationType {
    
    case email
    case stringWithFirstLetterCaps
    case phoneNo
    case alphabeticString
    case password
    case countryCode
    case empty
    case firstName
    case lastName
    case confirmPassword
    case age
    case passwordUnap
    
    
}
enum RegEx: String {
    case email = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}" // Email
    case password = "^(?=.*\\d)[0-9a-zA-Z!@#$%^&*()-_=+{}|?>.<,:;~`’]{8,}$" // Password should be 6-15 chars, one number at least.
    case alphabeticStringWithSpace = "^[a-zA-Z ]*$" // e.g. hello sandeep
    case alphabeticStringFirstLetterCaps = "^[A-Z]+[a-zA-Z]*$" // SandsHell
    case phoneNo = "^((\\+)|(00))[0-9]{11,11}$"
    case countryCode = ""
    //Change RegEx according to your Requirement
}

enum AlertMessages: String {
    
    case inValidEmail = "Please check your email address and try again."
    case invalidFirstLetterCaps = "First Letter should be capital"
    case invalidAlphabeticString = "Invalid String"
    case inValidPSW = "Password should contain 8 characters."
    case invalideCode = "Enter country code"
    case emptyEmail = "Please enter your email address."
    case emptyFirstLetterCaps = "Empty Name"
    case emptyAlphabeticString = "Empty String"
    case emptyPSW = "Please enter your password."
    case emptyString = "Empty string"
    case inValidPhone = "Please enter a valid phone number."
    case emptyPhone = "Please enter your phone number."
    case emptyFirstName = "Please enter your name."
    case emptyLastName = "Please enter your last name."
    case emptyConfirmPassword = "Please confirm your password."
    case invalidConfirmPassword = "Passwords doesn't match."
    case emptyAge = "Please specify your Age"
    case invalidAge = "Age must be between 18 and 100 years old"
    
}


