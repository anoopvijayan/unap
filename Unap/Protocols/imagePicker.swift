//
//  imagePicker.swift
//  Unap
//
//  Created by Levin  on 12/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation
import UIKit

typealias compeletion = (_ image:UIImage) -> Void

protocol ImagePickerPresentable: class {
    
   //func showImagePicker()
    func showImagePicker(allowEditing:Bool,result:@escaping compeletion)
}

extension ImagePickerPresentable where Self: UIViewController {
    
    fileprivate func pickerControllerActionFor(for type: UIImagePickerController.SourceType, title: String,allowEditing:Bool = true) -> UIAlertAction? {
        guard UIImagePickerController.isSourceTypeAvailable(type) else {
            return nil
        }
        let permission:PermissionType = PermissionType.camera
        
        switch PermissionManager.shared.permissionStatus(for: permission) {
        case .authorized: print("Autharized")
        case .denied: showAlertSettings(type: permission)
        case .notDetermined: showNotificationController(type: permission, block:  { (state) in
        
            })
        case .restricted:  showAlertSettings(type: permission)
        }
        return UIAlertAction(title: title, style: .default) { [unowned self] _ in
            let pickerController           = UIImagePickerController()
            pickerController.delegate      = ImagePickerHelper.shared
            pickerController.sourceType    = type
            pickerController.allowsEditing = allowEditing
            self.present(pickerController, animated: true)
        }
    }
    
    func showImagePicker(allowEditing:Bool = true,result:@escaping compeletion) {
       // ImagePickerHelper.shared.delegate = self
         ImagePickerHelper.shared.callback = result
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        if let action = self.pickerControllerActionFor(for: .camera, title: "Take photo", allowEditing:allowEditing) {
            optionMenu.addAction(action)
        }
        if let action = self.pickerControllerActionFor(for: .photoLibrary, title: "Select from library", allowEditing: allowEditing) {
            optionMenu.addAction(action)
        }
        
        optionMenu.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(optionMenu, animated: true)
    }
}

fileprivate class ImagePickerHelper: NSObject {
    
    weak var delegate: ImagePickerPresentable?
     var callback:compeletion?
    
    fileprivate struct `Static` {
        fileprivate static var instance: ImagePickerHelper?
    }
    
    fileprivate class var shared: ImagePickerHelper {
        if ImagePickerHelper.Static.instance == nil {
            ImagePickerHelper.Static.instance = ImagePickerHelper()
        }
        return ImagePickerHelper.Static.instance!
    }
    
    fileprivate func dispose() {
        ImagePickerHelper.Static.instance = nil
    }
    
    func picker(picker: UIImagePickerController, selectedImage data: Data?) {
        picker.dismiss(animated: true, completion: nil)
        
       // self.delegate?.selectedImage(data: data)
        guard let imageData = data, let image = UIImage(data: imageData) else{
            return
        }
        let downSampledImg = image.resizedImageWithinRect(rectSize: CGSize(width: 400.0, height: 400.0))
        self.callback!(downSampledImg)
        self.delegate = nil
        self.dispose()
    }
}

extension ImagePickerHelper: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        self.picker(picker: picker, selectedImage: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let data = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            return self.picker(picker: picker, selectedImage: nil)
        }
        let downsizedImage = data.resizedImageWithinRect(rectSize: CGSize(width: 400, height: 400))

        self.picker(picker: picker, selectedImage: downsizedImage.pngData())
    }
    
    
    
   // func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
  //      self.picker(picker: picker, selectedImage: image.pngData())
   // }
}

import Kingfisher

protocol ProfileImageDownloadble {
    func prefetchPorfilePic(url:URL?)
}

extension ProfileImageDownloadble {
    
    func prefetchPorfilePic(url:URL?){
        guard let imagerURL = url else{return}
        let prefetcher = ImagePrefetcher(resources: [imagerURL], options: nil, progressBlock: nil) { (skipped, failed, completed) in
            print(completed.count)
        }
        
        prefetcher.start()
    }
}
