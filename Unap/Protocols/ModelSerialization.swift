//
//  ModelSerialization.swift
//  Unapp
//
//  Created by Levin  on 11/09/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation

/// This protocol will convert any codable conforming item to dictionary
protocol dictify {
    
    func todict()->[String: Any]?
}

extension dictify where Self: Codable {
    
    func todict()->[String: Any]?{
        
        do {
            return try JSONSerialization.jsonObject(with: try JSONEncoder().encode(self), options: []) as? [String: Any]
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
}
