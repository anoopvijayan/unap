//
//  ViewModelProtocol.swift
//  BBCream
//
//  Created by Levin  on 28/03/18.
//  Copyright © 2018 Ileaf Solutions. All rights reserved.
//

import Foundation
import UIKit

protocol ViewModelDelegate: class {
    
    func showLoading(message:String)
    
    func hideLoading()
    
    func showError(message:String)
}

/// Extending viewModelDelegate Protocol to have a default behaviour.

extension ViewModelDelegate where Self: UIViewController {
    
    func showLoading(message:String){
        self.view.showHud(message: message)
    }
    
    func hideLoading() {
        self.view.hideHud()
    }
    
    func showError(message: String) {
        self.showAlertMessage(message: message)
    }
    
}


