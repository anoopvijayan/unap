//
//  Endpoint.swift
//  BBCream
//
//  Created by Levin  on 20/06/17.
//  Copyright © 2017 Levin . All rights reserved.
//

import Foundation
import Alamofire

public protocol Endpoint {
    var baseURL: String { get } // https://example.com
    var path: String { get } // /users/
    var fullURL: String { get } // This will automatically be set. https://example.com/users/
    var method: HTTPMethod { get } // .get
    var encoding: ParameterEncoding { get } // URLEncoding.default
    var body: Parameters { get } // ["foo" : "bar"]
    var headers: HTTPHeaders { get } // ["Authorization" : "Bearer SOME_TOKEN"]
    var name: String { get } // Name of the API for debbugging and logging
    var image:[String:Data] { get }
}


public extension Endpoint {
    var encoding: ParameterEncoding {
        return method == .get ? URLEncoding.default : URLEncoding.httpBody
    }
    
    var fullURL: String {
        return baseURL + path
    }
    
    var body: Parameters {
        return Parameters()
    }
    
    var headers : HTTPHeaders{
        return [:]
    }
    
    var baseURL : String{
      
        return Config.developmentEndPoint
    }
    
    var name: String{
        return "API-Test"
    }
    
    var image:[String:Data] {
        return [:]
    }
}





