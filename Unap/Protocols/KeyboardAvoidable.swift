//
//  KeyboardAvoidable.swift
//  BBCream
//
//  Created by Levin  on 22/03/18.
//  Copyright © 2018 Ileaf Solutions. All rights reserved.
//

import Foundation
import UIKit

protocol KeyboardAvoidable: class {
    typealias KeyboardHeightDuration = (height: CGFloat, duration: Double)
    typealias KeyboardHeightDurationBlock = (KeyboardHeightDuration) -> Void
    
    
    func addKeyboardObservers(_ block: KeyboardHeightDurationBlock?)
    
    func removeKeyboardObservers()
    
    var constraintsToAdjust: [NSLayoutConstraint]? { get }
}

var KeyboardShowObserverKey: UInt8 = 0
var KeyboardHideObserverKey: UInt8 = 1

extension KeyboardAvoidable where Self: UIViewController {
    
    private var keyboardShowObserver: Any? {
        get {
            return objc_getAssociatedObject(self, &KeyboardShowObserverKey)
        }
        set {
            objc_setAssociatedObject(self, &KeyboardShowObserverKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    private var keyboardHideObserver: Any? {
        get {
            return objc_getAssociatedObject(self, &KeyboardHideObserverKey)
        }
        set {
            objc_setAssociatedObject(self, &KeyboardHideObserverKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    var constraintsToAdjust: [NSLayoutConstraint]? {
        return nil
    }
    
    func addKeyboardObservers(_ block: KeyboardHeightDurationBlock? = nil) {
        let center = NotificationCenter.default
        
        //keyboard show observer
        keyboardShowObserver = center.addObserver(forName: UIResponder.keyboardWillShowNotification,
                                                  object: nil,
                                                  queue: nil)
        { [weak self] notification in
            
            guard let strongSelf = self,
                let heightDuration = strongSelf.getKeyboardHeightDurationFrom(notification: notification) else { return }
            
            if let block = block {
                
                block(heightDuration)
                
            }else if let constraints = strongSelf.constraintsToAdjust {
                
                constraints.forEach { $0.constant = -heightDuration.height }
                
                UIView.animate(withDuration: heightDuration.duration){
                    strongSelf.view.layoutIfNeeded()
                }
            }
        }
        
        //keyboard hide observer
        keyboardHideObserver = center.addObserver(forName: UIResponder.keyboardWillHideNotification,
                                                  object: nil,
                                                  queue: nil)
        { [weak self] notification in
            
            guard let strongSelf = self else { return }
            
            if let block = block {
                
                block((0, 0.25))
                
            }else if let constraints = strongSelf.constraintsToAdjust {
                
                constraints.forEach { $0.constant = 0 }
                
                UIView.animate(withDuration: 0.25) {
                    strongSelf.view.layoutIfNeeded()
                }
            }
        }
    }
    
    private func getKeyboardHeightDurationFrom(notification: Notification) -> KeyboardHeightDuration? {
        guard let userInfo = notification.userInfo,
            let rect = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect,
            let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double else { return nil }
        
        return (rect.height, duration)
    }
    
    func removeKeyboardObservers() {
        if let observer = keyboardShowObserver {
            NotificationCenter.default.removeObserver(observer)
        }
        if let observer = keyboardHideObserver {
            NotificationCenter.default.removeObserver(observer)
        }
        keyboardShowObserver = nil
        keyboardHideObserver = nil
    }
    
   
}
