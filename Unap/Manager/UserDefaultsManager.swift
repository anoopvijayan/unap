//
//  UserDefaultsManager.swift
//  Unapp
//
//  Created by Levin  on 04/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation

enum userDefaultsKey:String {
    case deviceToken
    case authToken
    case user
    case landLoardBankDetails
    case studentUniversityDetails
    case deviceTokenData
}

/// UserDefaultsManager will be resposible for saving and fetching data from userDefaults.

class UserDefaultsManager {
    
    static var deviceToken:String?{
        get{
            return UserDefaults.standard.string(forKey: userDefaultsKey.deviceToken.rawValue)
        }
        set{
            UserDefaults.standard.set(newValue, forKey: userDefaultsKey.deviceToken.rawValue)
        }
        
    }
    
    static var deviceTokenData:Data?{
        get{
            return UserDefaults.standard.data(forKey: userDefaultsKey.deviceTokenData.rawValue)
        }
        set{
            UserDefaults.standard.set(newValue, forKey: userDefaultsKey.deviceTokenData.rawValue)
        }
        
    }
    
    static var authToken:String?{
        get{
            return UserDefaults.standard.string(forKey: userDefaultsKey.authToken.rawValue)
        }
        set{
            UserDefaults.standard.set(newValue, forKey: userDefaultsKey.authToken.rawValue)
        }
        
    }
    
    static var user:User?{
        get{
            return UserDefaults.standard.df.fetch(forKey: userDefaultsKey.user.rawValue, type: User.self)
        }
        set{
            UserDefaults.standard.df.store(newValue, forKey: userDefaultsKey.user.rawValue)
        }
        
    }
    
    static var studentUniversity:Universities?{
        get{
            return UserDefaults.standard.df.fetch(forKey: userDefaultsKey.studentUniversityDetails.rawValue, type: Universities.self)
        }
        set{
            UserDefaults.standard.df.store(newValue, forKey: userDefaultsKey.studentUniversityDetails.rawValue)
        }
        
    }
    
    static var bankDetails:BankDetail?{
        get{
            return UserDefaults.standard.df.fetch(forKey: userDefaultsKey.landLoardBankDetails.rawValue, type: BankDetail.self)
        }
        set{
            UserDefaults.standard.df.store(newValue, forKey: userDefaultsKey.landLoardBankDetails.rawValue)
        }
        
    }
    
    
    
    static func clearData(){
        UserDefaults.standard.removeObject(forKey: userDefaultsKey.deviceToken.rawValue)
        UserDefaults.standard.removeObject(forKey: userDefaultsKey.landLoardBankDetails.rawValue)
         UserDefaults.standard.removeObject(forKey: userDefaultsKey.authToken.rawValue)
         UserDefaults.standard.removeObject(forKey: userDefaultsKey.user.rawValue)
        
    }
    
}

extension UserDefaults {
    
    /// EZSE: Generic getter and setter for UserDefaults.
     subscript(key: userDefaultsKey) -> AnyObject? {
        get {
            return object(forKey: key.rawValue) as AnyObject?
        }
        set {
            set(newValue, forKey: key.rawValue)
        }
    }
    
    /// EZSE: Date from UserDefaults.
     func date(forKey key: String) -> Date? {
        return object(forKey: key) as? Date
    }
}

import SwiftyJSON

extension Default where Base: UserDefaults {
    public func store<T: Codable>(_ value: T,
                                  forKey key: String,
                                  encoder: JSONEncoder = JSONEncoder()) {
        if let data: Data = try? encoder.encode(value) {
            (base as UserDefaults).set(data, forKey: key)
        }
    }
    
    public func fetch<T>(forKey key: String,
                         type: T.Type,
                         decoder: JSONDecoder = JSONDecoder()) -> T? where T: Decodable {
        
        if let data = (base as UserDefaults).data(forKey: key) {
            return try? decoder.decode(type, from: data) as T
        }
        return nil
    }
    
}

public final class Default<Base> {
    let base: Base
    public init(_ base: Base) {
        self.base = base
    }
}

public protocol DefaultCompatible {
    associatedtype CompatibleType
    var df: CompatibleType { get }
}

public extension DefaultCompatible {
    public var df: Default<Self> {
        return Default(self)
    }
}

extension UserDefaults: DefaultCompatible {}
