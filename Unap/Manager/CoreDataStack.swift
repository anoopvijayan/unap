//
//  CoreDataStack.swift
//  Unapp
//
//  Created by Levin  on 21/09/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation
import CoreData



class DataManager {
    
    lazy var stack:CoreDataStack = { return CoreDataStack.shared }()
    
    // MARK: Creating Entities
    
    func createEntity<entity:NSManagedObject>(entityName:String) throws -> entity{
        guard let description =  NSEntityDescription.entity(forEntityName: entityName, in: stack.mainContext) else {
            throw errorWithMessage("No entity found:\(entityName)")
        }
       return entity(entity: description, insertInto: stack.mainContext)
        
    }
    
    // MARK: Fetching Entities
    
    func fetchObjects <T: NSManagedObject>(_ entityClass:T.Type,
                                           sortBy: [NSSortDescriptor]? = nil,
                                           predicate: NSPredicate? = nil) throws -> [T] {
       
        let request: NSFetchRequest<T>
        if #available(iOS 10.0, *) {
            request = entityClass.fetchRequest() as! NSFetchRequest<T>
        } else {
            let entityName = String(describing: entityClass)
            request = NSFetchRequest(entityName: entityName)
        }
        
        request.returnsObjectsAsFaults = false
        request.predicate = predicate
        request.sortDescriptors = sortBy
        
        let fetchedResult = try stack.mainContext.fetch(request)
        return fetchedResult
    }
    
    func deleteEntity<T: NSManagedObject>(_ entityClass:T.Type) throws {
        let entityName = String(describing: T.self)
        let request = NSFetchRequest<T>(entityName: entityName)
        request.returnsObjectsAsFaults = false
        do{
           let fetchedResult = try stack.mainContext.fetch(request)
            for object in fetchedResult {
                
                stack.mainContext.delete(object)
            }
        }catch(let error){
            throw error
        }
    
    }
    
    
    // MARK: Error handling
    
    private func errorWithMessage(_ message: String) -> NSError {
        let userInfo = [NSLocalizedDescriptionKey: message]
        let error = NSError(domain: "com.levin.CoreDataStack", code: 0, userInfo: userInfo)
        return error
    }

    
}


class CoreDataStack{
    // MARK: - Core Data stack
    
    static let shared = CoreDataStack()
    
    
   lazy var mainContext: NSManagedObjectContext = {
        return persistentContainer.viewContext
    }()
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Unapp")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
           
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            } else{
                
                self.logInfo("DBPATH:\( storeDescription.url!.absoluteString)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
    
    
}



protocol HasNameProtocol {
    associatedtype FetchableType: NSManagedObject = Self
    static var entityName : String { get }
    var name: String {get}
    
    static func predicate(name: String) -> NSPredicate
    static func find(name: String, context: NSManagedObjectContext) throws -> [FetchableType]
    
}

extension HasNameProtocol where FetchableType == Self {
    
    static var entityName : String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
    
    static func predicate(name: String) -> NSPredicate {
        return NSPredicate(format: "name == %@", name)
    }
    
    static func find(name: String, context: NSManagedObjectContext) throws -> [FetchableType] {
        let request = NSFetchRequest<FetchableType>(entityName: entityName)
        let predicate = self.predicate(name: name)
        request.predicate = predicate
        return try context.fetch(request)
    }
}
