//
//  DataManager.swift
//  Unap
//
//  Created by Levin  on 05/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation
import CoreData


extension DataManager{
    
    func saveUser(userData:User?,token:String?,overwrite:Bool = false)throws{
        
        guard let user = userData,let tokenStr = token else {
            throw(NetworkError.init(mappingErrorKey: "User object can't be nil"))
        }
        
        do{
            let jsonData = try JSONEncoder().encode(userData)
            if !overwrite{
            guard let userUN = try self.createEntity(entityName: "UNUser") as? UNUser  else{
             throw NetworkError.init(mappingErrorKey: "Unable to create entity")
            }
            
            userUN.userId = "\(user.id ?? 0000)"
            userUN.userData = jsonData as NSObject
            userUN.userToken = tokenStr
            }
            
        }catch (let error){
            throw error
        }
        
    }
    
    func saveConversation(request:AcceptedList)throws{
        do{
            guard let id = request.home_request_id   else {throw(NetworkError.init(mappingErrorKey: "User ID can't be nil")) }
            let predicate = NSPredicate(format: "requestID contains[c] %@", "\(id)")
         let fetchConverstion = try self.fetchObjects(UNConversation.self, sortBy: nil, predicate: predicate)
             let jsonData = try JSONEncoder().encode(request)
            if fetchConverstion.count > 0{
                let conversation = fetchConverstion.first!
                conversation.acceptedList = jsonData as NSObject
                
            }else{
                guard let conversation = try self.createEntity(entityName: "UNConversation") as? UNConversation  else{
                    throw NetworkError.init(mappingErrorKey: "Unable to create entity")
                }
                conversation.requestID = "\(id)"
               
                conversation.acceptedList = jsonData as NSObject
            }
            
        }catch (let error){
            throw error
        }
    }
    
    static func fetchCountFor(entityName: String, predicate: NSPredicate, onMoc moc: NSManagedObjectContext) -> Int {
        
        var count: Int = 0
        
        moc.performAndWait {
            
            let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entityName)
            fetchRequest.predicate = predicate
            fetchRequest.resultType = NSFetchRequestResultType.countResultType
            
            do {
                count = try moc.count(for: fetchRequest)
            } catch {
                //Assert or handle exception gracefully
            }
            
        }
        
        return count
    }
    
    
    func saveMessage(chatMessage:QBChatMessage){
        
        let newMessage = UNMessage(entity: NSEntityDescription.entity(forEntityName: "UNMessage", in: CoreDataStack.shared.mainContext)!, insertInto: CoreDataStack.shared.mainContext)
        newMessage.dateSent = chatMessage.dateSent as NSDate?
        newMessage.message = chatMessage.text
        newMessage.dialogID = chatMessage.dialogID
        
        newMessage.id = chatMessage.id
//        if chatMessage.id == nil{
//            print("no ID")
//        }
        
        newMessage.recipientID = Int64(chatMessage.recipientID)
        newMessage.senderID = Int64(chatMessage.senderID)
        newMessage.qbMessage = chatMessage
        newMessage.isDelivered = true
        newMessage.isSeen = false
        
        if let matchId:String = chatMessage.customParameters["request_ID"] as? String{
            if let matched = self.fetchMatchWith(matchId: matchId){
                matched.lastMessage = chatMessage.text
                matched.lastMessageDate = chatMessage.dateSent as NSDate?
                matched.qblastMessage = chatMessage
            }
        }
        
        
        
        do {
            try CoreDataStack.shared.mainContext.save()
        } catch {
            print ("There was an error")
        }
        
        
    }
    
    
    func fetchMatchWith(matchId:String)->UNConversation?{
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "UNConversation")
        fetchRequest.predicate = NSPredicate(format: "requestID == %@",matchId)
        do{
            let matches:[UNConversation] = try self.fetchObjects(UNConversation.self, sortBy: nil, predicate: nil)
            
            return matches.first
        }
        catch{
            print(error.localizedDescription)
        }
        return nil
    }
    
    func fetchMessage(messageID:String)->UNMessage?{
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "UNMessage")
        fetchRequest.predicate = NSPredicate(format: "id == %@",messageID)
        fetchRequest.includesPendingChanges = true
        do{
            let messages:[UNMessage] = try CoreDataStack.shared.mainContext.fetch(fetchRequest) as! [UNMessage]
            if messages.count>0{
                let message = messages.first
                return message!
                
            }
        }
        catch{
            print(error.localizedDescription)
        }
        return nil
    }
    
   
        
    
    
}
