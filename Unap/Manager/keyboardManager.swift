//
//  keyboardManager.swift
//  Shulph
//
//  Created by Levin  on 15/12/17.
//  Copyright © 2017 Ileaf Solutions. All rights reserved.
//

import Foundation
import UIKit

class UNKeyboardManager{
    let targetView:UIScrollView
    var activeField:UIView?
    
    init(view:UIScrollView) {
        self.targetView = view
        self.registerForKeyboardNotifications()
        let scrollViewTap = UITapGestureRecognizer(target: self, action: #selector(scrollViewTapped))
        scrollViewTap.numberOfTapsRequired = 1
        scrollViewTap.cancelsTouchesInView = false
        
        targetView.addGestureRecognizer(scrollViewTap)
    }
    
    @objc func scrollViewTapped() {
        
        self.targetView.superview?.endEditing(true)
    }
    
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        //self.scrollView.isScrollEnabled = true
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize!.height, right: 0.0)
        
        self.targetView.contentInset = contentInsets
        self.targetView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.targetView.superview!.frame
        aRect.size.height -= keyboardSize!.height
        
        
        
       
        if let activeField = self.activeField {
            let re = self.targetView.convert(activeField.frame, from: self.targetView.superview!)
           
            if (!aRect.contains(re.origin)){
//                let paddedActiveFrame = CGRect(x: re.origin.x, y: re.origin.y + 15, width: re.size.width, height: re.size.height)
                self.targetView.scrollRectToVisible(re, animated: true)
            }
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        //  var info = notification.userInfo!
        // let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        // let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, -keyboardSize!.height, 0.0)
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        self.targetView.contentInset = contentInsets
        self.targetView.scrollIndicatorInsets = contentInsets
        self.targetView.superview!.endEditing(true)
        //self.scrollView.isScrollEnabled = false
    }
    
}
