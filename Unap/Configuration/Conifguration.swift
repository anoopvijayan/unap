//
//  Conifguration.swift
//  GO
//
//  Created by Levin  on 18/10/17.
//  Copyright © 2017 Ileaf Solutions. All rights reserved.
//

import Foundation
import UIKit

struct Config {
    
    static let deviceOS             = "IOS"
    static let appName              = "Unapp"
    static let screenSize           =  UIScreen.main.bounds
    static let screenWidth          =  UIScreen.main.bounds.width
    static let screenHeight         =  UIScreen.main.bounds.height
    
    static let stripePublishableKey = "pk_test_NmC1lXtcw2F9fCYr1SeMGFvI"
    static let developmentEndPoint  = "http://34.254.14.67"
    static let liveEndPoint         = "https://www.shulph.com/api/index.php"
    static let OneSignalKey         = "0744626e-cb1a-4202-9eec-72f878359a33"
    static let S3BucketName         = "shulph-dev-shelf"
    static let QuickBloxUserPassword =  "unap1235"
    
    
   static var isIphone5Family:Bool{
        if UIScreen.width == 320{
            return true
        }
        return false
    }
    
    static var isIphoneX: Bool {
        return UIScreen.main.nativeBounds.height == 2436
    }
    
    static var device:UIUserInterfaceIdiom{
        return UIScreen.main.traitCollection.userInterfaceIdiom
    }
    
}

extension UIScreen{
   static var width:CGFloat{
        return self.main.bounds.width
    }
    
   static var height:CGFloat{
       return UIScreen.main.bounds.height
    }
    
}
