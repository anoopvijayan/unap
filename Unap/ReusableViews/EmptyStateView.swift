//
//  EmptyStateView.swift
//  Unap
//
//  Created by Aneesh on 11/19/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation
import UIKit

class TableViewHelper {
    
    class func EmptyMessage(message:String, tableView:UITableView) {
        let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width:200, height: 200))
      
        let newView = UIView(frame: tableView.bounds)
        let imageView = UIImageView(frame: rect)
        imageView.image = UIImage(named: "searchLogo")
        imageView.center = newView.center
        newView.addSubview(imageView)
        
        let messageRect = CGRect(origin: CGPoint(x: 0,y :imageView.frame.origin.y + imageView.frame.size.height), size: CGSize(width:tableView.frame.size.width, height: 40))
        let messageLabel = UILabel(frame: messageRect)
        messageLabel.text = message
        messageLabel.textColor = UIColor.lightGray
//        messageLabel.backgroundColor = UIColor.red
        messageLabel.textAlignment = .center
        messageLabel.numberOfLines = 0
        messageLabel.lineBreakMode = .byWordWrapping
        messageLabel.font = UIFont(name: "Avenir", size: 15)
//        messageLabel.sizeToFit()
        newView.addSubview(messageLabel)
        tableView.backgroundView = newView
        tableView.separatorStyle = .none
    }
}
