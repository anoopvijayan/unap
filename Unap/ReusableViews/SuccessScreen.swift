//
//  SuccessScreen.swift
//  Unap Student
//
//  Created by Aneesh on 11/13/18.
//  Copyright © 2018 Aneesh. All rights reserved.
//

import Foundation
import UIKit
import Lottie
import AudioToolbox

enum NotifyType {
    case indicator
    case success
    case error
    case search
}

class  SuccessScreen: UIView {
    
     @IBOutlet weak var notificationMessage: UILabel!
     @IBOutlet weak var holderView: UIView!
    
     var type:NotifyType!
    
      var loopEnable:Bool = false
      var holderViewBgColor:UIColor = .white
      var titleHidden:Bool = false
      var notificationText:String = ""
      var transparentBg:Bool = true
      var dismissAnimation:Bool = false
      internal var animationView = LOTAnimationView()
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "SuccessScreen", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    override func awakeFromNib() {
        if transparentBg{
            self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
            self.isOpaque = false
        }
       
    }
    
    override func layoutSubviews() {
         layoutIfNeeded()
             animationView.center = self.center
        }

    func animatePin(loopEnable:Bool,holderViewBgColor:UIColor,titleHidden:Bool,notificationText:String,type:NotifyType ,size:CGSize) {
        holderView.backgroundColor = holderViewBgColor
        notificationMessage.isHidden = titleHidden
        notificationMessage.text = notificationText
        var lottie:String = ""
        switch type {
        case .indicator:lottie = "glow_loading"
        case .success:lottie = "checked_done_"
        case .error:lottie = "file_error"
        case .search:lottie = "search"
       }
        
        animationView = LOTAnimationView(name: lottie)
        animationView.frame.size = size
        animationView.center = self.center
        animationView.loopAnimation = loopEnable
        if !loopEnable{
             AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        }
        self.addSubview(animationView)
       
        animationView.play { (sucess:Bool) in
           
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.25, execute: {
                self.removeFromSuperview()
            })
            
        }
        
        layoutIfNeeded()
        }
    
    }




