//
//  SHContraint.swift
//  Shulph
//
//  Created by Levin  on 27/11/17.
//  Copyright © 2017 Ileaf Solutions. All rights reserved.
//

import Foundation
import UIKit

extension NSLayoutConstraint
{
    //We use a simple inspectable to allow us to set a value for iphone 5.
    @IBInspectable var iPhone5_Constant: CGFloat
        {
        set{
            //Only apply value to iphone 5 devices.
            if Config.screenWidth == 320
            {
                self.constant = newValue;
            }
        }
        get
        {
            return self.constant;
        }
    }
}

extension UIStackView
{
    //We use a simple inspectable to allow us to set a value for iphone 5.
    
    @IBInspectable var iPhone5_Spacing: CGFloat
        {
        set{
            //Only apply value to iphone 5 devices.
            if UIScreen.width == 320
            {
                self.spacing = newValue
            }
        }
        get
        {
            return self.spacing;
        }
    }
}

