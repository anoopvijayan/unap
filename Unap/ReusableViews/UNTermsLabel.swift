//
//  UNTermsLabel.swift
//  Unap
//
//  Created by Levin  on 08/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation
import UIKit


 protocol termsLabelDelegate: class {
    func showTermsAndCondition()
}

class UNTermsLabel: UILabel {
    
   weak var delegate:termsLabelDelegate?
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    func commonInit(){
        let termsTap = UITapGestureRecognizer(target: self, action: #selector(taplabel(sender:)))
        termsTap.numberOfTapsRequired = 1
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(termsTap)
       
    }
    @objc func taplabel(sender:UITapGestureRecognizer){
        self.delegate?.showTermsAndCondition()
        
    }
   
}

extension termsLabelDelegate where Self:UIViewController{
    
     func showTermsAndCondition(){
        
        if self.presentedViewController == nil {
            let vc = TermsVC.makeFromStoryboard()
           
           // self.hero.modalAnimationType = .cover(direction: .down)
            self.present(vc, animated: true, completion: nil)
            
        }else{
            self.logInfo("Terms and codition could not be loaded")
        }
        
    }
}

extension WelcomeVC:termsLabelDelegate{}
extension SignInVC:termsLabelDelegate{}
extension EmailVerificationVC:termsLabelDelegate{}
extension CreateAccountVC:termsLabelDelegate{}
extension GetStartedVC:termsLabelDelegate{}


