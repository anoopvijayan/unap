//
//  CustomMarker.swift
//  Unap Student
//
//  Created by Aneesh on 11/13/18.
//  Copyright © 2018 Aneesh. All rights reserved.
//

import Foundation
import UIKit

protocol UIViewLoading {
    func didTapInfoButton()
}
class CustomMarker: UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
   
    @IBOutlet weak var icon: UIImageView!
     var delegate:UIViewLoading!
    @IBAction func didTapInfoButton(_ sender: UIButton) {
      delegate.didTapInfoButton()
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "CustomMarker", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }}

extension UIView : UIViewLoading {func didTapInfoButton() {}}

extension UIViewLoading where Self : UIView {    
    // note that this method returns an instance of type `Self`, rather than UIView
    static func loadFromNib() -> Self {
        let nibName = "\(self)".split{$0 == "."}.map(String.init).last!
        let nib = UINib(nibName: nibName, bundle: nil)
        return nib.instantiate(withOwner: self, options: nil).first as! Self
    }
}
