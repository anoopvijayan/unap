//
//  SHLabel.swift
//  Shulph
//
//  Created by Levin  on 27/11/17.
//  Copyright © 2017 Ileaf Solutions. All rights reserved.
//

import UIKit

class UNLabel: UILabel {
    
    @IBInspectable var topInset: CGFloat = 7.0
    @IBInspectable var bottomInset: CGFloat = 7.0
    @IBInspectable var leftInset: CGFloat = 12.0
    @IBInspectable var rightInset: CGFloat = 12.0
    
    
    
    override func draw(_ rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        
        super.drawText(in:rect.inset(by: insets))
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            var contentSize = super.intrinsicContentSize
            contentSize.height += topInset + bottomInset
            contentSize.width += leftInset + rightInset
            return contentSize
        }
    }

    var _Iphone5fontSize: CGFloat = 0
    @IBInspectable var Iphone5fontSize: CGFloat {
        get {
            return _Iphone5fontSize
        }
        set {
            if UIScreen.width == 320 {
                _Iphone5fontSize = newValue
                    self.font = self.font.withSize(newValue)
            }
        }
    }

}
