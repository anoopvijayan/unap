//
//  CircularImageView.swift
//  Unap
//
//  Created by Levin  on 10/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation
import UIKit

class CircularImageView: UIImageView {
    override init(image: UIImage?) {
        super.init(image: image)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = self.frame.size.height / 2
        //self.clipsToBounds = true
    }
}
