//
//  UNMapController.swift
//  Unap Student
//
//  Created by Aneesh on 11/13/18.
//  Copyright © 2018 Aneesh. All rights reserved.
//

import Foundation
import UIKit
import GooglePlaces
import GoogleMaps


class UNMapController: UIViewController, GMSMapViewDelegate,UIViewLoading {
    func didTapInfoButton() {
        let controller:HouseDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "HouseDetailVC") as! HouseDetailVC
        controller.home = home
        present(controller, animated: true, completion: nil)
    }
    
    
    var mapView: GMSMapView!
    var placeMarker: GMSMarker?
    var placeView: UIImageView?
    var latitude = String()
    var longitude = String()
    var home:Home?
    
    override func loadView() {
        latitude = home?.lattitude ?? "9.45"
        longitude = home?.longitude ?? "9.45"
        let lat = Double(latitude);let lon = Double(longitude)
        
        let camera = GMSCameraPosition.camera(withLatitude: lat!,
                                              longitude: lon!,
                                              zoom: 14)
        
        mapView = GMSMapView.map(withFrame: .zero, camera: camera)
        view = mapView
        mapView.delegate = self
        
        let houseMarker = UIImage(named: "marker")!.withRenderingMode(.alwaysTemplate)
        let markerView = UIImageView(image: houseMarker)
        placeView = markerView
        placeView?.alpha = 0
        placeView?.transform = CGAffineTransform(scaleX: 0.5,y: 0.5)
        
        let position = CLLocationCoordinate2D(latitude: lat!, longitude: lon!)
        let marker = GMSMarker(position: position)
        marker.iconView = markerView
        marker.tracksViewChanges = true
        marker.map = mapView
        marker.infoWindowAnchor = CGPoint(x: 0.6, y: 0.4)
        placeMarker = marker
        
        loadUIBack()
        
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        UIView.animate(withDuration: 1.0, animations: { () -> Void in
            self.placeView?.alpha = 1
            self.placeView?.transform = CGAffineTransform(scaleX: 1.0,y: 1.0)
        }, completion: {(finished) in
            self.placeMarker?.tracksViewChanges = false
            self.placeMarker?.appearAnimation = .pop
            self.mapView.selectedMarker = self.placeMarker
        })
}
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        let customMarker:CustomMarker = CustomMarker.loadFromNib()
        customMarker.titleLabel.text = home?.home_title
        if let url = home?.home_img?.first?.home_image{
           customMarker.icon.kf.indicatorType = .activity
            customMarker.icon.kf.setImage(with: URL(string: url))
        }else{
           customMarker.icon.image = UIImage(named: "homePlaceHolder")
        }
       
        return customMarker
    }
}

extension UNMapController{
    func  loadUIBack(){
        
        let buttonPuzzle:UIButton = UIButton(frame: CGRect(x:20,y: 20, width:50, height:58))
//        buttonPuzzle.backgroundColor = UIColor.green
//        buttonPuzzle.setTitle("Back", for: UIControl.State.normal)
        buttonPuzzle.setImage(UIImage(named:"back_arrow"), for: .normal)
        buttonPuzzle.addTarget(self, action:#selector(buttonAction(sender:)), for: UIControl.Event.touchUpInside)
        buttonPuzzle.tag = 22;
        let window = UIApplication.shared.keyWindow!
            window.addSubview(buttonPuzzle)
        }
    
    @objc func buttonAction(sender:UIButton!) {
        let btnsendtag:UIButton = sender
        if btnsendtag.tag == 22 {
            //println("Button tapped tag 22")
            self.dismiss(animated: true) {
            self.removeBackFromUI()
            }
        }
    }
    
    func removeBackFromUI() {
         UIApplication.shared.keyWindow!.viewWithTag(22)?.removeFromSuperview()
    }
    
}
