//
//  SHButton.swift
//  Shulph
//
//  Created by Levin  on 27/11/17.
//  Copyright © 2017 Ileaf Solutions. All rights reserved.
//

import UIKit

class UNButton: UIButton {

    var _Iphone5fontSize: CGFloat = 0
    @IBInspectable var Iphone5fontSize: CGFloat {
        get {
            return _Iphone5fontSize
        }
        set {
            if UIScreen.width == 320 {
                _Iphone5fontSize = newValue

                    self.titleLabel?.font =  self.titleLabel?.font.withSize(newValue)
        
            }
        }
    }

}
