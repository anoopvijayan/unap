//
//  ForgotPasswordVC.swift
//  Unap
//
//  Created by Levin  on 09/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import UIKit
import SSSpinnerButton

class ForgotPasswordVC: UIViewController,Validator {
    
    //MARK:- IBOutlets
    
    @IBOutlet weak var TFEmail: UITextField!
    @IBOutlet weak var btnReset: SSSpinnerButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func resetPressed(_ sender: UIButton) {
        
         let result = self.validate(values: (ValidationType.email,TFEmail.text ?? ""))
        
        switch result{
            
        case .success: performResetAPI()
            
        case .failure(_, let message):
            self.showAlertMessage(message: message.rawValue)
        }
        
        
    }
    
    func performResetAPI(){
         btnReset.startAnimate(spinnerType: .circleStrokeSpin, spinnercolor: .greenUN, complete: nil)
        NetworkManager.shared.requestForm(endpoint: OnboardingEndpoints.reset(email: self.TFEmail.text!)) { (result:NetworkResult<APICommonResponse>) in
            self.btnReset.stopAnimate(complete: nil)
            
            switch result{
                
            case .success(let data):
                self.showAlertMessage(title: Config.appName, message: data.message, btnName: "Ok", btnCallback: { (action) in
                    self.hero.dismissViewController()
                })
            case .error(let error):
                self.showAlertMessage(message: error.localizedDescription)
            }
            
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ForgotPasswordVC:ViewModelDelegate{
    
    
    
    func showLoading(message: String) {
        btnReset.startAnimate(spinnerType: .circleStrokeSpin, spinnercolor: .greenUN, complete: nil)
    }
    
    func hideLoading() {
        btnReset.stopAnimate(complete: nil)
        
    }
    
    
    
    
}

