//
//  EmailVerificationVC.swift
//  Unapp
//
//  Created by Levin  on 01/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import UIKit

class EmailVerificationVC: UIViewController,StoryboardInitializable {
    
    
    //MARK: IBOutlets
     @IBOutlet weak var lblTerms: UNTermsLabel!
    //MARK :- Properties
    var user:User!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK : - UI Setups
    func prepareUI(){
        self.lblTerms.delegate = self
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let vc:GetStartedVC = segue.destination as? GetStartedVC{
            vc.user = self.user
        }
        
    }
   

}
