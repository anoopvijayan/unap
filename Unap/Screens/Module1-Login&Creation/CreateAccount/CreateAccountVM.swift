//
//  CreateAccountVM.swift
//  Unapp
//
//  Created by Levin  on 04/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation

struct CreateAccountVM:Validator {
    
    weak var delegate:ViewModelDelegate?
    var userType:userType?
    var name:String = ""
    var email:String = ""
    var age:String   = ""
    var password:String = ""
    var universityId:String = ""
    
    func Validate()->APIRequestRegister?{
        
        let result = self.validate(values: (ValidationType.firstName,name),(ValidationType.email,email),(ValidationType.age,age),(ValidationType.passwordUnap,password))
        switch result{
            
        case .success:
            
            guard let user = self.userType?.value else{fatalError("UserType can't be empty.") }
            
            
            let request = APIRequestRegister(name: self.name, age: self.age , password: self.password , user_type: user, device_token: UserDefaultsManager.deviceToken ?? "simulator", device_type: "IOS", email: self.email,university_id:nil)
                return request
        case .failure(_, let message):
            self.delegate?.showError(message: message.rawValue)
            return nil
        }

    }
    
    func ValidateStudent()->APIRequestRegister?{
        
        let result = self.validate(values: (ValidationType.firstName,name),(ValidationType.email,email),(ValidationType.age,age),(ValidationType.passwordUnap,password))
        switch result{
            
        case .success:
            
            guard let user = self.userType?.value else{fatalError("UserType can't be empty.") }
            
            guard  self.universityId.containsNonWhitespace else {
                self.delegate?.showError(message: "please choose University")
                return nil
            }
            
            let request = APIRequestRegister(name: self.name, age: self.age , password: self.password , user_type: user, device_token: UserDefaultsManager.deviceToken ?? "simulator", device_type: "IOS", email: self.email,university_id:self.universityId)
            return request
        case .failure(_, let message):
            self.delegate?.showError(message: message.rawValue)
            return nil
        }
        
    }
    
}

//MARK: - API Calls
extension CreateAccountVM{
    func createAccount(requestData:APIRequestRegister,completion:@escaping (_ result:User?)->()){
        self.delegate?.showLoading(message: "Loading")
        NetworkManager.shared.requestForm(endpoint: OnboardingEndpoints.createAccount(data: requestData)) { (result:NetworkResult<RegisterResponse>) in
            self.delegate?.hideLoading()
            switch result{
                
            case .success(let data):
                try? self.saveUserToLocal(data: data)
                completion(data.user)
            case .error(let error):
                self.delegate?.showError(message: error.localizedDescription)
            }
        }
    }
    
    func getuniversitylist(compeletion:@escaping (_ result:[Universities]?)->()){
        NetworkManager.shared.requestForm(endpoint: OnboardingEndpoints.getUniversityList()) { (result:NetworkResult<UniversityResponse>) in
            
            switch result {
                
            case .success(let data):compeletion(data.universities)

            case .error(let error):print("ERROR:\(error.localizedDescription)")
                
            }
           }
    }
    
}


//MARK: - Local Storage
extension CreateAccountVM{
    func saveUserToLocal(data:RegisterResponse) throws  {
        UserDefaultsManager.authToken = data.token
        UserDefaultsManager.studentUniversity = data.university
        UserDefaultsManager.bankDetails = data.bank
        UserDefaultsManager.user = data.user
        do{
        try DataManager().saveUser(userData: data.user, token: data.token)
        CoreDataStack.shared.saveContext()
        }catch(let error){
            print("Error:\(error.localizedDescription)")
        }
        
    }
}

