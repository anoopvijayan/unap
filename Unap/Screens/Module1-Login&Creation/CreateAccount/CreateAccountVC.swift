//
//  CreateAccountVC.swift
//  Unapp
//
//  Created by Levin  on 01/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import UIKit
import Hero
import SSSpinnerButton

class CreateAccountVC: UIViewController,StoryboardInitializable {

    
    //MARK: - IBOutlets
    @IBOutlet weak var accountDetails: UIStackView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var agePicker: GradientView!
    @IBOutlet var universityPicker: UIView!
    @IBOutlet var universitytable: UITableView!
    @IBOutlet weak var TFAge: UITextField!
    @IBOutlet weak var TFName: UITextField!
    @IBOutlet weak var TFEmail: UITextField!
    @IBOutlet weak var TFPassword: UITextField!
    @IBOutlet weak var bnSignUp: SSSpinnerButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var lblTerms: UNTermsLabel!
    @IBOutlet weak var stackViewUniversity: UIStackView!
    @IBOutlet weak var TFUniversity: UITextField!
    
    //MARK: - Properties
    var keyboardManager:UNKeyboardManager?
    var userType:userType!
    var viewModel:CreateAccountVM = CreateAccountVM()
    var universityList:[Universities] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       self.prepareUI()
       self.askforPushnotification()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.delegate = self
        if self.userType == .student{
            self.stackViewUniversity.isHidden = false
            self.TFUniversity.inputView = self.universityPicker
            if self.universityList.count == 0{
                self.loadUniversityList()
            }
        }else{
            self.stackViewUniversity.isHidden =  true
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.viewModel.delegate = self
    }
   
    func askforPushnotification(){
        let permission:PermissionType = PermissionType.notifications
        
        switch PermissionManager.shared.permissionStatus(for: permission) {
        case .authorized: print("Autharized")
        case .denied: showAlertSettings(type: permission)
        case .notDetermined: showNotificationController(type: permission, block:  { (state) in
           
        })
        case .restricted:  showAlertSettings(type: permission)
        }
    }
    
    func loadUniversityList(){
        if let universitylocalSaved = Universities.loadFromjsonFile(),
            let result = universitylocalSaved.universities
            {
            self.universityList = result
            self.universitytable.delegate = self
            self.universitytable.dataSource = self
            self.universitytable.reloadData()
        }
        
        
        viewModel.getuniversitylist { [unowned self] (result) in
            if let universities = result{
            self.universityList = universities
            self.universitytable.delegate = self
            self.universitytable.dataSource = self
            self.universitytable.reloadData()
            }
        }
    }
    
    // MARK: - Navigation

  
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
    }
    
    // MARK:- UI Setups
    func prepareUI(){
        self.keyboardManager = UNKeyboardManager(view: scrollView)
        TFAge.inputView = self.agePicker
        datePicker.maximumDate = Date()
        self.lblTerms.delegate = self
    }
    func bindUI(){
        viewModel.name = self.TFName.text?.clearSpaces ?? ""
        viewModel.email = self.TFEmail.text?.clearSpaces ?? ""
        viewModel.age = self.TFAge.text ?? ""
        viewModel.password = self.TFPassword.text?.clearSpaces ?? ""
        viewModel.userType = self.userType
    }
    
    //MARK: - IBActions
    @IBAction func signUpPressed(_ sender: SSSpinnerButton) {
        self.view.endEditing(true)
        self.bindUI()
        switch self.userType!{
            
        case .landlord: self.createAccountForLandoard()
            
        case .student:  self.createAccountForStudent()
            
        }
     
        
    }
    
    func createAccountForLandoard(){
        guard let request = viewModel.Validate() else { return }
        self.resignFirstResponder()
        viewModel.createAccount(requestData: request) { [unowned self] (user) in
            let emailVc = EmailVerificationVC.makeFromStoryboard()
            emailVc.user = user
            self.navigationController?.pushViewController(emailVc, animated: true)
        }
        
    }
    func createAccountForStudent(){
        
        guard let request = viewModel.ValidateStudent() else { return }
        self.resignFirstResponder()
        viewModel.createAccount(requestData: request) { [unowned self] (user) in
            let emailVc = EmailVerificationVC.makeFromStoryboard()
            emailVc.user = user
            self.navigationController?.pushViewController(emailVc, animated: true)
        }
    }
    
    
    @IBAction func datePickerChanged(_ sender: UIDatePicker) {
        let age = self.age(dateOfBirth: sender.date)
        self.TFAge.text = "\(age)"
    }
    
    func age(dateOfBirth: Date) -> Int {
        var ageComponents: DateComponents = Calendar.current.dateComponents([.year], from: dateOfBirth, to: Date())
        return ageComponents.year!
    }
    @IBAction func ageDonePressed(_ sender: UIButton) {
        self.TFAge.resignFirstResponder()
        let age = self.age(dateOfBirth: self.datePicker.date)
        self.TFAge.text = "\(age)"
    }
    
    @IBAction func UniversityDonePressed(_ sender: UIButton) {
        self.TFUniversity.resignFirstResponder()
    }
}



extension CreateAccountVC:ViewModelDelegate{
    
    
    
    func showLoading(message: String) {
        bnSignUp.startAnimate(spinnerType: .circleStrokeSpin, spinnercolor: .greenUN, complete: nil)
    }
    
    func hideLoading() {
        bnSignUp.stopAnimate(complete: nil)
        
    }
    
   
    
    
}


extension CreateAccountVC:HeroViewControllerDelegate{
    
    func heroWillStartAnimatingTo(viewController: UIViewController) {
        if viewController is EmailVerificationVC {
            self.accountDetails.hero.modifiers = [.fade, .scale(0.25)]
        }
        
    }
    
    
    
}

extension CreateAccountVC:UITextFieldDelegate{
    
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        keyboardManager?.activeField = textField
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        keyboardManager?.activeField = textField
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == self.TFName {
            TFEmail.becomeFirstResponder()
        } else if textField == TFEmail {
            TFAge.becomeFirstResponder()
        } else if textField == TFAge {
            TFPassword.becomeFirstResponder()
        } else if textField == TFPassword {
          self.signUpPressed(self.bnSignUp)
        }
        return true
    }

    
}

extension CreateAccountVC:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.universityList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "universityCell") as! universityCell
        cell.lblUniversity.text = self.universityList[indexPath.row].university_name ?? "Empty university"
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel.universityId = "\(self.universityList[indexPath.row].id ?? 0)"
        
        self.TFUniversity.text = self.universityList[indexPath.row].university_name ?? ""
        self.view.endEditing(true)
    }
}
