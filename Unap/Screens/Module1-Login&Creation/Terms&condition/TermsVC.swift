//
//  TermsVC.swift
//  Unap
//
//  Created by Levin  on 08/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import UIKit
import WebKit

class TermsVC: UIViewController,StoryboardInitializable {

    @IBOutlet weak var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let htmlPath = Bundle.main.path(forResource: "termsSample", ofType: "html")
        
        let url = URL(fileURLWithPath: htmlPath!)
        
        let request = URLRequest(url: url)
        webView.load(request)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
