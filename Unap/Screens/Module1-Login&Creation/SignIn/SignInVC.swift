//
//  SignInVC.swift
//  Unap
//
//  Created by Levin  on 08/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import UIKit
import Hero
import SSSpinnerButton

class SignInVC: UIViewController {
    
    //MARK:- IBOutlets
    
    @IBOutlet weak var btnLogin: SSSpinnerButton!
    @IBOutlet weak var lblTerms: UNTermsLabel!
    @IBOutlet weak var TFEmail: UITextField!
    @IBOutlet weak var TFPassword: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
    //MARK: - Properties
    var keyboardManager:UNKeyboardManager?
    var viewModel:SignInVM = SignInVM()
    
    //MARK:- ViewController life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.prepareUI()
        self.askforPushnotification()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.delegate = self
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.viewModel.delegate = self
    }
    
    
    //MARK: - UISetups
    func prepareUI(){
        self.lblTerms.delegate = self
        self.keyboardManager = UNKeyboardManager(view: scrollView)
    }
    
    func askforPushnotification(){
        let permission:PermissionType = PermissionType.notifications
        
        switch PermissionManager.shared.permissionStatus(for: permission) {
        case .authorized: print("Autharized")
        case .denied: showAlertSettings(type: permission)
        case .notDetermined: showNotificationController(type: permission, block:  { (state) in
            
        })
        case .restricted:  showAlertSettings(type: permission)
        }
    }
    
    func bindUI(){
        viewModel.email = self.TFEmail.text?.clearSpaces ?? ""
        viewModel.password = self.TFPassword.text?.clearSpaces ?? ""
    }
    
    //MARK: - IBActions
    @IBAction func loginPressed(_ sender: SSSpinnerButton) {
        self.bindUI()
        guard self.viewModel.validate() else { return }

        self.view.resignFirstResponder()
        self.viewModel.performLogin(email: self.TFEmail.text!, password: self.TFPassword.text!) { [unowned self]  (user) in
            guard let userdata = user?.user_type else { return }
            switch userdata{
                
            case .landlord:self.navigateToHome()
                
            case .student:self.navigateToStudentHome()
                
            }

        }
       // self.navigateToHome()
        
    }
    
    func navigateToHome(){
        self.hero.modalAnimationType = .uncover(direction: .down)
        let vc = UIStoryboard(name: "HomeLandLoard", bundle: nil).instantiateViewController(withIdentifier: "HomeTabbarController")
      self.navigationController?.pushViewController(vc, animated: true)
       
        
    }
    
    func navigateToStudentHome(){
    self.hero.modalAnimationType = .uncover(direction: .down)
    let vc = UIStoryboard(name: "StudentHome", bundle: nil).instantiateViewController(withIdentifier: "studentTabBarController")
    self.navigationController?.pushViewController(vc, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SignInVC:ViewModelDelegate{
    
    
    
    func showLoading(message: String) {
        btnLogin.startAnimate(spinnerType: .circleStrokeSpin, spinnercolor: .greenUN, complete: nil)
    }
    
    func hideLoading() {
        btnLogin.stopAnimate(complete: nil)
        
}

}

extension SignInVC:UITextFieldDelegate{
    
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        keyboardManager?.activeField = textField
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        keyboardManager?.activeField = textField
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == self.TFEmail {
            TFPassword.becomeFirstResponder()
        } else if textField == TFPassword {
           textField.resignFirstResponder()
           self.loginPressed(self.btnLogin)
        } 
        return true
}
}
