//
//  SigInVm.swift
//  Unap
//
//  Created by Levin  on 08/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation

struct SignInVM:Validator {
    
    weak var delegate:ViewModelDelegate?
    var email:String = ""
    var password:String = ""
    
   
    func validate()->Bool{
         let result = self.validate(values: (ValidationType.email,email),(ValidationType.passwordUnap,password))
        
        switch result{
            
        case .success:
            return true
        case .failure(_, let message):
             self.delegate?.showError(message: message.rawValue)
             return false
        }
    }
    
}

//MARK: - API Calls
extension SignInVM{
    
    func performLogin(email:String,password:String,completion:@escaping (_ data:User?)->()){
        self.delegate?.showLoading(message: "Loading")
        NetworkManager.shared.requestForm(endpoint: OnboardingEndpoints.login(email: email, password: password)) { (result:NetworkResult<RegisterResponse>) in
            ChatManager.shared.subscribeForPush()
            self.delegate?.hideLoading()
            
            switch result {
                
            case .success(let data):
                try? self.saveUserToLocal(data: data)
                completion(data.user)
                break
            case .error(let error):
                self.delegate?.showError(message: error.localizedDescription)
            }
            
        }
    }
}

//MARK: - Local Storage
extension SignInVM{
    func saveUserToLocal(data:RegisterResponse) throws  {
        UserDefaultsManager.authToken = data.token
        UserDefaultsManager.bankDetails = data.bank
        UserDefaultsManager.user = data.user
        UserDefaultsManager.studentUniversity = data.university
        do{
            
            try DataManager().saveUser(userData: data.user, token: data.token)
            CoreDataStack.shared.saveContext()
        }catch(let error){
            print("Error:\(error.localizedDescription)")
        }
        
    }
}

