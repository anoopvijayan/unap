//
//  WelcomeVC.swift
//  Unapp
//
//  Created by Levin  on 03/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import UIKit
import Hero


class WelcomeVC: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var lblTerms: UNTermsLabel!
    //MARK: - Properties
    var userType:userType = .student

    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.prepareUI()
    
        
    }
    
    //MARK:- UI Setups
    func prepareUI(){
       self.navigationController?.hero.navigationAnimationType = .fade
       self.lblTerms.delegate = self
    }
    
    //MARK: - IBActions
    @IBAction func studentPressed(_ sender: UIButton) {
        self.userType = .student
       performSegue(withIdentifier: .createAccount, sender: nil)
    }
    
    @IBAction func landloardPressed(_ sender: UIButton) {
        self.userType = .landlord
        performSegue(withIdentifier: .createAccount, sender: nil)
    }
    
    @IBAction func loginPressed(_ sender: UIButton) {
      
        performSegue(withIdentifier: .login, sender: nil)
    }
    
    
    

   
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     
        switch segueIdentifier(for: segue)!{
            
        case .createAccount:
            
            guard  let vc = segue.destination as? CreateAccountVC else { break }
            vc.userType = self.userType
            break
            
        case .login: break
            
        }
    }
    

}
extension WelcomeVC:HeroViewControllerDelegate{
    
    func heroWillStartAnimatingTo(viewController: UIViewController) {
         if viewController is SignInVC{
            
            self.btnLogin.hero.modifiers = [.fade, .translate(x: 0, y: -30, z: 0),.duration(0.2)]
         }else if viewController is CreateAccountVC{
            self.btnLogin.hero.modifiers = nil
        }
        
        
    }
    
    
    
}
