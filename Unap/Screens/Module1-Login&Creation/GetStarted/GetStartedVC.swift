//
//  GetStartedVC.swift
//  Unap
//
//  Created by Levin  on 08/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import UIKit

class GetStartedVC: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var lblTerms: UNTermsLabel!
    var user:User!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblTerms.delegate = self
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: - IBActions
    @IBAction func loginPressed(_ sender: UIButton) {
        
        
        switch self.user.user_type!{
            
        case .landlord: self.navigateToLandloardHome()
            
        case .student: self.navigateToStudentHome()
            
        }
        
        
      
        
    }
    
    func navigateToLandloardHome(){
        
        let vc = UIStoryboard(name: "HomeLandLoard", bundle: nil).instantiateViewController(withIdentifier: "HomeTabbarController")
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    func navigateToStudentHome(){
        
        let vc = UIStoryboard(name: "StudentHome", bundle: nil).instantiateViewController(withIdentifier: "studentTabBarController")
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
