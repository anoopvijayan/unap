//
//  ExploreVC.swift
//  Unap
//
//  Created by Levin  on 16/11/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import UIKit
import GooglePlaces

class ExploreVC: UIViewController,ProfileImageDownloadble,ViewModelDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBarView:UISearchBar!
    var userLatitude:String?
    var userLongitude:String?
    var onStateBlock : ((PermissionStatus) -> Void)?
    
    
    //MARK: - Properties
    var user:User  {
        get {
            return UserDefaultsManager.user!
        }
        set {
            UserDefaultsManager.user = newValue
        }
        
    }
    
    var viewModel:ExploreVM = ExploreVM()
    var homes:[Home] = []
    var isloading:Bool = false
    
    var refreshView: RefreshView!
    var tableViewRefreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = .clear
        refreshControl.tintColor = .clear
        refreshControl.addTarget(self, action: #selector(refreshTableView), for: .valueChanged)
        return refreshControl
    }()
    
    
    //MARK: - UIViewController lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.prepareRefreshUI()
        
        switch PermissionManager.shared.permissionStatus(for: .locationWhileUsing) {
        case .authorized: print("Autharized");
        self.callApi()
        case .denied: showAlertSettings(type: .locationWhileUsing)
        case .notDetermined: showNotificationController(type: .locationWhileUsing) { (state) in
            self.callApi()
            }
        case .restricted:  showAlertSettings(type: .locationWhileUsing)
        }
        
            //Login to chat server
            ChatManager.shared.QBLogin(profile: user)
    
       
       
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.delegate = self
        self.viewModel.getUserDetails { [unowned self](user) in
            self.user = user
        }
    }
    
    
    func callApi()  {
        /**
        userLatitude = "10.010069300000001"
        userLongitude = "76.36583809999999"
        searchHomes(lat: userLatitude ?? "10.013397216796875", long: userLongitude ?? "76.36583809999999")
        **/
        
        guard let currentLocation = LocationWhileUsingPermission().locationManager.location else {
            return
        }
                userLatitude = String(currentLocation.coordinate.latitude)
                userLongitude = String(currentLocation.coordinate.longitude)
        
        searchHomes(lat: userLatitude ?? "10.013397216796875", long: userLongitude ?? "76.36583809999999")
    }
    
    //MARK:- UI Setups
    func prepareRefreshUI(){
        
        // Adding 'tableViewRefreshControl' to tableView
        self.tableView.refreshControl = tableViewRefreshControl
        // Getting the nib from bundle
        getRefereshView()
        guard let url = user.profile_pic else { return }
        self.prefetchPorfilePic(url: URL(string: url))
    }
    
    func getRefereshView() {
        if let objOfRefreshView = Bundle.main.loadNibNamed("RefreshView", owner: self, options: nil)?.first as? RefreshView {
            // Initializing the 'refreshView'
            refreshView = objOfRefreshView
            // Giving the frame as per 'tableViewRefreshControl'
            refreshView.frame = tableViewRefreshControl.frame
            // Adding the 'refreshView' to 'tableViewRefreshControl'
            tableViewRefreshControl.addSubview(refreshView)
        }
    }
    

    //MARK:- IBActions and Selectors
    @objc func refreshTableView() {
        refreshView.startAnimation()
        
        self.isloading = true
        showIndicator(indicatorType: .search)
        viewModel.listAllHomes(loadintial: true, Lat:  self.userLatitude ?? "10.013397216796875", long: self.userLongitude ?? "76.36297367043865") { [unowned self] (data) in
            self.tableViewRefreshControl.endRefreshing()
            self.isloading = false
            guard let home = data else {return}
            self.addNewHomes(newHomes: home)
            self.tableView.reloadData()
            self.hidePopUp()
        }
        
    }
    
    func hideLoading(){
        self.hidePopUp()
    }
   
  
    //MARK :- Data Population
    func searchHomes(lat:String,long:String){
        self.isloading = true
        
        showIndicator(indicatorType: .search)
        homes.removeAll()
       tableView.backgroundView = nil
        viewModel.listAllHomes(loadintial: true, Lat: lat, long: long) { (result) in
            self.isloading = false
            self.hidePopUp()
            if let home = result {
                self.addNewHomes(newHomes: home)
                if self.homes.count > 0 {
                    self.tableView.isHidden = false
                }
                self.tableView.reloadData()
            }
             self.tableView.reloadData()
            
        }
    }
    
    func loadMoreHomes(){
        
        guard viewModel.nextpageAvailable else {return}
        self.isloading = true
        self.viewModel.listAllHomes(Lat: self.userLatitude ?? "10.010069300000001", long: self.userLongitude ?? "76.36583809999999") { (data) in
            self.isloading = false
            guard let home = data else {return}
            self.addNewHomes(newHomes: home)
            self.tableView.reloadData()
        }
    }
    
    func addNewHomes(newHomes:[Home]){
        for item in newHomes{
            if self.homes.contains(where: { (home) -> Bool in
                return  home.id == item.id
            }){
                
            }else{
                self.homes.append(item)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        if segue.identifier == "unmapLoaderHome" {
            let but:UIButton = sender as! UIButton
            if let destinationViewController = segue.destination as? UNMapController {
                destinationViewController.home = self.homes[but.tag]
            }
        }
    }
    @IBAction func unwindToExplore(_ unwindSegue: UIStoryboardSegue) {
        let sourceViewController = unwindSegue.source
        // Use data from the view controller which initiated the unwind segue
    }
    
}

//MARK:- UITableviewDelegate and Datasource
extension ExploreVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.homes.count > 0 {
            tableView.backgroundView = nil
            return 1
        } else {
            TableViewHelper.EmptyMessage(message: "", tableView: tableView)
            return 0
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.homes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let home = self.homes[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellHome") as! HomeListCell
        if let url = home.home_img?.first?.home_image{
            cell.homeImage.kf.indicatorType = .activity
            cell.homeImage.kf.setImage(with: URL(string: url))
        }else{
            cell.homeImage.image = UIImage(named: "homePlaceHolder")
        }
        cell.homeTitle.text = home.home_title ?? "No Title"
        cell.bedCount.setTitle(home.bedDisplayable, for: .normal)
        cell.roomCount.setTitle(home.roomDisplayable, for: .normal)
        cell.bathRoomCount.setTitle(home.bathroomDisplayable, for: .normal)
        cell.cost.setTitle(home.rentDisplayble, for: .normal)
        cell.locationIcon.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.homes.count - 2  && !self.isloading{
            self.loadMoreHomes()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = UIStoryboard(name: "ReUsableControllers", bundle:nil).instantiateViewController(withIdentifier: "HouseDetailVC") as! HouseDetailVC
        controller.home = self.homes[indexPath.row]
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
 }
//MARK:- Searchbar delegates
extension ExploreVC : UISearchBarDelegate {
    public func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool{
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true) {
            let views = autocompleteController.view.subviews
            let subviewsOfSubview = views.first!.subviews
            let subOfNavTransitionView = subviewsOfSubview[1].subviews
            let subOfContentView = subOfNavTransitionView[2].subviews
            let searchBar = subOfContentView[0] as! UISearchBar
            searchBar.text = self.searchBarView.text ?? ""
            searchBar.delegate?.searchBar?(searchBar, textDidChange: self.searchBarView.text ?? "")
        }
        return false
    }
    public func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool{
        return false
    }
    
}
extension ExploreVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        
        searchBarView.text = place.name
        
        userLatitude = String(place.coordinate.latitude)
        userLongitude = String(place.coordinate.longitude)
        /**
        userLatitude = "10.013397216796875"
        userLongitude = "76.36297367043865"
 **/
        searchHomes(lat: userLatitude ?? "10.013397216796875", long: userLongitude ?? "76.36297367043865")
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
