//
//  TenantListVM.swift
//  Unap
//
//  Created by Aneesh on 1/2/19.
//  Copyright © 2019 Levin . All rights reserved.
//

import Foundation

struct TenantListVM{
    weak var delegate:ViewModelDelegate?
}

extension TenantListVM{
    func getTenantListRequests(showLoading:Bool = true,completion:@escaping (_ requests:[TenantListRequest]?)->()){
        if showLoading { self.delegate?.showLoading(message: "loading")}
        NetworkManager.shared.request(endpoint: LandloardAPI.tenantList) { (result:NetworkResult<TenantListRequestResponse>) in
            if showLoading   { self.delegate?.hideLoading() }
            switch result{
                
            case .success(let data):
                completion(data.requests)
                break
            case .error(let error):
                self.delegate?.showError(message: error.localizedDescription)
            }
        }
    }
}
