//
//  TenantListController.swift
//  Unap
//
//  Created by Aneesh on 11/26/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation
import UIKit

class TenantListController: UIViewController {
    
    //MARK: - IBOutlets
    var isloading:Bool = false
    var user:User  {
        get {
            return UserDefaultsManager.user!
        }
        set {
            UserDefaultsManager.user = newValue
        }
        
    }
    var refreshView: RefreshView!
    var tableViewRefreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = .clear
        refreshControl.tintColor = .clear
        refreshControl.addTarget(self, action: #selector(refreshTableView), for: .valueChanged)
        return refreshControl
    }()
    @IBOutlet weak var tenantTable: UITableView!
    var viewModel:TenantListVM = TenantListVM()
     var tenantListRequest:[TenantListRequest]?
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareRefreshUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // self.viewModel.delegate = self
        reFresh()
        
    }
    
    func reFresh()  {
        self.viewModel.getTenantListRequests(showLoading: true) { [unowned self] (result) in
            self.refreshView.stopAnimation()
            self.tenantListRequest = result
            self.tenantTable.reloadData()
        }
    }
    //MARK:- IBActions and Selectors
    //MARK:- UI Setups
    func prepareRefreshUI(){
        
        // Adding 'tableViewRefreshControl' to tableView
        self.tenantTable.refreshControl = tableViewRefreshControl
        // Getting the nib from bundle
        getRefereshView()
        
    }
    //MARK:- IBActions and Selectors
    @objc func refreshTableView() {
        refreshView.startAnimation()
        self.isloading = true
        showIndicator(indicatorType: .search)
        reFresh()
    }
    
    func getRefereshView() {
        if let objOfRefreshView = Bundle.main.loadNibNamed("RefreshView", owner: self, options: nil)?.first as? RefreshView {
            // Initializing the 'refreshView'
            refreshView = objOfRefreshView
            // Giving the frame as per 'tableViewRefreshControl'
            refreshView.frame = tableViewRefreshControl.frame
            // Adding the 'refreshView' to 'tableViewRefreshControl'
            tableViewRefreshControl.addSubview(refreshView)
        }
    }
    
    
    @IBAction func back(sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
}

extension TenantListController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tenantListRequest?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "TenantCell") as! TenantCell
        if let tenant = tenantListRequest?[indexPath.row]{
            
            cell.tenantName.text = tenant.user_details?.first_name
            
            if let requestProfilePic = tenant.user_details?.profile_pic{
                cell.tenantAvatar.kf.setImage(with: URL(string: requestProfilePic), placeholder: UIImage(named: "user"), options: [.transition(.none)], progressBlock: nil, completionHandler: nil)
            }else{
                cell.tenantAvatar.image = UIImage(named: "user")
            }
           
            cell.agreementStatus.text = tenant.agreement_status == 1 ? "Valid":"Expired"
            cell.requestBut.setTitle( tenant.agreement_status == 1 ? "  Valid":"  Renew", for: .normal)
            cell.requestBut.isSelected =  tenant.agreement_status == 1 ? false:true
            cell.requestBut.isHidden =  tenant.agreement_status == 1 ? true:false
            cell.requestBut.setImage( tenant.agreement_status == 1 ? UIImage(named:"tick") :UIImage(named:"close"), for: .normal)
        }
        
        cell.removeTenantBut.tag = indexPath.row
        cell.requestBut.tag = indexPath.row
        cell.paymentsTenantBut.tag = indexPath.row
        
        
        cell.removeTenantBut.addTarget(self, action: #selector(removeTenant(sender:)), for: .touchUpInside)
        cell.requestBut.addTarget(self, action: #selector(renewAgreementRequest(sender:)), for: .touchUpInside)
        cell.paymentsTenantBut.addTarget(self, action: #selector(showPaymentHistory(sender:)), for: .touchUpInside)
        
        
        return cell
    }
}
    
    extension TenantListController{
        //Selectors
        
        @objc func removeTenant(sender:UIButton){
            
            let alert = UIAlertController(title: "Alert", message: "Are you sure? Do you want to remove \(String(describing: tenantListRequest?[sender.tag].user_details?.first_name!)).", preferredStyle: .alert)
            alert.addAction(title: "Yes", style:.destructive) { (sucess) in}
            alert.addAction(title: "No", style:.cancel) { (sucess) in}
            self.present(alert, animated: true, completion:nil)
           
        }
        
      @objc  func showPaymentHistory(sender:UIButton){
            let alert = UIAlertController(title: "Alert", message: "Show Payment History", preferredStyle: .alert)
        alert.addAction(title: "OK", style:.cancel) { (sucess) in}
            self.present(alert, animated: true, completion:nil)
        }
        
      @objc  func renewAgreementRequest(sender:UIButton){
        showSendRequestSuccess(messages: "Agreement Renewal Request Send to \(String(describing: tenantListRequest?[sender.tag].user_details?.first_name!)).")
        }
    
    
}

class TenantCell:UITableViewCell{
    @IBOutlet weak var tenantAvatar: UIImageView!
    @IBOutlet weak var tenantName: UILabel!
    @IBOutlet weak var agreementDuration: UILabel!
    @IBOutlet weak var agreementStatus: UILabel!
    @IBOutlet weak var requestBut: UIButton!
    @IBOutlet weak var removeTenantBut: UIButton!
     @IBOutlet weak var paymentsTenantBut: UIButton!
 
}
