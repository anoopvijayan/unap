//
//  HouseDetailVC.swift
//  Unap Student
//
//  Created by Aneesh on 11/6/18.
//  Copyright © 2018 Aneesh. All rights reserved.
//

import Foundation
import UIKit
import SKPhotoBrowser

class HouseDetailVC: UIViewController,StoryboardInitializable{
    //MARK:- Storyboard Injections
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var bottomButRequestConstraint: NSLayoutConstraint!
    @IBOutlet weak var galleryCollectionView: UICollectionView!
    @IBOutlet weak var SendRequestButHeight: NSLayoutConstraint!
    @IBOutlet weak var tenantBut:UIButton!
    @IBOutlet weak var btnLocation:UIButton!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            tableView.rowHeight = UITableView.automaticDimension
            tableView.estimatedRowHeight = 250
        }
    }
    //MARK:- Variable
    var home:Home?
    var home_request_status:Int?
    //MARK: - Properties
    var user:User  {
        get {
            return UserDefaultsManager.user!
        }
        set {
            UserDefaultsManager.user = newValue
        }
    }
    
    static var storyboardName: String{
        return "ReUsableControllers"
    }
    
    //MARK:- Controller LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        galleryCollectionView.delegate = self
        
        tenantBut.isHidden = user.user_type == .landlord ?false:true
        SendRequestButHeight.constant = user.user_type == .landlord ? 0:50
        if let home_request = home_request_status{
            SendRequestButHeight.constant = home_request == 1 ? 0:50
        }
        if let home_request = home?.home_request_status{
            SendRequestButHeight.constant = home_request == 1 ? 0:50
        }
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // bottomButRequestConstraint.constant = -150
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        //        UIView.animate(withDuration: 0.7, animations: {
        //            self.bottomButRequestConstraint.constant = 20
        //           self.view.layoutIfNeeded()
        //        }) { (done) in
        //        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "unmapLoader" {
            if let destinationViewController = segue.destination as? UNMapController {
                destinationViewController.home = home
            }
        }
    }
    @IBAction func unwindToHomeDetail(_ unwindSegue: UIStoryboardSegue) {
        let sourceViewController = unwindSegue.source
        // Use data from the view controller which initiated the unwind segue
    }
}
//MARK:- IBActions
extension HouseDetailVC{
    @IBAction func backToController(){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func sendRequestToLandlord(){
        
        guard let homeId = self.home?.id else { return }
        self.view.showHud(message: "Loading")
        NetworkManager.shared.request(endpoint: StudentsAPI.requestHome(homeID: "\(homeId)")) { [unowned self](result:NetworkResult<APICommonResponse>) in
            self.view.hideHud()
            switch result{
                
            case .success(let data):
                if data.status == 1{
                    self.home_request_status = 1
                    self.showSendRequestSuccess(messages: "You has been sent a request to \(self.home?.owner_name! ?? ("Landlord")).")
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }else{
                    self.showAlertMessage(message: data.message)
                }
            case .error(let error):
                self.showAlertMessage(message: error.localizedDescription)
            }
            
        }
        
        
    }
}

//MARK:- UITableView Delegate and Datasource
extension HouseDetailVC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  tableView.rowHeight
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "OverViewCell", for: indexPath) as! OverViewCell
            cell.bathroomCount.text = home?.bathroom_count
            cell.bedsCount.text = home?.bed_count
            cell.roomsCount.text = home?.room_count
            cell.cost.text = home?.rentDisplaybleWithOutMonth
            cell.overViewTitle.text = home?.home_title
            cell.hostedBy.text = home?.hostDisplayable
            cell.houseDescription.text = home?.description
            cell.houseType.text = home?.accomodation_type
            if user.user_type == .landlord{
                cell.hostedBy.text = "Hosted by \(user.first_name ?? "")"
            }
            return cell
        }else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AmentiesCell", for: indexPath) as! AmentiesCell
            cell.addArrangedViews(dataArray: home?.amenities ?? [] , stackView: cell.stackViewVertical, isAmenties: true)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BillsCell", for: indexPath) as! BillsCell
            cell.addArrangedViews(dataArray: home?.bills ?? [], stackView: cell.stackViewVertical, isAmenties: false)
            return cell
        }
        
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let _ = scrollView as? UITableView {
            // print(scrollView.contentOffset.y)
            if scrollView.contentOffset.y < 0 {
            } else {}}else if let _ = scrollView as? UICollectionView {}
        
    }
    
}
//MARK:- UICollectionView Delegate & DataSource
extension HouseDetailVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let count = home?.home_img?.count{
            pageController.numberOfPages =  count
            pageController.isHidden = !( count > 1)
            return  count == 0 ? 1 : count
        }else{
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath as IndexPath) as! imageCell
        if let count = home?.home_img?.count{
            if count > 0{
                if let url = home?.home_img?[indexPath.row].home_image{
                    cell.houseImage.kf.indicatorType = .activity
                    cell.houseImage.kf.setImage(with: URL(string: url))
                }else{
                    cell.houseImage.image = UIImage(named: "homePlaceHolder")
                }
            }else{
                cell.houseImage.image = UIImage(named: "homePlaceHolder")
            }}
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let count = home?.home_img?.count{
            if count > 0{
                var images = [SKPhoto]()
                for item  in home?.home_img ?? []{
                    let photo = SKPhoto.photoWithImageURL(item.home_image!)
                    photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
                    images.append(photo)
                }
                // 2. create PhotoBrowser Instance, and present.
                let browser = SKPhotoBrowser(photos: images)
                browser.initializePageIndex(0)
                present(browser, animated: true, completion: {})
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: galleryCollectionView.bounds.size.width , height: galleryCollectionView.bounds.size.height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let _ = scrollView as? UICollectionView{
            pageController?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        }
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        if let _ = scrollView as? UICollectionView{
            pageController?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        }
    }
}

//MARK :- UICollectionViewCell Class
//House Images
class imageCell: UICollectionViewCell {
    @IBOutlet weak var houseImage: UIImageView!
}

//MARK :- UITableViewCell Class
//House Details
class OverViewCell:UITableViewCell{
    @IBOutlet weak var roomsCount: UILabel!
    @IBOutlet weak var bedsCount: UILabel!
    @IBOutlet weak var bathroomCount: UILabel!
    @IBOutlet weak var cost: UILabel!
    @IBOutlet weak var overViewTitle: UILabel!
    @IBOutlet weak var houseType: UILabel!
    @IBOutlet weak var hostedBy: UILabel!
    @IBOutlet weak var houseDescription: UILabel!
}
class AmentiesCell: UITableViewCell {
    @IBOutlet weak var stackViewVertical: UIStackView!
}
class BillsCell: UITableViewCell{
    @IBOutlet weak var stackViewVertical: UIStackView!
}






