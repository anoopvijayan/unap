//
//  HomeStudentCell.swift
//  Unap
//
//  Created by Aneesh on 12/27/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation
import UIKit


protocol paymentActions : class {
    func didPerformedPaymentAction(row:Int)
}

class HomeStudentCell:UITableViewCell{
    @IBOutlet weak var butPayment:UIButton!
    @IBOutlet weak var lblHomeTitle: UILabel!
    @IBOutlet weak var lblPaymentDue: UILabel!
    @IBOutlet weak var lbluserName: UILabel!
    @IBOutlet weak var imgViewHome: UIImageView!
    @IBOutlet weak var imgViewProfile: CircularImageView!
    
    
    //delegate
    
    weak var delegate:paymentActions?
    var row:Int = 0
    
    
    
    //MARK: IBActions
    @IBAction func paymentBtnpressed(_ sender: UIButton) {
        self.delegate?.didPerformedPaymentAction(row: self.row)
    }
    
}

