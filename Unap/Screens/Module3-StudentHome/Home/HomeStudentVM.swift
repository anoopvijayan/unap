//
//  HomeStudentVM.swift
//  Unap
//
//  Created by Aneesh on 12/27/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation
import UIKit

struct HomeStudentVM {
    
    weak var delegate:ViewModelDelegate?
    
    func getHomeRequests(showLoading:Bool = true,completion:@escaping (_ requests:[HomeRequest]?)->()){
        if showLoading { self.delegate?.showLoading(message: "loading")}
        NetworkManager.shared.request(endpoint: StudentsAPI.acceptedHomeList()) { (result:NetworkResult<HomeRequestResponse>) in
            if showLoading   { self.delegate?.hideLoading() }
            switch result{
                
            case .success(let data):
                completion(data.requests)
                break
            case .error(let error):
                self.delegate?.showError(message: error.localizedDescription)
            }
        }
    }
}

