//
//  HomeStudentVC.swift
//  Unap
//
//  Created by Levin  on 07/11/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import UIKit
import Braintree
import BraintreeDropIn


class HomeStudentVC: UIViewController {

    //MARK: - IBOutlets
    var isloading:Bool = false
    @IBOutlet weak var tableView: UITableView!
    //MARK: - Properties
    var user:User  {
        get {
            return UserDefaultsManager.user!
        }
        set {
            UserDefaultsManager.user = newValue
        }
        
    }
    var refreshView: RefreshView!
    var tableViewRefreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = .clear
        refreshControl.tintColor = .clear
        refreshControl.addTarget(self, action: #selector(refreshTableView), for: .valueChanged)
        return refreshControl
    }()
    //MARK:- Properties
    var viewModel:HomeStudentVM = HomeStudentVM()
    var paymentModel:BraintreeManager = BraintreeManager()
    var homeRequest:[HomeRequest]?
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareRefreshUI()
        }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // self.viewModel.delegate = self
        reFresh()
        
    }
    
    func reFresh()  {
        self.refreshView.startAnimation()
        self.viewModel.getHomeRequests(showLoading: false) {[unowned self] (result) in
            self.refreshView.stopAnimation()
            self.homeRequest = result
            self.tableView.reloadData()
        }
    }
    //MARK:- IBActions and Selectors
    //MARK:- UI Setups
    func prepareRefreshUI(){
        
        // Adding 'tableViewRefreshControl' to tableView
        self.tableView.refreshControl = tableViewRefreshControl
        // Getting the nib from bundle
        getRefereshView()
        
    }
    
    func getRefereshView() {
        if let objOfRefreshView = Bundle.main.loadNibNamed("RefreshView", owner: self, options: nil)?.first as? RefreshView {
            // Initializing the 'refreshView'
            refreshView = objOfRefreshView
            // Giving the frame as per 'tableViewRefreshControl'
            refreshView.frame = tableViewRefreshControl.frame
            // Adding the 'refreshView' to 'tableViewRefreshControl'
            tableViewRefreshControl.addSubview(refreshView)
        }
    }
    
    
    //MARK:- IBActions and Selectors
    @objc func refreshTableView() {
        refreshView.startAnimation()
        self.isloading = true
        showIndicator(indicatorType: .search)
        reFresh()
    }

}

extension HomeStudentVC:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = self.homeRequest?.count {
            self.tableView.isHidden = false
        }else{
            self.tableView.isHidden = true
        }
        return self.homeRequest?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeStudentCell") as! HomeStudentCell
        cell.row = indexPath.row
        cell.delegate = self
        if let request = self.homeRequest?[indexPath.row],
            let home = request.home,
            let requestedBy = request.requested_by
        {
            if let paymentDue = request.payment_message{
              cell.lblPaymentDue.text = paymentDue
               
            }
            if let paymentStatus = request.payment_status{
                 cell.lblPaymentDue.textColor = paymentStatus == 1 ? UIColor.green:UIColor.red
                 cell.butPayment.isHidden = paymentStatus == 1 ? true:false
            }
            
            if let requestProfilePic = requestedBy.profile_pic{
                cell.imgViewProfile.kf.setImage(with: URL(string: requestProfilePic), placeholder: UIImage(named: "user"), options: [.transition(.none)], progressBlock: nil, completionHandler: nil)
            }else{
                cell.imgViewProfile.image = UIImage(named: "user")
            }
            if let requestName = requestedBy.first_name{
                cell.lbluserName.text = requestName
            }
            if let homePic = home.home_img?.first?.home_image{
                cell.imgViewHome.kf.setImage(with: URL(string: homePic))
            }
            if let homeName = home.home_title{
                cell.lblHomeTitle.text =  homeName
            }
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = UIStoryboard(name: "ReUsableControllers", bundle:nil).instantiateViewController(withIdentifier: "HouseDetailVC") as! HouseDetailVC
        controller.home_request_status = self.homeRequest?[indexPath.row].home_request_status
        controller.home = self.homeRequest?[indexPath.row].home
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
}
extension HomeStudentVC:paymentActions{
    func didPerformedPaymentAction(row: Int) {
        if let data = self.homeRequest?[row],let _ = data.home_request_id{
            paymentModel.getBraintreeClientTokenRequests { (token) in
                if let clientToken = token?.client_token{
                    BraintreeManager().showDropIn(clientTokenOrTokenizationKey: clientToken
                        , controller: self
                        , amount: data.home?.home_price
                        , completion: { (nonce, amount) in
                            
                            if let nonce = nonce, let amount = amount {
                                self.paymentModel.sendRequestPaymentToServer(nonceKey: nonce, payment:amount, completion: { (done) in
                                    
                                    self.showAlertMessage(message: done?.message ?? "Pyment Completed")
                                  self.reFresh()
                                })
                            }
                          
                            
                    })
                }
               
            }
            
            
        }
        
    }
    
    
    
    
}
