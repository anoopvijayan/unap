//
//  ProfileDetailStudentVC.swift
//  Unap
//
//  Created by Levin  on 15/11/18.
//  Copyright © 2018 Levin . All rights reserved.
//



import UIKit
import Kingfisher


protocol profileUpdateDelegate: class{
    func updatedNewProfilePic(image:UIImage)
}

class ProfileDetailStudentVC: UITableViewController,ImagePickerPresentable,ViewModelDelegate {
    
    // MARK:- IBOutlets
    @IBOutlet weak var imagViewBg: UIImageView!
    @IBOutlet weak var imagViewProfile: UIImageView!
    @IBOutlet weak var TFName: UITextField!
    @IBOutlet weak var TFAge: UITextField!
    @IBOutlet weak var TFEmail: UITextField!
    @IBOutlet weak var TFUniversity: UITextField!
    
    
    // MARK: - Properties
    weak var delegate:profileUpdateDelegate?
    var viewModel:ProfileDetailsStudentVM = ProfileDetailsStudentVM()
    var pickedImage:UIImage?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.delegate = self
        self.prepareUI()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action:#selector(handleTap(sender:)))
        
        self.view.addGestureRecognizer(tap)
    }
    
    // MARK: - UI Setups
    func prepareUI(){
        let perfillUI = {
            guard let user = UserDefaultsManager.user else { return }
            DispatchQueue.main.async{
                self.TFName.text = user.first_name?.capitalized ?? ""
                self.TFAge.text = user.age_or_dob ?? ""
                self.TFEmail.text = user.email ?? ""
                

                
                self.TFUniversity.isUserInteractionEnabled = false
                guard let university = UserDefaultsManager.studentUniversity else { return }
                self.TFUniversity.text = university.university_name ?? "No University"
                
                guard let imageURL = user.profile_pic, let url = URL(string: imageURL), self.pickedImage == .none   else {return}
                let image = ImageCache.default.retrieveImageInDiskCache(forKey: url.absoluteString)
                
                self.imagViewProfile.image = image
                self.imagViewBg.image = image
            }}
        
        perfillUI()
        
    }
    
    
    
    func bindUI(){
        
        viewModel.name = TFName.text ?? ""
        viewModel.age  = TFAge.text  ?? ""
        
        guard let profilePic = self.imagViewBg.image, let data = profilePic.jpegData(compressionQuality: 0.8) else{
            return
        }
        viewModel.imageData = data
        guard let universityID = UserDefaultsManager.user?.university_id else { return }
        viewModel.universityID = universityID
        
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        // handling code
        self.view.endEditing(true)
    }
    
    
    
    // MARK: - IBACtions
    @IBAction func backPressed(_ sender: UIButton) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    @IBAction func pickImagePressed(_ sender: UIButton) {
        self.showImagePicker { (image) in
            self.imagViewBg.image = image
            self.imagViewProfile.image = image
            self.pickedImage = image
            
        }
    }
    @IBAction func editBtnPressed(_ sender: UIButton) {
        
        self.bindUI()
        guard let endpoint = viewModel.validate() else { return }
        
        viewModel.performUpdate(endpoint: endpoint) { [weak self] (user) in
            if let image = self?.pickedImage{
                self?.delegate?.updatedNewProfilePic(image: image)
            self?.pickedImage = nil
            }
            
        
        }
        
        
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
