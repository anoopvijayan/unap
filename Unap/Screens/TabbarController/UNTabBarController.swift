//
//  UNTabBarController.swift
//  Unap
//
//  Created by Levin  on 28/12/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import UIKit

class UNTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
        NotificationCenter.default.addObserver(forName: .UNnewMessage, object: nil, queue: .main) { [unowned self](notification) in
            if self.selectedIndex != 2{
                if let tabItems = self.tabBar.items {
                    // In this case we want to modify the badge number of the third tab:
                    let tabItem = tabItems[2]
                    tabItem.badgeValue = "!"
                }
            }
        }
        /// Connecting to chat server
        /// This call will make user to go online
        if let user = UserDefaultsManager.user{
            ChatManager.shared.connectionDelegate?.chatWillConnect()
            ChatManager.shared.QBLogin(profile: user)
            
            
        }
    }
    
    // Override selectedViewController for User initiated changes
    override var selectedViewController: UIViewController? {
        didSet {
            tabChangedTo(selectedIndex: selectedIndex)
        }
    }
    // Override selectedIndex for Programmatic changes
    override var selectedIndex: Int {
        didSet {
            tabChangedTo(selectedIndex: selectedIndex)
        }
    }
    
    // handle new selection
    func tabChangedTo(selectedIndex: Int) {
        if selectedIndex == 2 {
            if let tabItems =  self.tabBar.items {
                let tabItem = tabItems[2]
                tabItem.badgeValue = nil
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    deinit {
        print("Remove NotificationCenter Deinit")
        NotificationCenter.default.removeObserver(self)
    }

}
