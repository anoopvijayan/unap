//
//  ChatViewController.swift
//  CoupleShip
//
//  Created by Levin  on 01/08/17.
//  Copyright © 2017 Levin . All rights reserved.
//

import UIKit
import RSKKeyboardAnimationObserver
import RSKGrowingTextView
import CoreData
import SKPhotoBrowser
import Kingfisher
import SVProgressHUD



class ChatViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,MessageDelegate,UITextViewDelegate,NSFetchedResultsControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,URLSessionDelegate,ImagePickerPresentable {
    // MARK: - IBOutlets
    @IBOutlet weak var lblPresence: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var imgTyping: UIImageView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var textView: RSKGrowingTextView!
    @IBOutlet weak var bottomCon: NSLayoutConstraint!
    @IBOutlet weak var tblChat: UITableView!
    
    // MARK: - Properties
    
    var match:UNConversation?
    var previousSection:Int?
    var expandedIndex:IndexPath?
    var recipientProfile:User?
    var recipientQb:QBUUser?
    var MessageArray:[QBChatMessage]? = []
    var chatDialog:QBChatDialog?
    var isLoading:Bool? = false {
        willSet(newValue){
            if newValue!{
                self.view.showChatHud(message: "Load More", position: CGPoint(x: 0, y: 84))
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                  
                    self.view.hideChatHud()
                }
                
            }
        }
    }
    var isChatFullyLoaded:Bool? = false
    let typingArray:[UIImage] = [UIImage(named: "Typing1")!,UIImage(named: "Typing2")!,UIImage(named: "Typing3")!]
    var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>!
    let imagePicker:UIImagePickerController = UIImagePickerController()
    let dateformatter: DateFormatter = {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "HH:mm"
        return dateformatter
       
    }()
    let FetchSize:Int = 25
    
    private var isVisibleKeyboard = true
    

    //MARK: - ViewController life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.match?.isNew = 0
       // CoreDataStore().saveDB()
        
        
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor = UIColor.greenUN
        statusBarView.backgroundColor = statusBarColor
        view.addSubview(statusBarView)
        // Do any additional setup after loading the view.
        let gesture = UITapGestureRecognizer(target: self, action:#selector(ChatViewController.tapped))
        gesture.cancelsTouchesInView = false
        self.tblChat.addGestureRecognizer(gesture)
        
      //  ChatManager.shared.getConversations()
        textView.delegate = self
        
        tblChat.estimatedRowHeight = 50
        tblChat.rowHeight = UITableView.automaticDimension
        self.lblName.text = recipientProfile?.first_name ?? "No Name"
        if let media = recipientProfile?.profile_pic{
            imgAvatar.kf.setImage(with: URL(string: media))
        }

        self.setupchatSetting()
        ChatManager.shared.PresenceDelegate = self
        if let qbUUD = recipientProfile?.qblocks_id{
            if let contactItem = ChatManager.shared.addAsBuddy(quibloxId: qbUUD){
                
                if contactItem.isOnline{
                    lblPresence.text = "online"
                }else{
                    lblPresence.text = "offline"
                }
                
            }
        }
      
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
        ChatManager.shared.connectionDelegate = self
        if ChatManager.shared.isConnecting() {
            self.view.showChatHud(message: "Connecting to chat server")
        }
        
        
    }
    
    
    
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.registerForKeyboardNotifications()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.unregisterForKeyboardNotifications()
    }

    //MARK:-
    func setupchatSetting(){
        
        var alreadySynched:Bool = false
        
        if let  qbdialog:QBChatDialog = match?.qbDialog as? QBChatDialog{
            
          //  self.recipientQb = otherUser
            self.chatDialog = qbdialog
            self.startObservingTypingStatus()
            self.fetchMessages(dialogId: (self.chatDialog?.id)!)
            self.tblChat.delegate = self
            self.tblChat.dataSource = self
            alreadySynched = true
            return
        }
        
        if let coupleID = recipientProfile?.qblocks_id{
//            self.lblName.text = recipientProfile?.partner_one_name?.capitalized
//            if let media = recipientProfile?.imageDetails?[safe:0]{
//                imgAvatar.af_setImage(withURL: URL(string: media.media!)!)
//            }
        
           self.view.showHud(message: "Fetching User")
            ChatManager.shared.getQuickBlockUser(coupleId: "\(coupleID)") {[weak self] (user) in
                
              //  self?.recipientQb = user
                // ChatManager.shared.messageDelegate = self
                
                if self?.chatDialog == nil{
                    ChatManager.shared.createDialog(recipientID:UInt((self?.recipientProfile?.qblocks_id)!)! , completion: {[weak self] (dialog) in
                        
                        if alreadySynched{
                            self?.match?.qbDialog = dialog
                           // self?.match?.qbOtherUser = user
                            CoreDataStack.shared.saveContext()
                        }else{
                            self?.chatDialog = dialog
                            self?.match?.qbDialog = dialog
                           // self?.match?.qbOtherUser = user
                           CoreDataStack.shared.saveContext()
                            self?.startObservingTypingStatus()
                            self?.fetchMessages(dialogId: (self?.chatDialog?.id)!)
                            self?.tblChat.delegate = self
                            self?.tblChat.dataSource = self
                        }
                        
                       
                        self?.view.hideHud()
                    })
                }
                
            }
            //evide vare
           
        }
    }
    
    
    
    @objc func tapped(){
        self.view.endEditing(true)
    }
    func startObservingTypingStatus(){
        
        imgTyping.animationImages = self.typingArray
        imgTyping.animationDuration = 1
        
        self.chatDialog?.onUserIsTyping = {[weak self](userID: UInt) in
            print("typing")
            self?.imgTyping.isHidden = false
            self?.imgTyping.startAnimating()
        }
        
        self.chatDialog?.onUserStoppedTyping = {[weak self](userID: UInt) in
            self?.imgTyping.isHidden = true
            self?.imgTyping.stopAnimating()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
       private func adjustContent(for keyboardRect: CGRect) {
        let keyboardHeight = keyboardRect.height
        let keyboardYPosition = self.isVisibleKeyboard ? keyboardHeight : 0.0;
        self.bottomCon.constant = keyboardYPosition
        if self.isVisibleKeyboard{

             self.bottomConstraint.constant = (keyboardYPosition-49)
            
        }else{
            self.bottomConstraint.constant = 0

        }
        self.view.layoutIfNeeded()
    }
    private func registerForKeyboardNotifications() {
        self.rsk_subscribeKeyboardWith(beforeWillShowOrHideAnimation: nil,
                                       willShowOrHideAnimation: { [unowned self] (keyboardRectEnd, duration, isShowing) -> Void in
                                        self.isVisibleKeyboard = isShowing
                                        self.adjustContent(for: keyboardRectEnd)
            }, onComplete: { (finished, isShown) -> Void in
                self.isVisibleKeyboard = isShown
        }
        )
        
        self.rsk_subscribeKeyboard(willChangeFrameAnimation: { [unowned self] (keyboardRectEnd, duration) -> Void in
            self.adjustContent(for: keyboardRectEnd)
            }, onComplete: nil)
    }
    
    private func unregisterForKeyboardNotifications() {
       self.rsk_unsubscribeKeyboard()
    }
    
    func ChatMessageRecieved(message: QBChatMessage?) {
        
    }
    
    func DeliveryStatus(messageID: String?, dialogID: String?, toUserID userID: UInt?) {
        
    }
    
    
    // MARK: - UITableView Delegate and Datasource
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
       // return 1
        guard let sectionCount = fetchedResultsController.sections?.count else {
            return 0
        }
        previousSection = sectionCount
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCell(withIdentifier: "Header")!
        let view:UIView = UIView(frame: CGRect(x: 0, y: 0, width: Config.screenWidth, height: header.frame.height))
        header.setNeedsLayout()
        header.layoutIfNeeded()
        view.addSubview(header)
        let titlelbl:UILabel = header.viewWithTag(10) as! UILabel
        titlelbl.text = fetchedResultsController.sections?[section].name
        titlelbl.frame.origin = CGPoint(x: Config.screenWidth/2 - titlelbl.frame.size.width/2, y: 0)
        
        return view
        
        }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
            return 0
        }
        return sectionData.numberOfObjects
    }
    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return fetchedResultsController.sections?[section].name
//    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if !isLoading! && indexPath.section == 0 && indexPath.row == 0{
            if isChatFullyLoaded! {
                return
            }
            isLoading = true
            var fetchCount = fetchedResultsController.fetchRequest.fetchOffset
            
            
            if (fetchCount - FetchSize) > 0{
                
            }else{
                fetchedResultsController.fetchRequest.fetchOffset = 0
                fetchedResultsController.fetchRequest.fetchLimit =  fetchedResultsController.fetchRequest.fetchLimit + fetchCount

                NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: nil)
                do {
                    try fetchedResultsController.performFetch()
                    isLoading = false
                    UIView.performWithoutAnimation {
                        tblChat.reloadData()

                    }

                } catch _ {
                    print("Fetch failed")
                }
                fetchCount = 0
                isChatFullyLoaded = true
            }
            if fetchCount == 0{
                isLoading = false
                return
            }
            print("fetchCount:\(fetchCount)")
            fetchedResultsController.fetchRequest.fetchOffset=fetchedResultsController.fetchRequest.fetchOffset - FetchSize
            fetchedResultsController.fetchRequest.fetchLimit =  fetchedResultsController.fetchRequest.fetchLimit + FetchSize
            
            NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: nil)
            do {
                try fetchedResultsController.performFetch()
                isLoading = false
                UIView.performWithoutAnimation {
                tblChat.reloadData()
                   
                }
                
            } catch _ {
                print("Fetch failed")
            }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = fetchedResultsController.object(at: indexPath) as! UNMessage
        
        let qbmessag:QBChatMessage = message.qbMessage as! QBChatMessage
        
        var hasAttachments:Bool = false
        var cell:ChatTableViewCell
        
     
        
        if message.senderID == Int64((QBChat.instance.currentUser?.id)!){
            
            if let _ = qbmessag.attachments {
               // cell = (tableView.dequeueReusableCell(withIdentifier: "chatCellWithAttachment") as? ChatTableViewCell)!
                 cell = (tableView.dequeueReusableCell(withIdentifier: "chatCellRecWithAttachment") as? ChatTableViewCell)!
                hasAttachments = true
                cell.imageAttachment.image = UIImage(named:"placeholder")
            }else{
                 //cell = (tableView.dequeueReusableCell(withIdentifier: "ChatCell") as? ChatTableViewCell)!
                cell = (tableView.dequeueReusableCell(withIdentifier: "ChatCellRec") as? ChatTableViewCell)!
                
//                if !message.isSeen{
//                    QBChat.instance.read(qbmessag, completion: nil)
//                    message.isSeen = true
//                    if (self.match?.unreadMessageCount)! > 0{
//                    self.match?.unreadMessageCount -= 1
//                    }
//                }
            }
            
        }else{
            if let _ = qbmessag.attachments {
               // cell = (tableView.dequeueReusableCell(withIdentifier: "chatCellRecWithAttachment") as? ChatTableViewCell)!
                cell = (tableView.dequeueReusableCell(withIdentifier: "chatCellWithAttachment") as? ChatTableViewCell)!
                hasAttachments = true
                cell.imageAttachment.image = UIImage(named:"placeholder")
            }else{
              //  cell = (tableView.dequeueReusableCell(withIdentifier: "ChatCellRec") as? ChatTableViewCell)!
                cell = (tableView.dequeueReusableCell(withIdentifier: "ChatCell") as? ChatTableViewCell)!
            }

            
            if !message.isSeen{
                QBChat.instance.read(qbmessag, completion: nil)
                message.isSeen = true
                if (self.match?.unreadMessageCount)! > 0{
                self.match?.unreadMessageCount -= 1
                }
            }

        }
        cell.delegate = self
        
        cell.selectionStyle = .none
        if !hasAttachments{
       cell.lblName.text = message.message
        }else{
            if !isLoading!{
            
            let currentIndex = indexPath
            if let attach:QBChatAttachment = qbmessag.attachments?.first{
            //cell.downloadImage(attachId: attach.id!)
                if let attchId = attach.id{
     
                self.downloadImageNew(attachId: attchId, comp: { (image) in
                    DispatchQueue.main.async {
                        
                        
                        if let cell:ChatTableViewCell  = tableView.cellForRow(at: currentIndex) as? ChatTableViewCell{
                            if let im = image{
                        cell.ImageDownloaded = true
                        cell.imageAttachment.image = im
                            }
                        }
                   // self.tblChat.reloadData()
                    }
                })
                     }
            }
    }
        }
        
        
        
        if message.isDelivered{
            cell.imgSeen.isHidden = false
        }else{
            cell.imgSeen.isHidden = true
        }
        
        if message.isDelivered && message.isSeen{
            cell.imgSeen.image = UIImage(named: "tickChat2")
        }else{
            cell.imgSeen.image = UIImage(named: "tickChat")
        }

        
        cell.lblDate.text = self.dateformatter.string(from: message.dateSent! as Date)
        if let expanded:IndexPath = self.expandedIndex{
            if expanded == indexPath{
                cell.dateHeight.constant = 13.5
            }else{
              // cell.dateHeight.constant = 0
            }
        }else{
             cell.dateHeight.constant = 0
        }
        
        
        return cell
    }
    
    func getImageFromCache(attachId:String,comp:@escaping (_ img:UIImage?)->()){
        let result =  ImageCache.default.isImageCached(forKey: attachId)
        
        if result.cached == true {
            
            ImageCache.default.retrieveImage(forKey: attachId, options: nil, completionHandler: { (image, cache) in
                if image != nil{
                    comp(image)
                }
            })
            
        }
       comp(nil)
    }
    
    func downloadImageNew(attachId:String,comp:@escaping (_ img:UIImage?)->()){
        
        let result =  ImageCache.default.isImageCached(forKey: attachId)

        if result.cached == true {
         
          ImageCache.default.retrieveImage(forKey: attachId, options: nil, completionHandler: { (image, cache) in
            if image != nil{
                comp(image)
            }
            })
            
        }
        else{

//            QBRequest.backgroundDownloadFile(withID: UInt(attachId)!, successBlock: { (response, data) in
//                DispatchQueue.main.async {
//                if response.isSuccess{
//                let image: UIImage? = UIImage(data: data)
//                ImageCache.default.store(image!, forKey: attachId)
//                comp(image)
//                    }
//                }
//                
//            }, statusBlock: nil, errorBlock: nil)
            
            
            QBUtil.qbDownload(with: attachId, successAction: { (Image) in
                ImageCache.default.store(Image!, forKey: attachId)
                comp(Image)
            }, errorAction: {
                
            })
            
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let expanded = expandedIndex{
            if expanded == indexPath{
                expandedIndex = nil
            } else{
                //expandedIndex = indexPath
            }

        }
        else{
        expandedIndex = indexPath
        }
        
        tableView.beginUpdates()
        tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
        tableView.endUpdates()
    }
    
    func openGallery(){
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            // Use editedImage Here
            self.uploadImageToQuickBlox(image: editedImage)
        } else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            // Use originalImage Here
            self.uploadImageToQuickBlox(image: originalImage)
        }
        picker.dismiss(animated: true)
    }
    
    @IBAction func Send(_ sender: UIButton) {
        
        guard let sender = QBChat.instance.currentUser?.id, let recipient = self.recipientProfile?.qblocks_id,!textView.isEmpty else{
            return
        }
        
        ChatManager.shared.sendMessage(dialog: self.chatDialog!,senderID: sender, recipientId: UInt(recipient)!, text: textView.text,converstation:self.match!)
        textView.text = ""
        if let dialog  = self.chatDialog{
            dialog.sendUserStoppedTyping()
        }
        

    }
    
    func fetchMessages(dialogId:String) {
        isLoading = true
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "UNMessage")
        
        let fetchSort = NSSortDescriptor(key: "dateSent", ascending: true)
        fetchRequest.sortDescriptors = [fetchSort]

        let predicate = NSPredicate(format: "dialogID == %@",dialogId)
        let count = DataManager.fetchCountFor(entityName: "UNMessage", predicate: predicate, onMoc: CoreDataStack.shared.mainContext)
        if count > 25{
         fetchRequest.fetchOffset = count-FetchSize
         fetchRequest.fetchLimit  = FetchSize

        }else{
           // fetchRequest.fetchOffset = count
          //  fetchRequest.fetchLimit  = count

        }
        
        
    fetchRequest.predicate = predicate
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: CoreDataStack.shared.mainContext, sectionNameKeyPath: "section", cacheName: nil)
        
        fetchedResultsController.delegate = self
        
        do {
            try fetchedResultsController.performFetch()
             self.isLoading = true
            if (fetchedResultsController.fetchedObjects?.count)! > 0{
                tblChat.reloadData()
                DispatchQueue.main.async {
                    self.tblChat.isHidden = true
                    self.tblChat.scrollToBottom()
                    self.tblChat.isHidden = false
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) { [weak self] in
                        // your code here
                        self?.isLoading = false
                    }
                }
               
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) { [weak self] in
                    // your code here
                    self?.isLoading = false
                }
                
            }
            
            
        } catch let error as NSError {
            print("Unable to perform fetch: \(error.localizedDescription)")
        }
    }
    
    
    
    
   // MARK: - UITextView delegates
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let dialog  = self.chatDialog{
            dialog.sendUserIsTyping()
        }
        if text == ""{
            if let dialog  = self.chatDialog{
                dialog.sendUserStoppedTyping()
            }
        }

       return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if let dialog  = self.chatDialog{
            dialog.sendUserStoppedTyping()
        }
    }
   
    
    // MARK: - FetchedResultsController Delegate
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        
        if (controller.fetchedObjects?.count)! > 0{
        tblChat.beginUpdates()
            
        }
        
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type{
        case .insert:
//            if (controller.fetchedObjects?.count)! > 0{
//            tblChat.insertSections(IndexSet(integer: sectionIndex), with: .none)
//            }else{
            if sectionIndex == 0{
                tblChat.reloadData()
            }else{
               tblChat.insertSections(IndexSet(integer: sectionIndex), with: .none) 
            }
        //    }
            
            
        default: break
            
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
    
        if (controller.fetchedObjects?.count)! > 0{
           
        tblChat.endUpdates()
        tblChat.scrollToBottom(animated: false)
        }
    }
    
    
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        // 2
        
        switch type {
        case .insert:
            
            if newIndexPath?.row == 0{
            self.tblChat.reloadData()
                
            }else{
                if indexPath?.section != newIndexPath?.section{
                   
                     self.tblChat.insertRows(at: [newIndexPath! as IndexPath], with: .none)
                    
                }else{
                    tblChat.endUpdates()
                    self.tblChat.reloadData()
                }
           
            }
           
        case .delete:
            tblChat.deleteRows(at: [indexPath!], with: .none)
        case .update:
//            UIView.performWithoutAnimation {
                self.tblChat.reloadRows(at: [indexPath!], with: .none)

//            }
            break;
        case .move:
            if let indexPath = indexPath, let newIndexPath = newIndexPath {
                //tblChat.moveRow(at: indexPath, to: newIndexPath)
                
                tblChat.deleteRows(at: [indexPath], with: .none)
                tblChat.insertRows(at: [newIndexPath], with: .none)
                
            }
            break;
        
        }
    }
    
    
  
    @IBAction func AttachmentClicked(_ sender: UIButton) {
        
        let alertSheet = UIAlertController(title: "Choose", message: "", preferredStyle: .actionSheet)
        
        alertSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { [weak self](action) in

            self?.showImagePicker(result: {[weak self] (image) in
                DispatchQueue.main.async {
                    self?.uploadImageToQuickBlox(image: image)
                }
            })
            
            }
        ))
        
        alertSheet.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { [weak self](action) in
            
//            MediaManager.shared.openGallery(controller: self!, select: { [weak self](image) in
//                 DispatchQueue.main.async {
//                self?.uploadImageToQuickBlox(image: image)
//                   }
//            }, cancel: { 
//                
//            })
            self?.openGallery()
            
        }))
        
        alertSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
             DispatchQueue.main.async {
           alertSheet.dismiss(animated: true, completion: nil)
            }
        }))
        
        self.present(alertSheet, animated: true, completion: nil)
        
        
    }
    
    func uploadImageToQuickBlox(image:UIImage){
        self.view.showHud(message: "Uploading")
        
        let qbutil:QBUtil = QBUtil()
        
        qbutil.qbUploadImage(image, succesAction: {[weak self] (qcblob) in
            SVProgressHUD.dismiss()
            let message: QBChatMessage = QBChatMessage.markable()
            
            
            let attachment: QBChatAttachment = QBChatAttachment()
            attachment.type = "image"
            attachment.id = qcblob?.uid
            attachment.url = qcblob?.publicUrl()
            message.attachments = [attachment]
            // Send message
            
            guard let sender = QBChat.instance.currentUser?.id, let recipient = self?.recipientProfile?.qblocks_id  else{
                return
            }
            
            ChatManager.shared.sendMessageWithAttachment(dialog: (self?.chatDialog!)!, senderID: sender, recipientId: UInt(recipient)!, message: message,match:(self?.match!)!)
        }) {
          SVProgressHUD.showError(withStatus: "This image canno't be uploaded.")
        }
        
        

    }
    
     deinit{
        print("chat deallocated.")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func ProfileClicked(_ sender: UIButton) {
//        if let other = recipientProfile{
//        let userDetailController =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UserDetailsViewController") as! UserDetailsViewController
//            userDetailController.otherProfile = other
//            userDetailController.isOtherProfile = true
//            userDetailController.match = self.match
//            self.navigationController?.pushViewController(userDetailController, animated: true)
//        }
    }
    
    @IBAction func backPressed(_ sender: UIButton) {
       
        
        QBRequest.markMessages(asRead: nil, dialogID: (self.chatDialog?.id)!, successBlock: { (Response) in
            print("Marked")
        }) { (Response) in
            print("ERROR")
        }
        if let user = QBChat.instance.currentUser{
        if let result = self.fetchedResultsController.fetchedObjects as? [UNMessage]{
            for message:UNMessage in result{
                if message.senderID != Int64(user.id){
                message.isSeen = true
                //QBChat.instance.read(message.qbMessage as! QBChatMessage, completion: nil)
                }
            }
        }
        }
        match?.unreadMessageCount = 0
        CoreDataStack.shared.saveContext()
        self.navigationController?.popViewController(animated: true)
        
    }
    
}
extension UITableView {
    func scrollToBottom(animated: Bool = false) {
        let sections = self.numberOfSections
        let rows = self.numberOfRows(inSection: sections - 1)
        self.scrollToRow(at: IndexPath(row: rows-1, section: sections-1), at: .bottom, animated: animated)
       }
}

extension ChatViewController:ChatConnectionDelegate,chatPresenceDelegate{
    func recivedSytemNotification(Qbmessage: QBChatMessage) {
        
    }
    
//    func recivedSytemNotification(Qbmessage: QBChatMessage) {
//        DispatchQueue.main.async {
//
//            let alert = UIAlertController(title: "CoupleShip", message: "This conversation is no longer valid", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .destructive, handler: {[weak self] (action) in
//                 CoreDataStore().managedObjectContext?.delete((self?.match!)!)
//                 CoreDataStore().saveDB()
//                 self?.navigationController?.popViewController(animated: true)
//
//            }))
//
//            self.present(alert, animated: true, completion: nil)
//
//        }
//    }

    
    func chatWillConnect(){
       // self.view.showChatHud(message: "Connecting to chat server")
        self.view.showChatHud(message: "Connecting to chat server", position: CGPoint(x: 0, y: 84))
    }
    func chatConnectedSuccessfully(){
        self.view.hideChatHud()
        
    }
    func chatConnectionFailed(Message: String?) {
        
    }
    
        

       
        
    func chatdidDropConnection(){
       // self.view.showChatHud(message: "Connecting to chat server")
         self.view.showChatHud(message: "Connecting to chat server", position: CGPoint(x: 0, y: 84))
    }
    
    func presnceInfo(userID: UInt?, isOnline: Bool?, status: String?) {
        
        if let QBUID = self.recipientProfile?.qblocks_id{
            let qbid:UInt = UInt(QBUID)!
            if qbid == userID {
                if let online = isOnline{
                if online{
                    lblPresence.text = "Online"
                }else{
                    lblPresence.text = "Offline"
                }
                }
                
            }
        }
        
   
        
    }
    
    
    
    func reloadPresence() {
        if let contactItem = ChatManager.shared.getbuddy(quibloxId: (recipientProfile?.qblocks_id)!){
            
            if contactItem.isOnline{
                lblPresence.text = "Online"
            }else{
                lblPresence.text = "offline"
            }
    }
    }
    
}
