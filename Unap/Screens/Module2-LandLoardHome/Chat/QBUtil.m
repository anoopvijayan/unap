//
//  QBUtil.m
//  CoupleShip
//
//  Created by Levin  on 17/08/17.
//  Copyright © 2017 Levin . All rights reserved.
//

#import "QBUtil.h"

@implementation QBUtil



-(void)QbUploadImage:(UIImage*)image
        succesAction:(successBlock)successBlock
        errorAction:(errorBlock)errorBlock{
    
    
    @try {
        
        NSData * data = UIImageJPEGRepresentation(image, 0.6);
        
        
        [QBRequest TUploadFile:data fileName:@"attachment" contentType:@"image/png" isPublic:true successBlock:^(QBResponse * _Nonnull response, QBCBlob * _Nonnull tBlob) {
            
            if (tBlob.UID.length > 0){
                dispatch_async(dispatch_get_main_queue(), ^{
                    successBlock(tBlob);
                });
                
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    errorBlock();
                });
                
            }
            
        } statusBlock:^(QBRequest * _Nonnull request, QBRequestStatus * _Nonnull status) {
            
        } errorBlock:^(QBResponse * _Nonnull response) {
            dispatch_async(dispatch_get_main_queue(), ^{
                errorBlock();
            });
        }];
    } @catch (NSException *exception) {
        dispatch_async(dispatch_get_main_queue(), ^{
            errorBlock();
        });
        
    } @finally {
        
    }
    
    

}

+(void)QbDownloadWith:(NSString*)attachID
        successAction:(successBlockImage)successBlock
        errorAction:(errorBlock)errorBlock{
  
    @try {
        [QBRequest downloadFileWithUID:attachID successBlock:^(QBResponse * _Nonnull response, NSData * _Nonnull fileData) {
            if (fileData != nil){
                
                UIImage * image = [UIImage imageWithData:fileData];
                dispatch_async(dispatch_get_main_queue(), ^{
                    successBlock(image);
                });
                
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    errorBlock();
                });
                
            }
            
        } statusBlock:^(QBRequest * _Nonnull request, QBRequestStatus * _Nonnull status) {
            
        } errorBlock:^(QBResponse * _Nonnull response) {
            dispatch_async(dispatch_get_main_queue(), ^{
                errorBlock();
            });
        }];
    } @catch (NSException *exception) {
        dispatch_async(dispatch_get_main_queue(), ^{
            errorBlock();
        });
    } @finally {
        
    }
    
    
  
}


@end
