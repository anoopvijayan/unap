//
//  ConversationVC.swift
//  Unap
//
//  Created by Levin  on 21/12/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation
import UIKit
import CoreData
import Kingfisher
import Quickblox
import AFDateHelper

class ConversationVC: UIViewController {
    
    //MARK:- ViewController life cycle
    @IBOutlet weak var tableViewCoversation: UITableView!{
        didSet {
             tableViewCoversation.tableFooterView = UIView(frame: .zero)
        }
    }
    let viewmodel:ConversationVM = ConversationVM()
    var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        self.viewmodel.getAcceptedList()
        if ChatManager.shared.isConnecting() {
            self.view.showChatHud(message: "Connecting to chat server")
        }else{
            self.view.hideChatHud()
        }
        ChatManager.shared.connectionDelegate = self
        
//        if self.tabBarController?.selectedIndex != 2{
//            if let tabItems = tabBarController?.tabBar.items {
//                // In this case we want to modify the badge number of the third tab:
//                let tabItem = tabItems[2]
//                tabItem.badgeValue = nil
//            }
//        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.tableViewCoversation.delegate = self
        self.tableViewCoversation.dataSource = self
        self.fetchConversation()
      
     }
    
    func fetchConversation() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "UNConversation")
//        if let matchOwnerID = User.shared.userProfile?.id{
//            fetchRequest.predicate = NSPredicate(format: "matchOwner == %@","\(matchOwnerID)")
//        }
//
        let fetchSort = NSSortDescriptor(key: "lastMessageDate", ascending: false)
        fetchRequest.sortDescriptors = [fetchSort]
        
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: CoreDataStack.shared.mainContext, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController.delegate = self
        
        do {
            try fetchedResultsController.performFetch()
            
            //            print("Count:\(String(describing: fetchedResultsController.fetchedObjects?.count))")
            if (fetchedResultsController.fetchedObjects?.count)!>0{
               
                tableViewCoversation.reloadData()
            }else{
                self.tableViewCoversation.isHidden = true
            }
            
            
        } catch let error as NSError {
            print("Unable to perform fetch: \(error.localizedDescription)")
        }
    }
    
    func navigateToChatScreen(otherProfile:User,match:UNConversation){
        let chat:ChatViewController = UIStoryboard(name: "ReUsableControllers", bundle: nil).instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        chat.recipientProfile = otherProfile
        chat.match = match
        self.navigationController?.pushViewController(chat, animated: false)
    }
   
    
    
}

extension ConversationVC:NSFetchedResultsControllerDelegate{
    
    // MARK: - FetchedResultsController Delegate
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        
        //        if (controller.fetchedObjects?.count)! > 0{
        //            tableViewMatch.beginUpdates()
        //
        //        }
        
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type{
        case .insert:
            //  tableViewMatch.insertSections(IndexSet(integer: sectionIndex), with: .none)
            self.tableViewCoversation.reloadData()
            
        default: break
            
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        
        if (controller.fetchedObjects?.count)! > 0{
            
          //self.tableViewCoversation.reloadData()
            
        }
    }
    
    
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        // 2
        
        switch type {
        case .insert:
            self.tableViewCoversation.reloadData()
            
        case .delete:
            
            self.tableViewCoversation.reloadData()
        case .update:
            
            
            self.tableViewCoversation.reloadData()
            break;
        case .move:
            self.tableViewCoversation.reloadData()
            break;
            
        }
    }
    
}

extension ConversationVC:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionData = fetchedResultsController.sections?[section] else {
           // self.lblNoMatch.isHidden = true
            self.tableViewCoversation.isHidden = true
            return 0
        }
        if sectionData.numberOfObjects == 0 {
            self.tableViewCoversation.isHidden = true
        }else{
            self.tableViewCoversation.isHidden = false
        }
        
       
        return sectionData.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ChatConversationCell! = tableView.dequeueReusableCell(withIdentifier: "ChatConversationCell") as? ChatConversationCell
        cell.selectionStyle = .none
        
        let conversationData:UNConversation = fetchedResultsController.object(at: indexPath) as! UNConversation
        if let acceptedList = conversationData.acceptListData{
        cell.lblName.text = acceptedList.user_details?.first_name
            if let imageURL = acceptedList.user_details?.profile_pic{
        cell.imgViewProfile.kf.setImage(with: URL(string: imageURL), placeholder: UIImage(named: "user"), options: [.transition(.none)], progressBlock: nil, completionHandler: nil)
            }
            
            if  let  lastmesage = conversationData.lastMessage{
                cell.lbldate.isHidden = false
                cell.lbllastMessage.text = lastmesage
                if let message:QBChatMessage =  conversationData.qblastMessage as? QBChatMessage{
                    if let _ = message.attachments{
                        cell.lbllastMessage.text = "Image"
                    }
                }
               
            }else{
                cell.lbldate.isHidden = true
                cell.lbllastMessage.text = "Start your conversation"
            }
            
        }
        
        if let date = conversationData.lastMessageDate {
            let newdate = date as Date
            cell.lbldate.text = newdate.toStringWithRelativeTime()
        }
        
        if  conversationData.unreadMessageCount == 0 {
            cell.lblunread.text = ""
            cell.lblunread.isHidden = true
        }else{
            cell.lblunread.text = "\(conversationData.unreadMessageCount)"
            cell.lblunread.isHidden = false
        }
        
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let match:UNConversation = fetchedResultsController.object(at: indexPath) as! UNConversation
        if let profile = match.acceptListData?.user_details{
        self.navigateToChatScreen(otherProfile: profile, match: match)
        }
        
        
        
    }
}


// MARK: - Chat Connection Delegates
extension ConversationVC:ChatConnectionDelegate{
    
    
    func chatWillConnect(){
        self.view.showChatHud(message: "Connecting to chat server")
    }
    func chatConnectedSuccessfully(){
        self.view.hideChatHud()
        
    }
    func chatConnectionFailed(Message:String?){
        
    }
    func chatdidDropConnection(){
        self.view.showChatHud(message: "Connecting to chat server")
    }
    
}
