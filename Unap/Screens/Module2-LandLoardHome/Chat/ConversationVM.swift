//
//  ConversationVM.swift
//  Unap
//
//  Created by Levin  on 21/12/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation
struct ConversationVM {
    
    weak var delegate:ViewModelDelegate?
    
    func getAcceptedList(showloading:Bool = true){
        var endpoint:Endpoint
        switch UserDefaultsManager.user!.user_type!{
            
        
        case .landlord:
            endpoint = LandloardAPI.acceptedList
        case .student:
            endpoint = StudentsAPI.acceptedHomeList()
        }
    
        NetworkManager.shared.request(endpoint: endpoint) { (result:NetworkResult<AcceptedListResponse>) in
            
            switch result {
                
            case .success(let data):
                
                guard let list = data.requests else { return }
                self.saveConversationToLocal(data: list)
                
                break
            case .error(let error):
                self.delegate?.showError(message: error.localizedDescription)
                break
            }
            
        }
    }
    
    
    func saveConversationToLocal(data:[AcceptedList]){
        let datamanager = DataManager()
        for request in data{
            do{
             try datamanager.saveConversation(request: request)
            }catch(let error){
                print("DataManger:\(error.localizedDescription)")
            }
        }
        CoreDataStack.shared.saveContext()
        
    }
}

