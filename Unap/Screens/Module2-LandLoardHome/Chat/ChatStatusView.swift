//
//  ChatStatusView.swift
//  CoupleShip
//
//  Created by Levin  on 09/08/17.
//  Copyright © 2017 Levin . All rights reserved.
//

import UIKit

class ChatStatusView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    static let shared:ChatStatusView = ChatStatusView(frame: CGRect(x: 0, y: 0, width: Config.screenWidth, height: 30))
    
    init() {
        super.init(frame:CGRect.zero)
        xibSetup()
    }
    
    override init(frame:CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    var view: UIView!
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "chatStatus", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
       
        return view
    }
    

}

extension UIView{
    
   func showChatHud(message:String){
    
    self.addSubview(ChatStatusView.shared)
    ChatStatusView.shared.indicator.startAnimating()
    ChatStatusView.shared.lblMessage.text = message
    ChatStatusView.shared.frame.origin = CGPoint(x: 0, y: 0)
    if let superView = ChatStatusView.shared.superview{
        superView.isUserInteractionEnabled = false
    }
    }

    func showChatHud(message:String,position:CGPoint){
        
        self.addSubview(ChatStatusView.shared)
        ChatStatusView.shared.indicator.startAnimating()
        ChatStatusView.shared.lblMessage.text = message
        ChatStatusView.shared.frame.origin = position
        if let superView = ChatStatusView.shared.superview{
            superView.isUserInteractionEnabled = false
        }
    }

    
    
    func hideChatHud(){
        if let superView = ChatStatusView.shared.superview{
            superView.isUserInteractionEnabled = true
        }
        
        ChatStatusView.shared.removeFromSuperview()
    }
    
}
