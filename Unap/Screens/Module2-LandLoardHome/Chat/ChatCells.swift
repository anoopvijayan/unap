//
//  ChatCells.swift
//  Unap
//
//  Created by Levin  on 26/12/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation
import UIKit
import SKPhotoBrowser

class ChatConversationCell:UITableViewCell{
      @IBOutlet weak var lblName: UILabel!
      @IBOutlet weak var lbllastMessage: UILabel!
      @IBOutlet weak var lbldate: UILabel!
      @IBOutlet weak var imgViewProfile: UIImageView!
    @IBOutlet weak var lblunread: UILabel!
    
    override func awakeFromNib() {
        lblunread.layer.cornerRadius = lblunread.frame.size.width/2
        lblunread.layer.masksToBounds = true
    }
}


protocol cellDelegate: class {
    func cellClicked(cell:ChatTableViewCell)
}

class ChatTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageAttachment: UIImageView!
    @IBOutlet weak var imgSeen: UIImageView!
    @IBOutlet weak var dateHeight: NSLayoutConstraint!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    weak var delegate: cellDelegate?
    var ImageDownloaded:Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    /**
    func downloadImage(attachId:String){
        if DataCache.instance.hasDataOnMemForKey(key: attachId) || DataCache.instance.hasDataOnDiskForKey(key: attachId){
            self.ImageDownloaded = true
            self.imageAttachment.image = DataCache.instance.readImageForKey(key: attachId)
        }
        else{
            
            QBRequest.downloadFile(withID: UInt(attachId)!, successBlock: { (response, data) in
                let image: UIImage? = UIImage(data: data)
                self.imageAttachment.image = image
                self.ImageDownloaded = true
                DataCache.instance.write(image: image!, forKey: attachId)
                
            }, statusBlock: nil, errorBlock: nil)
            
        }
        
    }
 */
    @IBAction func imageClicked(_ sender: UIButton) {
        self.delegate?.cellClicked(cell: self)
    }
    
    
    
}
extension ChatViewController:cellDelegate{
    func cellClicked(cell: ChatTableViewCell) {
        if cell.ImageDownloaded{
            var images = [SKPhoto]()
            let photo = SKPhoto.photoWithImage(cell.imageAttachment.image!)
            images.append(photo)
            let browser = SKPhotoBrowser(photos: images)
            browser.initializePageIndex(0)
            present(browser, animated: true, completion: {})
        }
        
    }
    
    
    
}


