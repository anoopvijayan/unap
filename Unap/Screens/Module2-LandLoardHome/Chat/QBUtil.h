//
//  QBUtil.h
//  CoupleShip
//
//  Created by Levin  on 17/08/17.
//  Copyright © 2017 Levin . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <Quickblox/Quickblox.h>

typedef void (^successBlock)(QBCBlob *tBlob);
typedef void (^successBlockImage)(UIImage *DownloadImage);
typedef void (^errorBlock)();

@interface QBUtil : NSObject
-(void)QbUploadImage:(UIImage*)image
        succesAction:(successBlock)successBlock
         errorAction:(errorBlock)errorBlock;

+(void)QbDownloadWith:(NSString*)attachID
        successAction:(successBlockImage)successBlock
          errorAction:(errorBlock)errorBlock;

@end
