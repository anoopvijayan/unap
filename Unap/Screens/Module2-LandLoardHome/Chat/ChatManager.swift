//
//  ChatManager.swift
//  CoupleShip
//
//  Created by Levin  on 27/07/17.
//  Copyright © 2017 Levin . All rights reserved.
//

import Foundation
import Quickblox

public protocol ChatConnectionDelegate: class  {
    func chatWillConnect()
    func chatConnectedSuccessfully()
    func chatConnectionFailed(Message:String?)
    func chatdidDropConnection()
}
public protocol chatErrorDelegate{
    func chatError()
}
public protocol MessageDelegate: class  {
    func ChatMessageRecieved(message: QBChatMessage?)
    func DeliveryStatus(messageID: String?, dialogID: String?, toUserID userID: UInt?)
}

public protocol chatPresenceDelegate: class{
   
    func presnceInfo(userID: UInt?, isOnline: Bool?, status: String?)
    func reloadPresence()
    func recivedSytemNotification(Qbmessage:QBChatMessage)
}


class ChatManager: NSObject,QBChatDelegate {
    
    static let shared: ChatManager = ChatManager()
    public weak var connectionDelegate: ChatConnectionDelegate?
    public weak var messageDelegate: MessageDelegate?
    public weak var PresenceDelegate: chatPresenceDelegate?
    
    var store: DataManager {
        return DataManager()
    }
    
    override init() {
        super.init()
        
        QBChat.instance.addDelegate(self)
    
    }
    
    func isConnecting()->Bool{
        return QBChat.instance.isConnecting
    }
    
//    func QbUserRegistration(profile:UserProfile,success:@escaping (_ user:QBUUser)->()){
//        let newUser:QBUUser = QBUUser()
//
//        newUser.password = "\(profile.social_id!)"
//        newUser.login = "\(profile.couple_id!)"
//        newUser.fullName = "\(profile.partner_one_name!)"+" / " + "\(profile.partner_two_name!)"
//        QBRequest.signUp(newUser, successBlock: { (response, user) in
//             print(user.login!)
//            success(user)
//        }) { (error) in
//            print(error.description)
//        }
//
//
//    }
    
    
    func QBLogin(profile:User){
        let login = "\(profile.email!)"
        let password = Config.QuickBloxUserPassword
        
        if let conncetion = self.connectionDelegate{
            conncetion.chatWillConnect()
        }
        
//        if (QBSession.current().currentUser != nil){
//            let user = QBSession.current().currentUser!
//            user.password = password
//            QBChat.instance().connect(with: user, completion: { (Error) in
//                
//            })
//            return
//        }
        
        
        QBRequest.logIn(withUserLogin: login, password: password, successBlock: { (response, user) in
            print("QuickBlox Id:\(user.id)")
            print("kitti")
            user.password = password
            
            QBChat.instance.connect(withUserID: UInt(profile.qblocks_id!)!, password: password, completion: { (error) in
                if error == nil {
                    self.subscribeForPush()
                }
            })
//            QBChat.instance.connect(with: user, completion: { (Error) in
//                if Error != nil {
//                  //  self.subscribeForPush()
//                }
//            })
            
        }) { (error) in
            print("potti")
        }
    }
  
    func sendMessage(){
        let chatDialog: QBChatDialog = QBChatDialog(dialogID: nil, type: QBChatDialogType.private)
        chatDialog.occupantIDs = [30560648]
        
        QBRequest.createDialog(chatDialog, successBlock: {(response: QBResponse?, createdDialog: QBChatDialog?) in
            let message: QBChatMessage = QBChatMessage()
            message.text = "Hey levin here"
            
            let params : NSMutableDictionary = NSMutableDictionary()
            params["save_to_history"] = true
            message.customParameters = params
            
            createdDialog?.send(message, completionBlock: { (Error) in
               
                if Error == nil{
                    print("Message sent Successfully")
                }
               
            })
            
        }, errorBlock: {(response: QBResponse!) in
            
        })
    }
    
    func createDialog(recipientID:UInt,completion:@escaping (_ dialog:QBChatDialog)->()){
         let chatDialog: QBChatDialog = QBChatDialog(dialogID: nil, type: QBChatDialogType.private)
         chatDialog.occupantIDs = [NSNumber(value: recipientID)]

    
        QBRequest.createDialog(chatDialog, successBlock: { (response, dialog) in
            
            completion(dialog)
        }) { (response) in
            
            print(response.error.debugDescription)
        }
        
   
        
    }
    
    func sendMessage(dialog:QBChatDialog,senderID:UInt,recipientId:UInt,text:String,converstation:UNConversation){
        
        
        let message:QBChatMessage = QBChatMessage.markable()
        message.text = text
        message.senderID = senderID
        message.dateSent = Date()
        message.recipientID = recipientId
        let params : NSMutableDictionary = NSMutableDictionary()
        params["save_to_history"] = true
        params["request_ID"] = converstation.requestID
        message.customParameters = params
        
        
    
        dialog.send(message) { [unowned self] (Error) in
            if Error == nil{
                print("Message sent Successfully")
                DispatchQueue.main.async {
                    self.store.saveMessage(chatMessage: message)
                }
            }
        }
        
        
    }
    
    func sendMessageWithAttachment(dialog:QBChatDialog,senderID:UInt,recipientId:UInt,message:QBChatMessage,match:UNConversation)
    {
        message.text = ""
        message.senderID = senderID
        message.dateSent = Date()
        message.recipientID = recipientId
        let params : NSMutableDictionary = NSMutableDictionary()
        params["save_to_history"] = true
        params["request_ID"] = match.requestID ?? "0"
        message.customParameters = params

        dialog.send(message) { (Error) in
            if Error == nil{
                print("Message sent Successfully")
                DispatchQueue.main.async {
              DataManager().saveMessage(chatMessage: message)
                }

            }
        }
    }
//
//
//
    func getQuickBlockUser(coupleId:String,completion:@escaping (_ user:QBUUser)->()){

        
        QBRequest.user(withID: UInt(coupleId)!, successBlock: { (response, user) in
            completion(user)
        }) { (response) in
            print("kittiella")
        }

//        QBRequest.user(withLogin: coupleId, successBlock: { (response, user) in
//            if user != nil{
//                completion(user)
//            }
//
//
//        }) { (response) in
//              print("kittiella")
//        }

//        QBRequest.users(for: [coupleId], successBlock: { (QBResponse, QBGeneralResponsePage, users) in
//
//
//
//        }, errorBlock: { (QBResponse) in
//
//
//        })

    }
    
    
    
    func getConversations(){
        
        let extendedRequest = ["sort_desc" : "_id"]
        
        let page = QBResponsePage(limit: 100, skip: 0)
        
        QBRequest.dialogs(for: page, extendedRequest: extendedRequest, successBlock: { (response: QBResponse, dialogs: [QBChatDialog]?, dialogsUsersIDs: Set<NSNumber>?, page: QBResponsePage?) -> Void in
            
            print(dialogs?.count ?? 0)
        }) { (response: QBResponse) -> Void in
            
        }
    }
    
    func SendSyetemMessage(recipientId:String,matchId:String){
        let message: QBChatMessage = QBChatMessage()
        let params = NSMutableDictionary()
        params["event_type"] = "block"
        params["matched_unique_id"] = matchId
        message.customParameters = params
        message.recipientID = UInt(recipientId)!
        QBChat.instance.sendSystemMessage(message) { (Error) in
            
        }
    }
    
    func getbuddy(quibloxId:String)->QBContactListItem?{
        if let itemContact = QBChat.instance.contactList{
            if let contacts = itemContact.contacts{
                for item:QBContactListItem in contacts{
                    if item.userID == UInt(quibloxId){
                        return item
                    }
                }
            }
           
            
        }
        
        return nil
    }
    
    func addAsBuddy(quibloxId:String)->QBContactListItem?{
        
        
        
        var buddyAvailable:Bool = false
         if let itemContact = QBChat.instance.contactList{
            if let contacts = itemContact.contacts{
                for item:QBContactListItem in contacts{
                    if item.userID == UInt(quibloxId){
                        buddyAvailable = true
                        return item
                    }
                }
            }
        }
      
        
        if !buddyAvailable{
            QBChat.instance.addUser(toContactListRequest: UInt(quibloxId)!, completion: { (error) in
                
            })
        }
        return nil
    }
    
    func subscribeForPush(){

        if let devicetoken = UserDefaultsManager.deviceTokenData{
            let deviceIdentifier: String = UIDevice.current.identifierForVendor!.uuidString
            let subscription: QBMSubscription! = QBMSubscription()

            subscription.notificationChannel = .APNS
            subscription.deviceUDID = deviceIdentifier
            subscription.deviceToken = devicetoken
            QBRequest.createSubscription(subscription, successBlock: { (response: QBResponse!, objects: [QBMSubscription]?) -> Void in
                  print("success")
                //
            }) { (response: QBResponse!) -> Void in
                //
                print(response.error?.description ?? "")
            }
        }
    }
    
    
    func unscubscribeForPush(){
        
   
    if let deviceUDID = UIDevice.current.identifierForVendor?.uuidString{
        QBRequest.unregisterSubscription(forUniqueDeviceIdentifier: deviceUDID, successBlock: nil, errorBlock: nil)
        
    }
    
    
//        if let user = QBChat.instance().currentUser(){
//        QBRequest.deleteSubscription(withID: user.id, successBlock: { (response) in
//             print("success")
//        }, errorBlock: { (Response) in
//             print(Response.error?.description ?? "")
//        })
//        }
    }
 
    
    //Mark: Chat Delegates
    
    func chatDidReceive(_ message: QBChatMessage) {
        print("message:\(message.text ?? "")")
       
        DataManager().saveMessage(chatMessage: message)
 
        QBChat.instance.mark(asDelivered: message, completion: nil)
        if let messageDel = self.messageDelegate{
            messageDel.ChatMessageRecieved(message: message)
        }
        if let matchId:String = message.customParameters["request_ID"] as? String{
            DispatchQueue.main.async {
                
            if let matched = DataManager().fetchMatchWith(matchId: matchId){
                matched.lastMessage = message.text
                matched.lastMessageDate = message.dateSent as NSDate?
                matched.qblastMessage = message
                matched.unreadMessageCount += 1
                CoreDataStack.shared.saveContext()
                NotificationCenter.default.post(name: .UNnewMessage, object: nil)
            }
 
 
            }
        }
        
        
    }
    
    func chatDidConnect(){
        print("connected to Chat after login")
        if let conncetion = self.connectionDelegate{
            conncetion.chatConnectedSuccessfully()
        }
    }
    func chatDidNotConnectWithError(_ error: Error){
        print("Chat not connected:\(String(describing: error.localizedDescription))")
        if let conncetion = self.connectionDelegate{
            conncetion.chatConnectionFailed(Message: (error.localizedDescription))
        }
    }
    
  
    
    func chatDidDeliverMessage(withID messageID: String, dialogID: String, toUserID userID: UInt){
        
        if let chatmessage:UNMessage = store.fetchMessage( messageID: messageID){
            chatmessage.isDelivered = true
            CoreDataStack.shared.saveContext()
            
        }
       
    }
    
    
    
    func chatDidReadMessage(withID messageID: String, dialogID: String, readerID: UInt) {
    
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) { [weak self] in
       
        if let chatmessage:UNMessage = self?.store.fetchMessage(messageID: messageID){
            
            if let currentUser = QBChat.instance.currentUser{
                if currentUser.id != readerID {
                    if !chatmessage.isSeen{
                       chatmessage.isSeen = true
                        CoreDataStack.shared.saveContext()
                    }
            
                }
            }
            
        }
             }
 
    }
    
    func chatDidReceiveContactAddRequest(fromUser userID: UInt) {
        QBChat.instance.confirmAddContactRequest(userID, completion: nil)
    }
    
  
    func chatContactListDidChange(_ contactList: QBContactList) {
        if let presence = self.PresenceDelegate{
            presence.reloadPresence()
            
        }
    }
    
    func chatDidReceiveContactItemActivity(_ userID: UInt, isOnline: Bool, status: String?) {
        if let presence = self.PresenceDelegate{
                presence.presnceInfo(userID: userID, isOnline: isOnline, status: status)
            
        }
    }
    
    
    
    
   
    
    func chatDidReceiveSystemMessage(_ message: QBChatMessage) {
        
        if let para = message.customParameters{
            
            if  let _ = para["matched_unique_id"] as? String{
                if let presence = self.PresenceDelegate{
                    presence.recivedSytemNotification(Qbmessage: message)
                }
            }
            
        
            
        }
        
    }
    
    
    
    func chatDidReconnect() {
        if let conncetion = self.connectionDelegate{
            conncetion.chatConnectedSuccessfully()
        }
    }
    
    func chatDidFail(withStreamError error: Error) {
        if let conncetion = self.connectionDelegate{
            conncetion.chatConnectionFailed(Message: (error.localizedDescription))
        }
    }
    
    func chatDidAccidentallyDisconnect() {
        if let conncetion = self.connectionDelegate{
            conncetion.chatdidDropConnection()
        }
        
    }
    
    
    
}


