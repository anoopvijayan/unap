//
//  ProfileVC.swift
//  Unap
//
//  Created by Levin  on 09/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import UIKit
import Kingfisher
import SKPhotoBrowser

class ProfileVC: UIViewController,ViewModelDelegate {
    
    enum RowType:Int,CaseIterable{
        case Documents = 0
        case CardDetails   = 1
        case AddaHome = 2
        case Logout   = 3
    }
    
    
    
    //MARK:- IBOutlets
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var imgViewProfile: CircularImageView!
    
    
    //MARK:- Properties
    var viewModel:ProfileVM = ProfileVM()
    var user:User = UserDefaultsManager.user!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      //  self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.hero.navigationAnimationType = .fade
        viewModel.delegate = self
      //  viewModel.getUseDetails { (user) in
     //       self.user = user
     //      self.tableview.reloadData()
     //   }
         self.user = UserDefaultsManager.user!
         self.prepareUI()
         self.tableview.reloadData()

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.prepareUI()
       
       
       // self.imgViewProfile.kf.setImage(with: URL(string: imageURL), placeholder: UIImage(named: "user"), options: [.transition(.none)], progressBlock: nil, completionHandler: nil)
        
    }
    
    // MARK: - UISetups
    func prepareUI(){
        
        self.lblName.text = user.first_name?.capitalized ?? "No Name"
        self.tableview.delegate = self
        self.tableview.dataSource = self
        guard let imageURL = user.profile_pic, let url = URL(string: imageURL)   else {return}
        let image = ImageCache.default.retrieveImageInDiskCache(forKey: url.absoluteString)
        self.imgViewProfile.image = image
       
        
    }
    // MARK: - IBActions
    @IBAction func viewAndEditPressed(_ sender: UIButton) {
        self.performSegue(withIdentifier: "profileDetails", sender: nil)
    }
    @IBAction func profilePicPressed(_ sender: UIButton) {
        if let image = self.imgViewProfile.image {
            var images = [SKPhoto]()
            let photo = SKPhoto.photoWithImage(image)// add some UIImage
            images.append(photo)
            let browser = SKPhotoBrowser(photos: images)
            present(browser, animated: true, completion: {})
        }
    }
    
    // MARK: - Navigation
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let profileDetail = segue.destination as? ProfileDetailsVC{
            profileDetail.delegate = self
        }
    }

}

//MARK: - UITableView delegate and datasource
extension ProfileVC:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return RowType.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let rowtype = RowType(rawValue: indexPath.row)
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell") as! ProfileCell
        switch rowtype!{
            
        case .Documents:
            cell.lblName.text = "Documents"
            cell.imageViewIcon.image = UIImage(named: "document")
           
            if user.doc_is_verified == 0{
                cell.imageViewStatus.image = UIImage(named: "close")
                cell.lblInfo.text = "Required document verification."
            }else if user.doc_is_verified == 1{
                cell.imageViewStatus.image = UIImage(named: "tick")
                cell.lblInfo.text = "All documents are verified."
            }else if user.doc_is_verified == 2{
                cell.imageViewStatus.image = UIImage(named: "InReview")
                cell.lblInfo.text = "Document is in review."
            }
        case .CardDetails:
            cell.lblName.text = "Bank account"
             cell.imageViewIcon.image = UIImage(named: "bank")
            if let _  = UserDefaultsManager.bankDetails{
                cell.lblInfo.text = "Account details linked successfully."
                cell.imageViewStatus.image = UIImage(named: "tick")
            }else{
               cell.lblInfo.text = "Add account details to receive payments. "
               cell.imageViewStatus.image = UIImage(named: "hzd")
            }
            
        case .Logout:
            cell.lblName.text = "Logout"
            cell.imageViewIcon.image = UIImage(named: "logout")
            cell.imageViewStatus.image = nil
        case .AddaHome:
            if user.is_verified! == 1{
                cell.lblName.text = "Email Verification"
                 cell.lblInfo.text = "Email verified successfully."
                cell.imageViewIcon.image = UIImage(named: "addahome")
                cell.imageViewStatus.image = UIImage(named: "tick")
            }else{
                cell.lblName.text = "Email Verification"
                cell.lblInfo.text = "Email not yet verified.Please verify your email Id to receive student request."
                cell.imageViewIcon.image = UIImage(named: "addahome")
                cell.imageViewStatus.image = UIImage(named: "hzd")
            }
            
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let rowtype = RowType(rawValue: indexPath.row)
        
        switch rowtype!{
            
        case .Documents:
            self.navigationController?.isNavigationBarHidden = true
            if user.doc_is_verified == 0{
                self.performSegue(withIdentifier: "Documents", sender: nil)
            }
           
        case .CardDetails:
             self.performSegue(withIdentifier: "BankInfo", sender: nil)
        case .Logout:
            self.performLogout()
        case .AddaHome:break
            
        }
    }
    
    func performLogout(){
        self.viewModel.logout {
            UserDefaultsManager.clearData()
            ChatManager.shared.unscubscribeForPush()
            QBChat.instance.disconnect(completionBlock: nil)
            QBRequest.logOut(successBlock: nil, errorBlock: nil)
            try? DataManager.init().deleteEntity(UNConversation.self)
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NavigationOnboarding")
            if let appdelegate = UIApplication.shared.delegate as? AppDelegate{
                appdelegate.window?.rootViewController = vc
            }
        }
    }
}

extension ProfileVC:profileUpdateDelegate{
    func updatedNewProfilePic(image: UIImage) {
        self.imgViewProfile.image = image
    }
}
