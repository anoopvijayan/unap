//
//  ProfileVM.swift
//  Unap
//
//  Created by Levin  on 23/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation
struct ProfileVM {
    
   weak var delegate:ViewModelDelegate?

}

//MARK: - API Calls
extension ProfileVM{
    func getUseDetails(compeletion:@escaping (_ user:User)->()){
        NetworkManager.shared.requestForm(endpoint: LandloardAPI.getUserDetails) { (result:NetworkResult<GetDetailsResponse>) in
            switch result{
                
            case .success(let data):
                compeletion(data.user!)
            case .error(_):
                break
            }
        }
        
    }
    
    func logout(completion:@escaping ()->()){
        self.delegate?.showLoading(message: "Loading")
        NetworkManager.shared.request(endpoint: LandloardAPI.logout) { (result:NetworkResult<APICommonResponse>) in
            self.delegate?.hideLoading()
            switch result{
                
            case .success(let data):
                if data.status == 1{
                    completion()
                }else{
                    self.delegate?.showError(message: data.message)
                }
            case .error(let error):
                self.delegate?.showError(message: error.localizedDescription)
            }
            
        }
    }
    
}


