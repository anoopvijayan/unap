//
//  ProfileDetailsVM.swift
//  Unap
//
//  Created by Levin  on 25/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation

struct ProfileDetailsVM:Validator,ProfileImageDownloadble {
    
    weak var delegate:ViewModelDelegate?
    var name:String = ""
    var age:String  = ""
    var imageData:Data?
    
    func validate()->Endpoint?{
        
        let result = self.validate(values: (ValidationType.firstName,name),(ValidationType.age,age))
        switch result{
            
        case .success:
            let requestData = APIRequestUpdateProfile(first_name: name, dob: age, university_id: nil)
            let endpoint = LandloardAPI.updateProfile(model: requestData, data: imageData)
            return endpoint
           
        case .failure(_, let message):
             self.delegate?.showError(message: message.rawValue)
        }
        return nil
    }
    
    func  performUpdate(endpoint:Endpoint,completion:@escaping (_ result:User)->()){
        self.delegate?.showLoading(message: "Updating")
        NetworkManager.shared.requestForm(endpoint: endpoint) { (result:NetworkResult<userUpdateResponse>) in
        self.delegate?.hideLoading()
            switch result {
                
            case .success(let data):
                UserDefaultsManager.user = data.user
                guard let url = data.user!.profile_pic else{return}
                self.prefetchPorfilePic(url: URL(string: url))
                
                completion(data.user!)
            case .error(let error):
                self.delegate?.showError(message: error.localizedDescription)
            }
        }
        
    }
    
}
