//
//  NotificationVC.swift
//  Unap
//
//  Created by Levin  on 11/12/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import UIKit
import Kingfisher

class NotificationVC: UIViewController,ViewModelDelegate {

    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var tableView: UITableView!{
        didSet {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            tableView.rowHeight = UITableView.automaticDimension
            tableView.estimatedRowHeight = 210
            tableView.tableFooterView = UIView(frame: .zero)
           
            
        }
    }
    
    //MARK:- Properties
    var viewModel:NotificationVM = NotificationVM()
    var homeRequest:[HomeRequest]?
    
    //MARK:- ViewController life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // self.viewModel.delegate = self
        self.viewModel.getHomeRequests(showLoading: false) {[weak self] (result) in
            self?.homeRequest = result
            self?.tableView.reloadData()
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    //MARK:- IBActions
    @IBAction func refresh(_ sender: UIButton) {
        self.viewModel.getHomeRequests {[weak self] (result) in
            self?.homeRequest = result
            
            self?.tableView.reloadData()
        }
    }
    
}

extension NotificationVC:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard self.homeRequest?.count != nil else {
            self.tableView.isHidden = true
            return 0
        }
        
        if self.homeRequest?.count == 0{
            self.tableView.isHidden = true
        }else{
            self.tableView.isHidden = false
        }
        return self.homeRequest?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
        cell.row = indexPath.row
        cell.delegate = self
        if let request = self.homeRequest?[indexPath.row],
            let home = request.home,
            let requestedBy = request.requested_by
            {
                if let requestProfilePic = requestedBy.profile_pic{
            cell.imgViewProfile.kf.setImage(with: URL(string: requestProfilePic), placeholder: UIImage(named: "user"), options: [.transition(.none)], progressBlock: nil, completionHandler: nil)
                }else{
                    cell.imgViewProfile.image = UIImage(named: "user")
                }
                if let homePic = home.home_img?.first?.home_image{
                    cell.imgViewHome.kf.setImage(with: URL(string: homePic))
                }
                if let homeName = home.home_title,
                    let requesteeName = requestedBy.first_name{
                    cell.lblrequest.text =  "\(requesteeName) has requested to join your home"
                }
                
        }
        return cell
    }
}

extension NotificationVC:NotificationActions{
    func didPerformedNotificationAction(action: notificationActionType, row: Int) {
        
        if let data = self.homeRequest?[row],let requestID = data.home_request_id{
           
            switch action{
                
            case .accept:
                self.viewModel.performAction(action: action, ID: requestID) {
                    if let index = self.homeRequest?.firstIndex(where: {$0.home_request_id == data.home_request_id }){
                        self.homeRequest?.remove(at: index)
                        self.tableView.reloadData()
                    }
                }
                break
            case .reject:
                self.viewModel.performAction(action: action, ID: row) {
                    if let index = self.homeRequest?.firstIndex(where: {$0.home_request_id == data.home_request_id }){
                        self.homeRequest?.remove(at: index)
                        self.tableView.reloadData()
                    }
                }
                break
            }
        }
        
    }
    
    
    
  
}
