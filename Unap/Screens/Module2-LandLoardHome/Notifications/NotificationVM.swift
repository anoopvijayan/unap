//
//  NotificationVM.swift
//  Unap
//
//  Created by Levin  on 11/12/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import UIKit

struct NotificationVM {

   weak var delegate:ViewModelDelegate?
    
    func getHomeRequests(showLoading:Bool = true,completion:@escaping (_ requests:[HomeRequest]?)->()){
        if showLoading { self.delegate?.showLoading(message: "loading")}
                NetworkManager.shared.request(endpoint: LandloardAPI.listAllRequest) { (result:NetworkResult<HomeRequestResponse>) in
            if showLoading   { self.delegate?.hideLoading() }
            switch result{
                
            case .success(let data):
                
            completion(data.requests)
                break
            case .error(let error):
                if showLoading {
                self.delegate?.showError(message: error.localizedDescription)
                }
            }
        }
    }
    
    func performAction(action:notificationActionType,ID:Int ,completion:@escaping ()->() ){
      self.delegate?.showLoading(message: "Loading")
        NetworkManager.shared.requestForm(endpoint: LandloardAPI.request(action: action, requestID: ID)) { (result:NetworkResult<APICommonResponse>) in
            self.delegate?.hideLoading()
            
            switch result{
                
            case .success(let data):
                if data.status == 1{
                    completion()
                }else{
                    if data.message.contains(find: "Student is now connected"){
                        completion()
                    }
                    self.delegate?.showError(message: data.message)
                }
                break
            case .error(let error):
                self.delegate?.showError(message: error.localizedDescription)
            }
        }
    }

}
