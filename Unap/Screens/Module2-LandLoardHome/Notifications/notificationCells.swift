//
//  notificationCells.swift
//  Unap
//
//  Created by Levin  on 17/12/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation
import UIKit


protocol NotificationActions : class {
    func didPerformedNotificationAction(action:notificationActionType, row:Int)
}

class NotificationCell:UITableViewCell{
    @IBOutlet weak var lblrequest: UILabel!
    @IBOutlet weak var imgViewHome: UIImageView!
    @IBOutlet weak var imgViewProfile: CircularImageView!
    
    //delegate
    
    weak var delegate:NotificationActions?
    var row:Int = 0
    
    
    
    //MARK: IBActions
    @IBAction func acceptBtnpressed(_ sender: UIButton) {
        self.delegate?.didPerformedNotificationAction(action: .accept, row: self.row)
    }
    
    @IBAction func rejectBtnpressed(_ sender: UIButton) {
        self.delegate?.didPerformedNotificationAction(action: .reject, row: self.row)
    }
    
}




enum notificationActionType:Int {
    case accept = 1
    case reject = 0
}
