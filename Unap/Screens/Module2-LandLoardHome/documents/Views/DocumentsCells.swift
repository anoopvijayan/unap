//
//  DocumentsCells.swift
//  Unap
//
//  Created by Levin  on 11/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import UIKit

class DocumentsTableCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblsubtitle: UILabel!
    @IBOutlet weak var imgViewIcon: UIImageView!
    @IBOutlet weak var btnUpload: UIButton!
    
    var doc:Documentupload?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
       
    }

}

class DocumentUploadCell:UICollectionViewCell{
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imagviewUpload: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.layer.cornerRadius = 8.0
    }
}
