//
//  DocumentsVC.swift
//  Unap
//
//  Created by Levin  on 11/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import UIKit
import Disk

class DocumentsVC: UIViewController,ImagePickerPresentable {
    //MARK:- IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var gradientView: GradientView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnUpload: UIButton!
    
    //MARK:- Properties
    
    lazy var documents:[Documentupload] = {
        var proofOfHome = Documentupload(isSelected: true, selectedImageName: "homeProofSelected", unselectedImageName: "homeProofUnselected", documentCount: 1, name: "Proof of home ownership", subtitle: "Electricity bill\\/Water bill\\/Tax reciept", DocumentImages: [ImageInfo(position: 0, name: nil, tag: .homeownership)])
        var idDoc = Documentupload(isSelected: false, selectedImageName: "idDocSelected", unselectedImageName: "idDocUnselected", documentCount: 2, name: "ID document", subtitle: "upload ID(both sides if available)", DocumentImages: [ImageInfo(position: 0, name: nil, tag: .id_doc0),ImageInfo(position: 1, name: nil, tag: .id_doc1)])
        var insurance = Documentupload(isSelected: false, selectedImageName: "InsuranceSelected", unselectedImageName: "InsuranceUnselected", documentCount: 1, name: "LandLoard Insurance", subtitle: "Insurance", DocumentImages: [ImageInfo(position: 0, name: nil, tag: .landlord_insurance)])
        return [proofOfHome,idDoc,insurance]
    }()
    
    var selecteddocument:Documentupload?
    
    override func viewDidLoad() {
        super.viewDidLoad()

       self.prepareUI()
    }
    
    //MARK:- UI Setups
    
    func prepareUI(){
      
       
        tableView.delegate = self
        tableView.dataSource = self
        
        
        
  
        self.selecteddocument = documents.first
        collectionView.delegate = self
        collectionView.dataSource = self
        
        if let layout:CollectionViewCenteredFlowLayout = collectionView.collectionViewLayout as? CollectionViewCenteredFlowLayout {
         // layout.itemSize =
        
          layout.scrollDirection = .horizontal
        }
    btnUpload.setTitleColor(UIColor.lightGray, for: .disabled)
    self.btnUpload.isEnabled = false
    
    }
    
    func checkUploadStatus(){

        
        let status = self.documents.filter { (doc:Documentupload) -> Bool in
            let d =  doc.DocumentImages?.filter({ (img:ImageInfo) -> Bool in
                return img.imageName != nil
            })

            return (d?.count ?? 0) >= 1

        }.count == 3
        
        
        
        
        
    
        
        print("Status:\(status)")
        self.btnUpload.isEnabled = status
        
    }
    
    //MARK: - IBActions
    @IBAction func backPressed(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func uploadBtnPressed(_ sender: UIButton) {
        let items = self.documents.map { (doc:Documentupload) -> [ImageInfo] in
            let it =  doc.DocumentImages?.compactMap({ (img:ImageInfo) -> ImageInfo? in
                if img.imageName != nil {
                    return img
                }
                return nil
            })
            var new:[ImageInfo] = []
            new.append(contentsOf: it!)
           return new
    
            }.flatMap{$0}
        print(items.count)
       
        self.view.showHud(message: "Uploading docs")
        self.uploadDocs(images: items, completion: { (user) in
            self.view.hideHud()
            print("ALL Docs uploaded")
            if let landloard  = user{
                UserDefaultsManager.user = landloard
            }
            self.showAlertMessage(title: Config.appName, message: "You have succefully uploaded all documents,We will review your document.You can now add a new home", btnName: "Ok", btnCallback: { (action) in
                self.navigationController?.popViewController(animated: true)
            })
            
        }) {
            self.view.hideHud()
            print("Uploading failed")
        }
        
        
    
    }
    
    func uploadDocs(images:[ImageInfo],completion:@escaping (_ user:User?)->(),failed:@escaping ()->()){
        let endpoints  = images.map{ $0.getEnpoint()}
        let count = endpoints.count
        var counter = 0
        var successCounter = 0
        var operationArray:[NetworkOperaton] = []
        for item in endpoints{
            
            let op =  NetworkOperaton(endpoint: item) { (result) in
                
                counter = counter + 1
                
                if let data = result {
                    if data.status == 1{
                        successCounter = successCounter + 1
                    }
                }else{
                   
                }
                
                if counter == count{
                    if counter == successCounter{
                    completion(result?.response?.user)
                    }else{
                        failed()
                    }
                }
                
            }
            operationArray.append(op)
            
        }
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1
        queue.addOperations(operationArray, waitUntilFinished: false)
        
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
         // bgView.roundCorners(corners: [.bottomLeft,.bottomRight], radius:24.0)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
     */

}
extension DocumentsVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return documents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let document = self.documents[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "DocumentsTableCell") as! DocumentsTableCell
        
        if document == self.selecteddocument!{
            cell.lblName.textColor = UIColor.greenUN
            cell.imgViewIcon.image = UIImage(named: document.selectedImageName )
        }else{
            cell.lblName.textColor = UIColor.black
            cell.imgViewIcon.image = UIImage(named: document.unselectedImageName )
        }
        
        cell.doc = document
        
        if document.ready{
            cell.btnUpload.setBackgroundImage(UIImage(named: "tick"), for: .normal)
        }else{
             cell.btnUpload.setBackgroundImage(UIImage(named: "upload"), for: .normal)
        }
        
        cell.imgViewIcon.image = UIImage(named: document.unselectedImageName)
        cell.lblName.text = document.name
        cell.lblsubtitle.text = document.subtitle
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         self.selecteddocument = self.documents[indexPath.row]
        self.tableView.reloadData()
        
        UIView.transition(with: self.collectionView, duration: 0.3, options: .transitionCrossDissolve, animations: {
            //Do the data reload here
             self.collectionView.reloadData()
        }, completion: nil)

       
    }
}

extension DocumentsVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return selecteddocument?.documentCount ?? 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let index = self.documents.firstIndex(of: self.selecteddocument!)
        let document = self.documents[index ?? 0]
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "DocumentUploadCell", for: indexPath) as! DocumentUploadCell
        if let Info = document.DocumentImages?[indexPath.row],let image  = Info.image{
            cell.imagviewUpload.image = image
        }else{
            cell.imagviewUpload.image = nil
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = self.documents.firstIndex(of: self.selecteddocument!)
        var documents = self.documents[index ?? 0]
        
        let row = indexPath.row
        var name:String = ""
        var tag :DocUploadTag = .bank_docs
        if index == 0 {
            name = "Proof"
            tag = .homeownership
        }else if index == 1{
            name = "IdDoc"
            if row == 0 {
            tag = .id_doc0
            }else{
            tag = .id_doc1
            }
        }else if index == 2{
            name = "Insurance"
              tag = .landlord_insurance
            
        }
        
        self.showImagePicker { (image) in
            
            
            let imageName = name+"\(row)"
            let img = ImageInfo(position: row, name: imageName, tag: tag)
            
            do{
             try Disk.save(image, to: .temporary, as: "Docs/" + imageName)
                self.addimageToDoc(doc: &documents, info: img)
                //documents.addimageToDoc(info: img)
              
                 print(self.documents.first?.DocumentImages ?? "s")
                
                
                DispatchQueue.main.async {
                    
                    if let cell = collectionView.cellForItem(at: indexPath) as? DocumentUploadCell{
                        cell.imagviewUpload.image = image
                        self.tableView.reloadData()
                        self.checkUploadStatus()
                    }
                }
            }catch(let error){
                self.logError(error)
            }
      
        }
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      return  CGSize(width: UIScreen.width/2.5, height: self.collectionView.frame.height-10
        
        )
    }
    
    func addimageToDoc( doc:inout Documentupload,info:ImageInfo){
       // guard var document = doc else {return}
        
        guard let infoImage = doc.DocumentImages?[info.position] else { return }
        
        infoImage.imageName = info.imageName
       
        
       
        
    }
    
}


struct Documentupload:Equatable{
   
    
    var isSelected:Bool
    var selectedImageName:String
    var unselectedImageName:String
    var documentCount:Int
    var name:String
    var subtitle:String
    var DocumentImages:[ImageInfo]?
    
    static func == (lhs: Documentupload, rhs: Documentupload) -> Bool {
        return lhs.name == rhs.name
    }
    
    var ready:Bool {
        let d =  DocumentImages?.filter({ (img:ImageInfo) -> Bool in
            return img.imageName != nil
        }).count ?? 0 >= 1
        return d
    }
 
}

class ImageInfo{
    var position:Int
    var imageName:String?
    var tag:DocUploadTag
    
    
    
    var image:UIImage?{
        guard let name  = self.imageName else { return nil }
        do{
            let img = try Disk.retrieve("Docs/" + name, from: .temporary, as: UIImage.self)
            return img
        }catch(_){
            return nil
        }
    }
    
    init(position:Int,name:String?,tag:DocUploadTag) {
        self.position = position
        self.imageName = name
        self.tag = tag
    }
    
    func getEnpoint() -> Endpoint{
        
        let data =  self.image?.jpegData(compressionQuality: 0.5)
        return OnboardingEndpoints.uploadDoc(name: self.imageName ?? "No_Name", type: self.tag.rawValue, data: data ?? Data())
    }
    
    func getHomeUploadEndPoint(homeUDID:String) -> Endpoint?{
        guard let data = self.image?.jpegData(compressionQuality: 0.5) else {
            return nil
        }
        return LandloardAPI.uploadHomeImage(homeId: homeUDID, data: data)
    }
    
  
    
    
 
}

enum DocUploadTag:String{
    case id_doc0
    case id_doc1
    case bank_docs
    case homeownership
    case landlord_insurance
    case home_image
}
