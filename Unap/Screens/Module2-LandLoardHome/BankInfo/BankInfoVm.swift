//
//  BankInfoVm.swift
//  Unap
//
//  Created by Levin  on 23/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation

struct BankInfoVM {
    
    weak var delegate:ViewModelDelegate?
    var name:String = ""
    var sortCode:String = ""
    var accountNumber:String = ""
    var bankAddress:String   = ""
    
    func validate() -> APISaveBank?{
        guard name.containsNonWhitespace else{
            self.delegate?.showError(message: "Account Name can not be empty.")
            return nil
        }
        guard sortCode.containsNonWhitespace && sortCode.count >= 8 else{
            self.delegate?.showError(message: "Not a valid SORT code.")
            return nil
        }
        guard accountNumber.containsNonWhitespace else {
            self.delegate?.showError(message: "Account number is not valid")
            return nil
        }
        
        let requestInfo =  APISaveBank(sort_code: self.sortCode, account_number: self.accountNumber, address: self.bankAddress, name: self.name)
        return requestInfo
        
    }
    
    
}

//MARK: API CAll
extension BankInfoVM{
    func performSaveBankAPI(info:APISaveBank,compeletion:@escaping (_ result:BankDetail?)->()){
        
        self.delegate?.showLoading(message:"Loading")
        NetworkManager.shared.requestForm(endpoint: LandloardAPI.saveBankDetails(model: info)) { (result:NetworkResult<BankDetail>) in
            self.delegate?.hideLoading()
            switch result{
                
            case .success(let data):
                compeletion(data)
                break
            case .error(let error):
                self.delegate?.showError(message: error.localizedDescription)
                break
            }
        }
        
    }
}
