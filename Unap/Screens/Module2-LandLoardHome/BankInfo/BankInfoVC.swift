//
//  BankInfoVC.swift
//  Unap
//
//  Created by Levin  on 16/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import UIKit

class BankInfoVC: UITableViewController,ViewModelDelegate {
    
    //MARK:- IBOutlets
    @IBOutlet weak var TFName: UITextField!
    @IBOutlet weak var TFSortCode: UITextField!
    @IBOutlet weak var TFAccountNo: UITextField!
    @IBOutlet weak var TFBankAddress: UITextField!
    
    //MARK: - Properties
    var viewModel:BankInfoVM = BankInfoVM()
    
    //MARK: - View controller life cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.delegate = self
        self.prepareUI()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()


        let tap = UITapGestureRecognizer(target: self, action:#selector(handleTap(sender:)))
        
        self.view.addGestureRecognizer(tap)
    }
    
    //MARK: UI Setups
    
    func prepareUI(){
        let prefillUI = {
            guard let bankDetails = UserDefaultsManager.bankDetails else {return}
            self.TFName.text = bankDetails.name ?? ""
            self.TFSortCode.text = bankDetails.sort_code ?? ""
            if let accountNo = bankDetails.account_number{
            self.TFAccountNo.text = "\(accountNo)"
            }
            self.TFBankAddress.text = bankDetails.address ?? ""
        }
        prefillUI()
    }
    
    func bindUI(){
        self.viewModel.name = self.TFName.text ?? ""
        self.viewModel.sortCode = self.TFSortCode.text ?? ""
        self.viewModel.accountNumber = self.TFAccountNo.text  ?? ""
        self.viewModel.bankAddress = self.TFBankAddress.text ?? ""
    }
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        // handling code
        self.view.endEditing(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
   
    

    @IBAction func backBtnPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func savebtnPressed(_ sender: UIButton) {
       self.bindUI()
        guard let requestInfo = viewModel.validate() else{return}
        viewModel.performSaveBankAPI(info: requestInfo) {[unowned self]  (BankDetails) in
            
            guard let bank = BankDetails else { return }
            UserDefaultsManager.bankDetails = bank
            self.showAlertMessage(title: Config.appName, message: "Succusfully uploaded all account details", btnName: "Ok", btnCallback: { (action) in
                self.navigationController?.popViewController(animated: true)
            })
        }
    }
}


extension BankInfoVC:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var result = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? ""
        result = result.formattedNumber(mask: "XX-XX-XX")
        
        textField.text = result
        
        
        return false
    }
    
   
}
