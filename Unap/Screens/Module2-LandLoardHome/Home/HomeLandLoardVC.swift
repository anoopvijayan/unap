//
//  HomeLandLoardVC.swift
//  Unap
//
//  Created by Levin  on 08/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import UIKit
import Hero
import Kingfisher

class HomeLandLoardVC: UIViewController,ProfileImageDownloadble {
    //MARK: - IBOutlets
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var homeTableView: UITableView!
    @IBOutlet weak var btnAddaHome: UIButton!
    
    //MARK: - Properties
    var user:User  {
        get {
            return UserDefaultsManager.user!
        }
        set {
            UserDefaultsManager.user = newValue
        }
    }
    var viewModel:HomeLandLoardVM = HomeLandLoardVM()
    var homes:[Home] = []
    var isloading:Bool = false
    var selectedRow:Int = 0
    
    var refreshView: RefreshView!
    var tableViewRefreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = .clear
        refreshControl.tintColor = .clear
        refreshControl.addTarget(self, action: #selector(refreshTableView), for: .valueChanged)
        return refreshControl
    }()
    
    
   
      //MARK: - UIViewController life cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        viewModel.getUserDetails {[weak self] (user) in
            
            self?.user = user
            self?.prepareUI()
        }
    }
  
    override func viewDidLoad() {
     super.viewDidLoad()
      self.prepareUI()
      self.navigationController?.hero.navigationAnimationType = .fade
      self.prepareRefreshUI()
      self.loadHomes()
        //Login to chat server
        ChatManager.shared.QBLogin(profile: user)
       self.refreshNewlyAddedHome()
    }
    
    func refreshNewlyAddedHome(){
        NotificationCenter.default.addObserver(forName: .UNnewHomeAdded, object: nil, queue: .main) { [weak self] (notification) in
            self?.refreshTableView()
        }
    }
    
    func loadHomes(){
        self.isloading = true
        viewModel.listAllHomes(loadintial: true) { [weak self](result) in
            self?.isloading = false
            if let home = result {
              
                self?.addNewHomes(newHomes: home)
                if self?.homes.count ?? 0 > 0 {
                    self?.homeTableView.isHidden = false
                    self?.btnAddaHome.isHidden = false
                }
                self?.homeTableView.reloadData()
            }
           
            
        }
    }
    
    func loadMoreHomes(){

        guard viewModel.nextpageAvailable else {return}
        self.isloading = true
        self.viewModel.listAllHomes {[weak self] (data) in
            self?.isloading = false
            guard let home = data else {return}
             self?.addNewHomes(newHomes: home)
            self?.homeTableView.reloadData()
        }
    }
    
    func addNewHomes(newHomes:[Home]){
        for item in newHomes{
            if self.homes.contains(where: { (home) -> Bool in
                return  home.id == item.id
            }){
                
            }else{
                self.homes.insert(item, at: 0)
            }
        }
    }
    
    
    // MARK: - UISetups
    func prepareUI(){
        guard let url = user.profile_pic else { return }
          self.prefetchPorfilePic(url: URL(string: url))
        if let name = user.first_name?.components(separatedBy: " ").first{
            self.lblName.text = "Hello" + " " + name + "!"
        }else{
            self.lblName.text = "Hello User!"
        }
        
    }
    
    func prepareRefreshUI(){
        // Adding 'tableViewRefreshControl' to tableView
        self.homeTableView.refreshControl = tableViewRefreshControl
        // Getting the nib from bundle
        getRefereshView()
    }
    
    func getRefereshView() {
        if let objOfRefreshView = Bundle.main.loadNibNamed("RefreshView", owner: self, options: nil)?.first as? RefreshView {
            // Initializing the 'refreshView'
            refreshView = objOfRefreshView
            // Giving the frame as per 'tableViewRefreshControl'
            refreshView.frame = tableViewRefreshControl.frame
            // Adding the 'refreshView' to 'tableViewRefreshControl'
            tableViewRefreshControl.addSubview(refreshView)
        }
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "unmapLoaderHome" {
            let but:UIButton = sender as! UIButton
            if let destinationViewController = segue.destination as? UNMapController {
                destinationViewController.home = self.homes[but.tag]
            }
        }
    }
   
    //MARK: IBActions
    @IBAction func newhomePressed(_ sender: UIButton) {
        self.performSegue(withIdentifier: "newHome", sender: nil)
//        guard let verifed = user.is_verified  else { return }
//        if verifed == 0{
//            self.showAlertMessage(title: "Unap", message: "Looks like you have not verified your Documents,Would you like to do that now.", btnName: "OK") { (action) in
//                self.tabBarController?.selectedIndex = 2
//            }
//        }
    }
    
    @objc func refreshTableView() {
        refreshView.startAnimation()

        self.isloading = true
        viewModel.listAllHomes(loadintial: true) { [weak self] (data) in
             self?.tableViewRefreshControl.endRefreshing()
             self?.isloading = false
             guard let home = data else {return}
            self?.addNewHomes(newHomes: home)
            self?.homeTableView.reloadData()
        }
        
    }
}






//MARK:- UITableviewDelegate and Datasource
extension HomeLandLoardVC:UITableViewDelegate,UITableViewDataSource{
func numberOfSections(in tableView: UITableView) -> Int {
    return 1
}

func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    if self.homes.count == 0 {
        self.homeTableView.isHidden = true
    }else{
        self.homeTableView.isHidden = false
    }
    
    return self.homes.count
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let home = self.homes[indexPath.row]
    let cell = tableView.dequeueReusableCell(withIdentifier: "cellHome") as! HomeListCell
    if let url = home.home_img?.first?.home_image{
        cell.homeImage.kf.indicatorType = .activity
        cell.homeImage.kf.setImage(with: URL(string: url))
    }else{
        cell.homeImage.image = UIImage(named: "homePlaceHolder")
    }
    cell.homeTitle.text = home.home_title ?? "No Title"
    cell.bedCount.setTitle(home.bedDisplayable, for: .normal)
    cell.roomCount.setTitle(home.roomDisplayable, for: .normal)
    cell.bathRoomCount.setTitle(home.bathroomDisplayable, for: .normal)
    cell.cost.setTitle(home.rentDisplayble, for: .normal)
    cell.locationIcon.tag = indexPath.row
    return cell
}
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.homes.count - 2  && !self.isloading{
            self.loadMoreHomes()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        if let cell  = tableView.cellForRow(at: indexPath) as? HomeListCell{
            self.selectedRow = indexPath.row
            cell.hero.id = "collection\(indexPath.row)"
            cell.locationIcon.hero.id = "btn\(indexPath.row)"
        }
        let controller = HouseDetailVC.makeFromStoryboard()
        controller.home = self.homes[indexPath.row]
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}

extension HomeLandLoardVC:HeroViewControllerDelegate{
    
    func heroWillStartAnimatingTo(viewController: UIViewController) {
        if let vc =  viewController as? HouseDetailVC{
            vc.galleryCollectionView.hero.id = "collection\(self.selectedRow)"
            vc.btnLocation.hero.id = "btn\(self.selectedRow)"
        }
        
    }
}
