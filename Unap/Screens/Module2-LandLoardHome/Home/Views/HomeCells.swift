//
//  HomeCells.swift
//  Unap
//
//  Created by Levin  on 12/11/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation
import UIKit

class HomeCell:UITableViewCell{
    @IBOutlet weak var lblName: UILabel!
}

class HomeListCell:UITableViewCell{
    @IBOutlet weak var homeImage: UIImageView!
    @IBOutlet weak var homeTitle: UILabel!
    @IBOutlet weak var bedCount: UIButton!
    @IBOutlet weak var roomCount: UIButton!
    @IBOutlet weak var bathRoomCount: UIButton!
    @IBOutlet weak var cost: UIButton!
    @IBOutlet weak var locationIcon: UIButton!
    
}
