//
//  HomeLandLoardVM.swift
//  Unap
//
//  Created by Levin  on 08/11/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation

class  HomeLandLoardVM {
    
    weak var delegate:ViewModelDelegate?
    
    var nextpage = 1
    var nextpageAvailable = true
}


//MARK:- API Calls
extension HomeLandLoardVM{
    
    func listAllHomes(loadintial:Bool = false,completion:@escaping (_ home:[Home]?)->()){
        var page:Int!
        if loadintial{
           
           page = 1
           self.nextpageAvailable = true
        }else{
            page = self.nextpage
        }
        NetworkManager.shared.request(endpoint: LandloardAPI.listAllHomes(page: "\(page ?? 0)")) { (result:NetworkResult<HomeListResponse>) in
            
            switch result{
                
            case .success(let data):
                if let home = data.homeList?.home {
                    completion(home)
                }else{
                    completion(nil)
                }
                if let _ = data.homeList?.next_page_url {
                    if loadintial{
                        if self.nextpage == 1{ self.nextpage += 1 }
                    }else{
                        self.nextpage = (data.homeList?.current_page ?? 0) + 1
                    }
                   
                    self.nextpageAvailable = true
                }else{
                    self.nextpageAvailable = false
                }
            
            case .error(let error):
                completion(nil)
                self.delegate?.showError(message: error.localizedDescription)
                
            }
        }
    }
    
    
    func getUserDetails(compeletion:@escaping (_ user:User)->()){
        NetworkManager.shared.requestForm(endpoint: LandloardAPI.getUserDetails) { (result:NetworkResult<GetDetailsResponse>) in
            switch result{
                
            case .success(let data):
                compeletion(data.user!)
            case .error(_):
                break
            }
        }
        
    }
    
}
