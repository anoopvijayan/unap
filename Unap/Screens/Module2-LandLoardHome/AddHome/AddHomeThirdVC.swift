//
//  AddHomeThirdVC.swift
//  Unap
//
//  Created by Levin  on 31/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import UIKit
import Disk

class AddHomeThirdVC: UIViewController,ViewModelDelegate {

    
    // MARK:- IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    // MARK:- Properties
    lazy var imageHome:[ImageInfo] = {
        var one = ImageInfo(position: 0, name: nil, tag: .home_image)
        var Two = ImageInfo(position: 1, name: nil, tag: .home_image)
        var three = ImageInfo(position: 2, name: nil, tag: .home_image)
        var four = ImageInfo(position: 3, name: nil, tag: .home_image)
        var five = ImageInfo(position: 4, name: nil, tag: .home_image)
        return [one,Two,three,four,five]
    }()
    
    var viewModel:AddHomeViewModel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if let layout = self.collectionView.collectionViewLayout as? DXScaleFlowLayout{
            layout.scrollDirection = .horizontal
            layout.isPagingEnabled = true
            layout.transformScale  = 0.15
            layout.itemSize = CGSize(width: self.collectionView.frame.width - 60, height: self.collectionView.frame.height)
        }
        collectionView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.viewModel.delegate = self
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- IBOutlets
    @IBAction func publishbtnPressed(_ sender: UIButton) {
        self.viewModel.homeImages = self.imageHome
        if self.viewModel.validateThirdStage(){
            self.publishHome()
        }
       
    }
   
    func publishHome(){
        guard let publishhomeEndpoint = viewModel.getCreateHomeEndpoint() else { return }
        
        viewModel.performCreatehomeAPI(endpoint: publishhomeEndpoint) {[unowned self]  (homeAPIResponse) in
            guard let homeID = homeAPIResponse.home?.id else { return }
            self.uploadHomeImages(homeID: "\(homeID)")
        }
    }
    
    func uploadHomeImages(homeID:String){
        
        viewModel.uploadHomeImages(homeID: homeID, completion: {[unowned self] (result) in
          
            print ("All image uploaded successfully")
            
            if let response = result{
                self.showAlertMessage(title: Config.appName, message: "Successfully published your home", btnName: "OK", btnCallback: { (action) in
                    
                   NotificationCenter.default.post(name: .UNnewHomeAdded, object: nil)
                    self.navigationController?.dismiss(animated: true, completion: nil)
                })
            }
            
        }) {
            print("Home Image Uploading Failed.")
           
        }
    }
}

extension AddHomeThirdVC:UICollectionViewDataSource,UICollectionViewDelegate,ImagePickerPresentable{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectImageCell", for: indexPath) as! SelectImageCell
        if let image = self.imageHome[indexPath.row].image{
            cell.imageView.image = image
        }else{
            cell.imageView.image = nil
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let position  = indexPath.item
        let imageData = self.imageHome[indexPath.item]
       
        self.showImagePicker(allowEditing: false) { (image) in
            let name = imageData.tag.rawValue + "\(position)"
            do{
            try Disk.save(image, to: .temporary, as: "Docs/" + name)
            imageData.imageName = name
            self.collectionView.reloadData()
            }catch(let error){
                self.logError(error)
            }

        }
        
    }
    
}
