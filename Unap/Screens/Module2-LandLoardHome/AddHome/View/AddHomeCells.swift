//
//  AddHomeCells.swift
//  Unap
//
//  Created by Levin  on 30/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation
import UIKit

class AddHomeTableCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
   
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
//    var propertyType:[String] = {
//        return ["Apartmnet","House","Seconday Unit","Unique space","Bed and breakfast","Boutique hotel"]
//        
//    }()
    
    var propertyType:[String]?
    var data:HomeDescription?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let count = self.propertyType?.count else {
            return 0
        }
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectDescriptionCell", for: indexPath) as! SelectDescriptionCell
        cell.lblName.text = propertyType?[indexPath.item]
        if let desc = self.data {
            if desc.selectedOption.contains(indexPath.item){
                cell.lblName.backgroundColor = UIColor.greenUN
                cell.lblName.textColor = UIColor.white
            }else{
                 cell.lblName.backgroundColor = UIColor.clear
                cell.lblName.textColor = UIColor.black
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let label = UILabel(frame: CGRect.zero)
        label.text = propertyType?[indexPath.item]
        label.sizeToFit()
        var rect: CGRect = label.frame //get frame of label
        rect.size = (label.text?.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: label.font.fontName , size: 14)!]))! //Calculate as per label font
        var lblWidth = rect.width
        if lblWidth < 25 {
            lblWidth = 35
        }
        return CGSize(width: lblWidth+17, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let desc = self.data{
            if desc.signleSelection{
                desc.selectedOption = [indexPath.item]
            }else{// multiple selection
                if desc.selectedOption.contains(indexPath.item),let index = desc.selectedOption.index(of:indexPath.item){
                    desc.selectedOption.remove(at: index)
                    
                }else{
                    desc.selectedOption.append(indexPath.item)
                }
                
                
            }
            self.collectionView.reloadData()
        }
    }
}

class SelectDescriptionCell:UICollectionViewCell{
     @IBOutlet weak var lblName: UILabel!
    
    var gradient:CAGradientLayer {
        let grad = CAGradientLayer()
        grad.frame = self.lblName.bounds
        grad.colors = [UIColor.greenUN.cgColor,UIColor.blueUN.cgColor]
        return grad
    }
    
    func higlightOption(_ status:Bool){
        if status{
        self.lblName.layer.insertSublayer(self.gradient, at: 0)
        }else{
           self.gradient.removeFromSuperlayer()
        }
    }
    
    
    
}

class SelectImageCell:UICollectionViewCell{
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.layer.cornerRadius = 16
    }
    
}

