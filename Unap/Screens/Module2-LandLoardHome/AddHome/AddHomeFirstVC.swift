//
//  AddHomeFirstVC.swift
//  Unap
//
//  Created by Levin  on 30/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import UIKit
import Hero
import GooglePlaces
import GooglePlacePicker
import LionheartCurrencyTextField


class AddHomeFirstVC: UIViewController,ViewModelDelegate {
    
    // MARK:- IBOutlets
    @IBOutlet weak var TWtitle: UITextView!
    @IBOutlet weak var TWDescription: UITextView!
    @IBOutlet weak var TWLocation: UITextView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var TFprice: LionheartCurrencyTextField!
    
    //MARK: - Properties
    var keyboardManager:UNKeyboardManager?
    var viewModel:AddHomeViewModel = AddHomeViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        // Do any additional setup after loading the view.
        self.perepareUI()
        self.askForPermission()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.delegate = self
    }
    
    
    //MARK:- UI Setups
    
    func perepareUI(){
          self.navigationController?.hero.navigationAnimationType = .fade
          self.keyboardManager = UNKeyboardManager(view: scrollView)
    }
    func bindUI(){
        self.viewModel.hometitle = self.TWtitle.text.clearSpaces
        self.viewModel.description = self.TWDescription.text.clearSpaces
        self.viewModel.price = "\(self.TFprice.currencyValue ?? 0)"
    }
    
    func askForPermission(){
        let permission:PermissionType = PermissionType.locationWhileUsing
        
        switch PermissionManager.shared.permissionStatus(for: permission) {
        case .authorized: print("Autharized")
        case .denied: showAlertSettings(type: permission)
        case .notDetermined: showNotificationController(type: permission, block:  { (state) in
           
        })
        case .restricted:  showAlertSettings(type: permission)
        }
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      
        if let vc:AddHomeSecondVC = segue.destination as? AddHomeSecondVC {
            vc.viewModel = self.viewModel
        }
    }
    

    @IBAction func nextPressed(_ sender: UIButton) {
        self.bindUI()
        if viewModel.validateStageOne(){
            self.performSegue(withIdentifier: "second", sender: nil)
        }
        
        //self.performSegue(withIdentifier: "second", sender: nil)
    }
    @IBAction func backpressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension AddHomeFirstVC:UITextViewDelegate,GMSPlacePickerViewControllerDelegate,UITextFieldDelegate{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if textView == TWtitle {
            let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
            let numberOfChars = newText.count
            return numberOfChars < 200
        }else if textView == TWDescription {
            let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
            let numberOfChars = newText.count
            return numberOfChars < 600
        }
        
        return true
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.keyboardManager?.activeField = textView
        if textView == self.TWLocation {
    
            let config = GMSPlacePickerConfig(viewport: nil)
            let placePicker = GMSPlacePickerViewController(config: config)
            placePicker.delegate = self
            present(placePicker, animated: true, completion: nil)
            return false
        }
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.keyboardManager?.activeField = textField
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
       
        
        return true
        
    }
    
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        // Dismiss the place picker, as it cannot dismiss itself.
       
       
        viewModel.location = locationUn(lat:"\(place.coordinate.latitude)",long: "\(place.coordinate.longitude)")
        
        GMSPlacesClient.shared().lookUpPlaceID(place.placeID, callback: {[unowned self]  (place, error) -> Void in
            if let error = error {
                print("lookup place id query error: \(error.localizedDescription)")
                return
            }
            
            guard let Nplace = place else {
               // print("No place details for \(place.placeID)")
                return
            }
            
            print("Place name \(Nplace.name)")
            print("Place address \(Nplace.formattedAddress)")
            print("Place placeID \(Nplace.placeID)")
            print("Place attributions \(Nplace.attributions)")
             print("Place coordinates \(Nplace.coordinate)")
            
            for  item in Nplace.addressComponents!{
                print("Type:\(item.type) : \(item.name)")
            }
             self.TWLocation.text = Nplace.formattedAddress ?? "No address"
            viewController.dismiss(animated: true, completion: nil)
            
        })

    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
        print("No place selected")
    }
    
}



