//
//  AddHomeSecondVC.swift
//  Unap
//
//  Created by Levin  on 30/10/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import UIKit

class AddHomeSecondVC: UIViewController,ViewModelDelegate {
    
    // MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- Properties
    let descriptions = HomeDescription.gethomeDescriptions()
    var viewModel:AddHomeViewModel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.delegate   = self
    }
    


    // MARK: - Navigation

   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? AddHomeThirdVC{
            vc.viewModel = self.viewModel
        }
    }
   

}

extension AddHomeSecondVC:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return HomeDescription.gethomeDescriptions().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddHomeTableCell") as! AddHomeTableCell
        cell.lblName.text = self.descriptions[indexPath.row].name
        cell.propertyType = self.descriptions[indexPath.row].options
        cell.data = self.descriptions[indexPath.row]
        cell.collectionView.delegate = cell
        cell.collectionView.dataSource = cell
        cell.collectionView.reloadData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
        
    }
    
    @IBAction func nextPressed(_ sender: UIButton) {
        
        self.viewModel.homeDescription = self.descriptions
        if self.viewModel.validateSecondStage(){
        self.performSegue(withIdentifier: "photoSelection", sender: nil)
        }
    }
}


class HomeDescription{
    var name:String
    var options:[String]
    var signleSelection:Bool
    var selectedOption:[Int]
    
    init(name:String,options:[String],signleSelection:Bool,selectedOption:[Int]) {
        self.name = name
        self.options = options
        self.signleSelection = signleSelection
        self.selectedOption  = selectedOption
    }
    
    var selectedOptionSingle:String? {
        
        if let firstObject = self.selectedOption.first{
           return self.options[firstObject]
        }else{
            return nil
        }

    }
    
    
    var selectedOptionMultiple:[String]?{
        
        let options = self.selectedOption.map({ (index:Int) -> String in
            return self.options[index]
        })
        
        return options
    }
    
    
    
    static func gethomeDescriptions() ->[HomeDescription]{
        let typeofAccomodation = HomeDescription(name: "Type of accommodation", options:["Apartment","House","Seconday Unit","Unique space","Bed and breakfast","Boutique hotel"], signleSelection: true, selectedOption: [] )
        let howManyRooms = HomeDescription(name: "How many rooms", options:["1","2","3","4","5","5+"], signleSelection: true, selectedOption: [] )
         let howManyBeds = HomeDescription(name: "How many beds", options:["1","2","3","4","5","5+"], signleSelection: true, selectedOption: [] )
        let Amenities = HomeDescription(name: "Amenities", options:["Wifi","Kitchen","TV","hot water","heating","AC","Washer","Pool","Gym","Elevator","Free parking"], signleSelection: false, selectedOption: [] )
        let numberofbathrooms = HomeDescription(name: "Number of bathrooms", options:["1","2","3","4","4+"], signleSelection: true, selectedOption: [] )
         let billsIncluded = HomeDescription(name: "Bills included", options:["Water","Gas","Electricity","Internet"], signleSelection: false, selectedOption: [] )
        
        return [typeofAccomodation,howManyRooms,howManyBeds,Amenities,numberofbathrooms,billsIncluded]
        
    }
}
