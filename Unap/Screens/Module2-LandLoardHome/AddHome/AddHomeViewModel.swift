//
//  AddHomeViewModel.swift
//  Unap
//
//  Created by Levin  on 05/11/18.
//  Copyright © 2018 Levin . All rights reserved.
//

import Foundation



struct AddHomeViewModel {
    
    //MARK: - Properties
     weak var delegate:ViewModelDelegate?
    
    var hometitle:String = ""
    var description:String = ""
    var location:locationUn  = locationUn(lat: "0",long: "0")
    var price:String = ""
    var homeDescription:[HomeDescription]?
    var homeImages:[ImageInfo]?
    var currencyCode:String{
        return Locale.current.currencyCode ?? "USD"
    }
    
    
    func validateStageOne() -> Bool{
        
        guard self.hometitle.containsNonWhitespace else {
            self.delegate?.showError(message: "Home title canot be empty.")
            return false
        }
        
        guard self.description.containsNonWhitespace else{
            self.delegate?.showError(message: "Description canot be empty.")
            return false
        }
        
        guard self.location.lat != "0" && self.location.long != "0" else {
            self.delegate?.showError(message: "Please select a valid location")
             return false
        }
        
        guard self.price.containsNonWhitespace && price  != "0" else {
            self.delegate?.showError(message: "Price cannot be empty.")
            return false
        }
        
        return true
    }
    
    
    func validateSecondStage()->Bool{
        if let descriptions = self.homeDescription{
            var status = false
            for item in descriptions{
                if item.selectedOption.count == 0 {
                    self.delegate?.showError(message: "Please choose \(item.name)")                 
                    return false
                }
            }
            status = true
            return status
            
        }
        return false
    }
    
    func validateThirdStage()->Bool{
        if let images =  self.homeImages{
            
            for item in images{
                if item.imageName != nil{
                    return true
                }
            }
        }
        self.delegate?.showError(message: "Please choose atleast one image.")
        return false
    }
    
    func getCreateHomeEndpoint()->Endpoint?{
        
        if let AcomodationType = self.homeDescription?[0].selectedOptionSingle,
           let rooms = self.homeDescription?[1].selectedOptionSingle,
           let beds = self.homeDescription?[2].selectedOptionSingle,
           let amenities = self.homeDescription?[3].selectedOptionMultiple,
           let bathroom = self.homeDescription?[4].selectedOptionSingle,
           let bilLsIncluded = self.homeDescription?[5].selectedOptionMultiple
           {
            let apiRequest = APIRegisterHome(accomodation_type:AcomodationType,room_count:rooms,bed_count:beds,bathroom_count:bathroom,amenities:amenities,bill_included:bilLsIncluded,home_price:self.price,lattitude:self.location.lat,longitude:self.location.long,description:self.description,home_title:self.hometitle,currency_code:self.currencyCode)
            let endpoint = LandloardAPI.createHome(model: apiRequest)
            return endpoint
            
        }else{
            return nil
            
        }
        
       
        
        
       
    }
    
}

//MARK: API Calls
extension AddHomeViewModel{
    
    func performCreatehomeAPI(endpoint:Endpoint,compeletion:@escaping (_ data:HomeAPIResponse)->()){
        self.delegate?.showLoading(message: "Creating Home")
        NetworkManager.shared.requestForm(endpoint: endpoint) { (result:NetworkResult<HomeAPIResponse>) in
            self.delegate?.hideLoading()
            
            switch result{
                
            case .success(let data):
                 compeletion(data)
            case .error(let error):
                self.delegate?.showError(message: error.localizedDescription)
            }
           
        }
    }
    
    func uploadHomeImages(homeID:String,completion:@escaping (_ result:APICommonResponse?)->(),failed:@escaping ()->()){
         self.delegate?.showLoading(message: "Uploading Images")
        guard let homeImages = self.homeImages else{
        self.delegate?.hideLoading()
            failed()
            return
        }
        let endpoints = homeImages.compactMap{$0.getHomeUploadEndPoint(homeUDID: homeID)}
        let count = endpoints.count
        
        var counter = 0
        var successCounter = 0
        var operationArray:[NetworkOperaton] = []
        for item in endpoints{
            
            let op =  NetworkOperaton(endpoint: item) { (result) in
                
                counter = counter + 1
                
                if let data = result {
                    if data.status == 1{
                        successCounter = successCounter + 1
                    }
                }else{
                    
                }
                
                if counter == count{
                    if counter == successCounter{
                         self.delegate?.hideLoading()
                        completion(result)
                    }else{
                         self.delegate?.hideLoading()
                        failed()
                    }
                }
                
            }
            operationArray.append(op)
            
        }
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1
        queue.addOperations(operationArray, waitUntilFinished: false)
        
        
        
    }
}



struct locationUn {
    var lat:String
    var long:String
}
